#!/bin/bash -ex
#

script_dir="$(readlink -f "$(dirname "$0")")"
cd "$script_dir"

echo "Check if virtualenv is already configured"
echo $(pwd)

[ -z "$ArmarX_DIR" ] && ArmarX_DIR="$(readlink -f ../../../../..)"

if ! [ -d "$ArmarX_DIR" ]
	then
		echo "Did not find ArmarXCore! '$ArmarX_DIR' is no directory"
		exit 1
fi

# if virtualenv not configured
if [ ! -d "env" ]
	then
		echo "Virtualenv not configured"	
		virtualenv env --no-site-packages
		source env/bin/activate
	        pip install -U pip
		pip install -r requirements.txt
                cd "$ArmarX_DIR/ArmarXCore/etc/python/"
                python setup.py develop
                echo "Installation finished"
	else
		echo "Virtualenv configured"
		source env/bin/activate
		echo "Activated virtualenv"
fi

if [ "$ENV_ONLY" != 1 ]
then
    cd "$script_dir"
    echo "Start Script '$script_dir/PoseBasedActionRecognizer_parseOption.py'"
    python PoseBasedActionRecognizer_parseOption.py -c
    echo "End Script"
else
    echo "Not starting the script"
fi
