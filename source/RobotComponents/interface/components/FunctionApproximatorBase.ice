/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotCOMPONENTS::DMPComponent
* @author     You
* @date       2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>


module armarx
{
    sequence<Ice::DoubleSeq > DVector2d;
    sequence<string> SVector;

    ["cpp:virtual"]
    class FunctionApproximatorBase
    {
        void initialize(string name, Ice::DoubleSeq factors);
        void learn(string name, DVector2d x, DVector2d y);
        void blearn(string name, Ice::DoubleSeq x, Ice::DoubleSeq y);
        void ilearn(string name, Ice::DoubleSeq x, double y);
        double predict(string name, Ice::DoubleSeq x);
        Ice::DoubleSeq bpredict(string name, Ice::DoubleSeq x);
        void reset();

        void getFunctionApproximatorFromFile(string funcName, string name);
        void getFunctionApproximatorsFromFile(SVector funcNames, string filename);
        void saveFunctionApproximatorInFile(string funcName, string name);

    };
};

