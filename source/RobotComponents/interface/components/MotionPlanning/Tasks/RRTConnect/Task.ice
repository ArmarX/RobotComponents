/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/interface/components/MotionPlanning/Tasks/MotionPlanningTask.ice>
#include <RobotComponents/interface/components/MotionPlanning/Tasks/RRTConnect/DataStructures.ice>

module armarx
{
module rrtconnect
{
    ["cpp:virtual"]
    class TaskBase extends MotionPlanningTaskWithDefaultMembersBase
    {
        //management
        /**
         * @brief The number of workers used to perform planning.
         */
        long workerCount;

        //problem specific params
        /**
         * @brief Stores the paths in the task.
         * Used by the manager node to store the results before shut down.
         * @param paths The paths.
         */
        void setPath(Path paths);


        void workerHasAborted(long workerId);
    };
};
};
