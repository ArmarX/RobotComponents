/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/core/RemoteObjectNode.ice>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/DataStructures.ice>
#include <RobotComponents/interface/components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/Task.ice>

module armarx
{
    class CSpaceBase;

module addirrtstar
{
    //forward
    class TaskBase;
    class MotionPlanningTaskBase;

    ["cpp:virtual"]
    class ManagerNodeBase implements TreeUpdateInterface, ResourceManagementInterface
    {
        idempotent void kill();

        //client requests
        ["cpp:const"] idempotent PathWithCost getBestPath();
        ["cpp:const"] idempotent long getPathCount();
        ["cpp:const"] idempotent PathWithCost getNthPathWithCost(long n);
        ["cpp:const"] idempotent PathWithCostSeq getAllPathsWithCost();

        //worker requests
        ["cpp:const"] idempotent Update getUpdate(long workerId, long updateSubId);
        ["cpp:const"] idempotent FullIceTree getTree();

        idempotent void setWorkersFinalUpdateId(long workerId, long finalUpdateId);

        //member
        TaskBase* task;
        armarx::RemoteObjectNodePrxList remoteObjectNodes;

        long initialWorkerCount;
        long maximalWorkerCount;
        cprs::ComputingPowerRequestStrategyBase planningComputingPowerRequestStrategy;

        float dcdStep;
        long maximalPlanningTimeInSeconds;
        CSpaceBase cspace;

        VectorXf startNode;
        VectorXf goalNode;
        AdaptiveDynamicDomainParameters addParams;
        float targetCost;
        long batchSize;
        /**
         * @brief Number of nodes created (by a worker) before a connect to the goal node is tried (by this worker).
         */
        long nodeCountDeltaForGoalConnectionTries;

        ["cpp:const"] idempotent long getNodeCount();
    };
};
};
