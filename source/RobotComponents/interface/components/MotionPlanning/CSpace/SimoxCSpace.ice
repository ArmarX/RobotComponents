/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <RobotComponents/interface/components/MotionPlanning/CSpace/CSpace.ice>

module armarx
{
    struct AttachedObject
    {
        memoryx::ObjectClassBase objectClassBase;
        armarx::PoseBase relativeObjectPose;
        string attachedToRobotNodeName;
        string associatedCollisionSetName;
    };
    sequence<AttachedObject> AttachedObjectList;

    struct AgentPlanningInformation
    {
        string agentProjectName;
        Ice::StringSeq agentProjectNames;
        string agentRelativeFilePath;
        Ice::StringSeq kinemaicChainNames;
        Ice::StringSeq additionalJointsForPlanning;
        Ice::StringSeq jointsExcludedFromPlanning;
        Ice::StringSeq collisionSetNames;
        Ice::StringSeq collisionObjectNames; // used additionally to the collision sets (as collision model in a CD manager)
        AttachedObjectList attachedObjects;
        StringFloatDictionary initialJointValues;

        armarx::PoseBase agentPose;
    };

    struct StationaryObject
    {
        memoryx::ObjectClassBase objectClassBase;
        armarx::PoseBase objectPose;
    };
    sequence <StationaryObject> StationaryObjectList;

    ["cpp:virtual"]
    class SimoxCSpaceBase extends CSpaceBase
    {
        void addStationaryObject(StationaryObject obj);
        void setAgent(AgentPlanningInformation agent);

        ["protected"] StationaryObjectList stationaryObjects;
        ["protected"] AgentPlanningInformation agentInfo;

        ["protected"] memoryx::CommonStorageInterface* commonStorage;
        ["protected"] float stationaryObjectMargin = 0.0f;

    };

    /**
     * @brief Similar to SimoxCSpaceBase.
     * Difference: The agent's 3D pose is planned, too. (first 6 coordinates = x,y,z, roll,pitch,yaw)
     */
    ["cpp:virtual"]
    class SimoxCSpaceWith3DPoseBase extends SimoxCSpaceBase
    {
        ["protected"] Vector6fRange poseBounds;
    };

    /**
     * @brief Similar to SimoxCSpaceBase.
     * Difference: The agent's 2D pose is planned, too. (first 3 coordinates = x,y,rz)
     */
    ["cpp:virtual"]
    class SimoxCSpaceWith2DPoseBase extends SimoxCSpaceBase
    {
        ["protected"] Vector3fRange poseBounds;
    };
};
