#pragma once

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

module armarx
{
    dictionary<string, Vector3f> StringVector3fMap;

    interface ViconMarkerProviderListenerInterface
    {
        void reportLabeledViconMarkerFrame(StringVector3fMap labeledMarkers);
        void reportUnlabeledViconMarkerFrame(Vector3fSeq unlabeledMarkers);
    };

};
