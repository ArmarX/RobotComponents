#include <RobotComponents/interface/components/GraspingManager/GraspingManagerInterface.ice>
#include <RobotComponents/interface/components/GraspingManager/GraspGeneratorInterface.ice>
#include <RobotComponents/interface/components/MotionPlanning/CSpace/SimoxCSpace.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <RobotAPI/interface/core/Trajectory.ice>



module armarx
{

    interface PlannedMotionProviderInterface
    {
        GraspingTrajectory planMotion(SimoxCSpaceBase cSpace, SimoxCSpaceBase cspacePlatform, MotionPlanningData mpd) throws RuntimeError;
        GraspingTrajectory planMotionParallel(SimoxCSpaceBase cSpace, SimoxCSpaceBase cspacePlatform, MotionPlanningData mpd) throws RuntimeError;
        TrajectoryBase planJointMotion(SimoxCSpaceBase cSpace, MotionPlanningData mpd) throws RuntimeError;
        TrajectoryBase planPlatformMotion(SimoxCSpaceBase cspacePlatform, MotionPlanningData mpd) throws RuntimeError;
    };
};
