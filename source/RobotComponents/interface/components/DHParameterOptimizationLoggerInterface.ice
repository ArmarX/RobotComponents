
#include <RobotComponents/interface/components/ViconMarkerProviderInterface.ice>

module armarx
{
    interface DHParameterOptimizationLoggerInterface extends ViconMarkerProviderListenerInterface
    {
        void logData();
        /**
         * @brief logDataWithRepeatAccuracy Logs data and calculates error between the current viconMarkerData and the data in the log entry,
         * that was logged 'comparedLogEntryBack' steps back.
         * @param comparedLogEntryBack amount of logEntries back (I.e. '1' means this logEntry is compared with the logEntry right before,
         * '2' means it is compared with the second last entry, ...)
         */
        void logDataWithRepeatAccuracy();
        void startViconLogging();
        void stopViconLogging();
        void init(string kinematicChainName,
                  StringStringDictionary neckMarker,
                  StringStringDictionary handMarker,
                  string loggingFileName,
                  bool logRepeatAccuracy);
    };
};
