/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021 Humanoids Group, H2T, KIT
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/InertialMeasurementUnit.ice>
#include <RobotAPI/interface/core/RobotLocalization.ice>
#include <RobotAPI/interface/units/PlatformUnitInterface.ice>
#include <RobotAPI/interface/units/LaserScannerUnit.ice>

#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>


module armarx {

  interface CartographerMappingAndLocalizationInterface
          extends //OdometryListener,
                  LaserScannerUnitListener,
                 // InertialMeasurementUnitListener,
                 armem::client::MemoryListenerInterface
  {};

  interface SelfLocalizationListener
  {
    void reportCorrectedPose(float x, float y, float theta);
  }

};
