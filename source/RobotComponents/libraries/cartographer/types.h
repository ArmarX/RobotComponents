/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cstdint>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <opencv2/core/mat.hpp>

#include <RobotAPI/interface/units/LaserScannerUnit.h>


namespace armarx::cartographer
{

    struct LocalSlamData
    {
        pcl::PointCloud<pcl::PointXYZ> local_points;
        pcl::PointCloud<pcl::PointXYZ> misses;

        // pose of the node in the local SLAM frame (-> trajectory pose)
        Eigen::Affine3f local_pose;
        int64_t timestamp;


        // pose of the trajectory relative to the world frame
        Eigen::Affine3f trajectory_pose;


        // pose of the node in the global SLAM frame (=> world frame)
        Eigen::Affine3f global_pose() const
        {
            return trajectory_pose * local_pose;
        }
    };

    struct OptimizedNodeData
    {
        Eigen::Quaternionf gravity_alignment;
        Eigen::Affine3f global_pose;
        Eigen::Affine3f local_pose;
        pcl::PointCloud<pcl::PointXYZ> node_cloud;

        cv::Mat grid_map;

        int node_id;

        int64_t timestamp;
    };

    struct ColoredMeshGrid
    {
        /* const */ Eigen::Array2i num_cells;

        using Color =  Eigen::Vector4i;
        using Position = Eigen::Vector3f;

        using ColorGrid = std::vector<std::vector<Color>>;
        using VertexGrid = std::vector<std::vector<Position>>;

        VertexGrid vertices;
        ColorGrid colors;

        ColoredMeshGrid(Eigen::Array2i numCells);
        ColoredMeshGrid() = default; // delete;

        void setColoredVertex(const Eigen::Array2i& idx, const Position& position,
                              const Color& color);

        Eigen::Vector3f innerSlicePosition = Eigen::Vector3f::Zero();

    };

    struct SubMapData
    {
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        //! the submap id within the trajectory
        int id;

        int trajectory_id;

        // pose in the local SLAM frame (within trajectory)
        Eigen::Affine3f slice_pose;

        // pose of the submap relative to the world frame (still need to consider slice_pose!)
        Eigen::Affine3f pose;


        ColoredMeshGrid mesh_grid;

        //! submap rendered as image
        //! channels: occupancy, mask (if cell is known)
        cv::Mat2b submap;

        // pose of the node in the global SLAM frame (=> world frame)
        Eigen::Affine3f global_pose() const
        {
            return pose * slice_pose;
        }

    };

    using SubMapDataVector = std::vector<SubMapData>;

    struct OptimizedGraphData
    {
        std::vector<OptimizedNodeData> nodes_data;
    };

    class LaserScannerSensorBase
    {
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    public:

        // SensorId id;
        std::string frame;
        Eigen::Affine3f pose;

        LaserScannerSensorBase(const std::string& frame, const Eigen::Affine3f& pose)
            :  frame(frame), pose(pose)
        {
        }

    };

    struct LaserScannerData
    {
        int64_t timestamp;
        pcl::PointCloud<pcl::PointXYZ> points;
    };

    struct LaserScannerMessage
    {
        std::string frame; // sensor frame
        LaserScan scan;

        //! timestamp in [ms]
        int64_t timestamp;
    };


}  // namespace armarx::cartographer