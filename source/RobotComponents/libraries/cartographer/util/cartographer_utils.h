/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <filesystem>
#include <memory>

#include <cartographer/common/lua_parameter_dictionary.h>
#include <cartographer/common/time.h>
#include <cartographer/mapping/map_builder_interface.h>

namespace armarx::cartographer
{
    /**
     * @brief Helper function to create Lua parameter object from string.
     *
     * Example:
     *
     *      const std::string mapBuilderLuaCode = R"text(
     *            include "map_builder.lua"
     *            return MAP_BUILDER)text";
     *
     *      auto paramsDict = resolveLuaParameters(mapBuilderLuaCode, ...);
     *
     *
     * @param luaCode The lua code that will be evaluated.
     * @param configPath The path where the lua config files reside.
     * @return std::unique_ptr<::cartographer::common::LuaParameterDictionary>
     */
    std::unique_ptr<::cartographer::common::LuaParameterDictionary>
    resolveLuaParameters(const std::string& luaCode, const std::filesystem::path& configPath);

    /**
     * @brief Convert cartographer time to unix time in [µs]
     *
     * @param time cartographer time
     * @return int64_t time in [µs]
     */
    int64_t fromCarto(::cartographer::common::Time time);


    /**
     * @brief Convert unix time in [µs] to cartographer time
     *
     * @param time in [µs]
     * @return ::cartographer::common::Time
     */
    ::cartographer::common::Time toCarto(const int64_t& time);

    /**
     * @brief Creates a map builder object from a stored map.
     *
     * @param mapPath The path to the map (e.g. /path/to/map.carto)
     * @param configPath The path where the lua config files reside.
     * @return std::unique_ptr<cartographer::mapping::MapBuilderInterface>
     */
    std::unique_ptr<::cartographer::mapping::MapBuilderInterface>
    loadMap(const std::filesystem::path& mapPath, const std::filesystem::path& configPath);


}  // namespace armarx::cartographer