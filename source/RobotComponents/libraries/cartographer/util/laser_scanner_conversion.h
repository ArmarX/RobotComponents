/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <algorithm>
#include <cmath>

#include <Eigen/Geometry>

#include <RobotAPI/interface/units/LaserScannerUnit.h>

namespace armarx
{

    template <typename EigenVectorT>
    EigenVectorT toCartesian(const LaserScanStep& laserScanStep)
    {
        EigenVectorT point = EigenVectorT::Identity();

        point.x() = -laserScanStep.distance * std::sin(laserScanStep.angle);
        point.y() = laserScanStep.distance * std::cos(laserScanStep.angle);

        return point;
    }

    template <typename EigenVectorT>
    std::vector<EigenVectorT> toCartesian(const armarx::LaserScan& laserScan)
    {
        std::vector<EigenVectorT> points;
        points.reserve(laserScan.size());

        std::transform(laserScan.begin(),
                       laserScan.end(),
                       std::back_inserter(points),
                       [](const LaserScanStep & pt)
        {
            return toCartesian<EigenVectorT>(pt);
        });

        return points;
    }

} // namespace armarx