/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <cartographer/sensor/rangefinder_point.h>

namespace armarx::cartographer
{
    ::pcl::PointCloud<::pcl::PointXYZ>
    toPCL(const std::vector<::cartographer::sensor::RangefinderPoint>& points);

    ::pcl::PointCloud<::pcl::PointXYZ>
    toPCL(const std::vector<::cartographer::sensor::TimedRangefinderPoint>& points);

    ::pcl::PointCloud<::pcl::PointXYZ>
    toPCL(const auto& pointCloud, const ::cartographer::transform::Rigid3d& pose);

    ::pcl::PointCloud<::pcl::PointXYZ>
    toPCL(const std::vector<::cartographer::sensor::TimedRangefinderPoint>& pointCloud,
          const Eigen::Affine3f& pose);

} // namespace armarx::cartographer