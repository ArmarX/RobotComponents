
#include "eigen_conversions.h"

#include <Eigen/Geometry>

namespace armarx::cartographer
{

    Eigen::Affine3f toEigen(const ::cartographer::transform::Rigid3d& pose)
    {
        Eigen::Affine3f eigenPose = Eigen::Affine3f::Identity();

        eigenPose.translation() = pose.translation().cast<float>();
        eigenPose.linear() = pose.rotation().cast<float>().toRotationMatrix();

        return eigenPose;
    }


    ::cartographer::transform::Rigid3d fromEigen(const Eigen::Affine3f& pose)
    {
        return ::cartographer::transform::Rigid3d(pose.translation().cast<double>(), Eigen::AngleAxisf(pose.linear()).cast<double>());
    }


} // namespace armarx::cartographer