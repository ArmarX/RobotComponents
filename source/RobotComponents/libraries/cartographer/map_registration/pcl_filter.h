/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <algorithm>
#include <functional>
#include <vector>

#include <pcl/point_cloud.h>

// To my future me: use std::erase_if (header: <vector>) @ c++20
// https://en.cppreference.com/w/cpp/container/vector/erase2
template <typename Predicate>
void erase_if(auto& c, Predicate pred)
{
    const auto it = std::remove_if(c.begin(), c.end(), pred);
    c.erase(it, c.end());
}

namespace armarx
{

    template <typename PointT>
    class Filter
    {
    public:
        virtual ~Filter()                                              = default;
        virtual void applyFilter(pcl::PointCloud<PointT>& cloud) const = 0;
    };

    namespace detail
    {

        template <typename PointT, typename OperatorT>
        class RangeFilter : virtual public armarx::Filter<PointT>
        {
        public:
            explicit RangeFilter(const float rangeThreshold) : rangeThreshold(rangeThreshold)
            {
            }

            void applyFilter(pcl::PointCloud<PointT>& cloud) const override
            {
                constexpr auto comp = OperatorT();

                const auto pred = [&](const PointT & point)
                {
                    return comp(rangeThreshold, Eigen::Vector2f(point.x, point.y).norm());
                };

                erase_if(cloud.points, pred);
            }

        private:
            const float rangeThreshold;
        };
    } // namespace detail

    template <typename PointT>
    using MinRangeFilter = detail::RangeFilter<PointT, std::greater<float>>;
    template <typename PointT>
    using MaxRangeFilter = detail::RangeFilter<PointT, std::less<float>>;

    namespace detail
    {

        template <typename PointT, typename OperatorT>
        class PointRangeFilter : virtual public armarx::Filter<PointT>
        {
        public:
            explicit PointRangeFilter(const float rangeThreshold, const Eigen::Vector3f& point) :
                rangeThreshold(rangeThreshold), point(point)
            {
            }

            void applyFilter(pcl::PointCloud<PointT>& cloud) const override
            {
                constexpr auto comp = OperatorT();

                const auto pred = [&](const PointT & pclPoint) -> bool
                {
                    const Eigen::Vector3f pt(pclPoint.x, pclPoint.y, pclPoint.z);
                    return comp(rangeThreshold, (pt - point).norm());
                };

                erase_if(cloud.points, pred);
            }

        private:
            const float rangeThreshold;
            const Eigen::Vector3f point;
        };
    } // namespace detail

    template <typename PointT>
    using PointMinRangeFilter = detail::PointRangeFilter<PointT, std::greater<float>>;
    template <typename PointT>
    using PointMaxRangeFilter = detail::PointRangeFilter<PointT, std::less<float>>;

} // namespace armarx