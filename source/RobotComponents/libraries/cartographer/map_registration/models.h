/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <wykobi.hpp>

#include "wykobi_types.h"

namespace armarx::wykobi
{

    struct Model
    {
        Eigen::Vector3f center;
        wykobi::Polygon polygon;
    };

    inline Model makeCube(const Eigen::Vector2f& position, const float width)
    {
        const float w2 = width / 2;

        std::vector<::wykobi::point2d<float>> pointList;
        pointList.push_back(::wykobi::make_point(position.x() + w2, position.y() + w2));
        pointList.push_back(::wykobi::make_point(position.x() - w2, position.y() + w2));
        pointList.push_back(::wykobi::make_point(position.x() - w2, position.y() - w2));
        pointList.push_back(::wykobi::make_point(position.x() + w2, position.y() - w2));

        return Model({.center  = Eigen::Vector3f{position.x(), position.y(), 0.F},
                      .polygon = ::wykobi::make_polygon(pointList)});
    }

} // namespace armarx::wykobi