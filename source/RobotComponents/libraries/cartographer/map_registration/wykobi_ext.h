/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <wykobi.hpp>

namespace armarx::wykobi::ext
{
    /**
    * Compared to wykobi::closest_point_on_polygon_from_point, this function also projects points inside the polygon onto the boundary
    */
    template <typename T>
    inline ::wykobi::point2d<T>
    closest_point_on_polygon_from_point(const ::wykobi::polygon<T, 2>& polygon,
                                        const ::wykobi::point2d<T>& point)
    {
        if (polygon.size() < 3)
        {
            return ::wykobi::degenerate_point2d<T>();
        }

        std::size_t j = polygon.size() - 1;
        T minDist     = std::numeric_limits<T>::max();

        ::wykobi::point2d<T> closestPoint = ::wykobi::degenerate_point2d<T>();

        for (std::size_t i = 0; i < polygon.size(); ++i)
        {
            ::wykobi::point2d<T> currPoint = ::wykobi::closest_point_on_segment_from_point(
                                                 ::wykobi::make_segment(polygon[i], polygon[j]), point);

            const T currDist = distance(point, currPoint);

            if (currDist < minDist)
            {
                closestPoint = currPoint;
                minDist      = currDist;
            }

            j = i;
        }

        return closestPoint;
    }

} // namespace armarx::wykobi::ext