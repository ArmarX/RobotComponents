/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <unordered_map>

#include <Eigen/Geometry>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "RobotComponents/libraries/cartographer/InterpolatingTimeQueue.h"
#include "RobotComponents/libraries/cartographer/map_registration/pcl_filter.h"

namespace armarx
{
    namespace armem
    {
        struct LaserScanStamped;
    }

    // struct LaserScanStamped;

    class LaserScanAggregator
    {
    public:
        using Point      = pcl::PointXYZ;
        using PointCloud = pcl::PointCloud<Point>;

        using SensorPoseMap  = std::unordered_map<std::string, Eigen::Affine3f>;
        using RobotPoseQueue = simox::math::InterpolatingTimeQueue<simox::math::PoseStamped>;

        LaserScanAggregator(const SensorPoseMap& sensorPoses,
                            const RobotPoseQueue& robotPoseQueue,
                            float minRange);

        void add(const armem::LaserScanStamped& scan);
        void add(const std::vector<armem::LaserScanStamped>& scans);

        const PointCloud::Ptr& pointCloud() const;

    private:
        const SensorPoseMap sensorPoses;
        const RobotPoseQueue robotPoseQueue;

        const MinRangeFilter<Point> rangeFilter;

        PointCloud::Ptr cloud;
    };
} // namespace armarx
