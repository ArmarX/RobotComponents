/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <set>

#include <Eigen/Geometry>

#include <RobotComponents/libraries/cartographer/InterpolatingTimeQueue.h>

#include "types.h"
#include "interfaces.h"

namespace armarx::cartographer
{
    struct LaserMessage
    {
        LaserScannerMessage data;
        Eigen::Affine3f robotPoseCorrection; // from pose where data was captured to timestamp pose
    };

    using LaserMessages = std::vector<LaserMessage>;

    struct TimedData
    {
        int64_t timestamp; // [µs]

        armarx::cartographer::PoseStamped odomPose;

        LaserMessages laserData;
    };


    /**
     * @brief The ApproximateTimeQueue class.
     *
     * Synchronizes incoming data and returns shapshots.
     *
     */
    class ApproximateTimeQueue
    {
    public:
        /**
        * @brief Construct a new Approximate Time Queue object
        *
        * @param dt [µs] time delay to limit processing of messages (inverse of frequency)
        * @param laserSensorIds the laser sensors that are used
        * @param messageCallee once messages are available, the data will be made available to the MessageCallee
        */
        ApproximateTimeQueue(int64_t dt, int64_t dtHistoryLength, const std::set<std::string>& laserSensorIds, MessageCallee& messageCallee);

        void insertLaserData(LaserScannerMessage message);
        void insertOdomData(armarx::cartographer::PoseStamped odomPose);


    private:

        bool allDataAvailable(int64_t timestamp) const;

        void processAvailableData(int64_t timestamp);
        void processAllAvailableData(int64_t timestamp);
        void trimUntil(const int64_t& timestamp);

        const int64_t dtEps; // [µs]
        const int64_t dtHistoryLength; // [µs]

        int64_t lastProcessedTimestamp{0};

        using LaserTimeQueue = armarx::cartographer::TimeQueue<LaserScannerMessage>;

        armarx::cartographer::InterpolatingTimeQueue<armarx::cartographer::PoseStamped> odomQueue;
        std::unordered_map<std::string, LaserTimeQueue> laserQueues;

        MessageCallee& messageCallee;

        std::mutex processingMtx;

    };

}  // namespace armarx::cartographer
