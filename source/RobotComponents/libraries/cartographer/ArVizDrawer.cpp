#include "RobotComponents/libraries/cartographer/ArVizDrawer.h"

#include "RobotAPI/libraries/core/FramedPose.h"

// STL
#include <algorithm>
#include <cmath>
#include <cstdint>
#include <iomanip>
#include <iterator>
#include <map>
#include <mutex>
#include <sstream>
#include <string>
#include <type_traits>

// Eigen
#include <Eigen/Geometry>

// PCL
#include <pcl/common/transforms.h>
#include <pcl/point_types.h>

// Simox

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/components/ArViz/Client/elements/Color.h>
#include <RobotAPI/components/ArViz/Client/elements/Mesh.h>
#include <RobotAPI/components/ArViz/Client/elements/PointCloud.h>

constexpr int mToMM{1000};

inline Eigen::Vector3f toMM(const Eigen::Vector3f& vec)
{
    return vec * 1000;
}

inline Eigen::Affine3f toMM(Eigen::Affine3f pose)
{
    pose.translation() *= 1000;
    return pose;
}

namespace armarx::cartographer
{

    ArVizDrawer::ArVizDrawer(armarx::viz::Client& arviz, const Eigen::Affine3f& world_T_map) :
        arviz(arviz),
        trajectoryLayer(arviz.layer("localization_trajectory")),
        world_T_map(world_T_map)
    {
    }

    ArVizDrawer::~ArVizDrawer()
    {
        // make sure, the Arviz Ice calls are handled.
        // {
        //     std::unique_lock g{layerMtx};
        //     g.lock();
        // }
    }

    inline std::string
    nameWithId(const std::string& name, const int id, const unsigned int idWidth = 6)
    {
        std::stringstream ss;
        ss << name << "_" << std::setw(static_cast<int>(idWidth)) << std::setfill('0') << id;
        return ss.str();
    }

    inline std::string nameWithIds(const std::string& name,
                                   const int id,
                                   const int subId,
                                   const unsigned int idWidth = 6)
    {
        std::stringstream ss;
        ss << name << "_" << std::setw(static_cast<int>(idWidth)) << std::setfill('0') << id << "_"
           << std::setw(static_cast<int>(idWidth)) << std::setfill('0') << subId;
        return ss.str();
    }

    void mToMM(pcl::PointXYZ& point)
    {
        point.x *= 1000;
        point.y *= 1000;
        point.z *= 1000;
    }

    void mToMMWithOffset(pcl::PointXYZ& point)
    {
        mToMM(point);

        point.z += 30; // slightly above the ground
    }

    void trimToLatest(auto& vec, const unsigned int size)
    {

        if (vec.size() <= size)
        {
            return;
        }

        const auto nElementsToRemove = vec.size() - size;
        vec.erase(vec.begin(), vec.begin() + nElementsToRemove);
    }

    void ArVizDrawer::onOdomPose(const Eigen::Affine3f& odomPose)
    {
        auto odomLayer = arviz.layer("odom");
        odomLayer.add(viz::Pose("odom").pose(odomPose.translation() * 1000, odomPose.linear()));

        {
            std::lock_guard guard{layerMtx};
            arviz.commit(odomLayer);
        }
    }

    void ArVizDrawer::onLocalSlamData(const LocalSlamData& data)
    {
        ARMARX_TRACE;
        // auto originLayer = arviz.layer("origin");
        // originLayer.add(viz::Pose("origin").scale(5.F));

        // {
        //     std::lock_guard guard{layerMtx};
        //     arviz.commit(originLayer);
        // }

        // localization trajectory
        // static unsigned int poseId{0};

        const Eigen::Affine3f map_T_robot = toMM(data.global_pose());

        latestWorldToRobotPose = world_T_map * map_T_map_correct * map_T_robot;

        // auto trajRootLayer = arviz.layer("trajectory_root");
        // trajRootLayer.add(viz::Pose("traj").pose(data.trajectory_pose.translation(), data.trajectory_pose.linear()));

        // {
        //     std::lock_guard guard{layerMtx};
        //     arviz.commit(trajRootLayer);
        // }

        auto framesLayer = arviz.layer("frames_dynamic");
        framesLayer.add(viz::Pose(MapFrame).pose(world_T_map));
        framesLayer.add(viz::Pose(MapFrame + "Correction").pose(world_T_map * map_T_map_correct));
        framesLayer.add(viz::Pose("robot").pose(*latestWorldToRobotPose));
        framesLayer.add(viz::Arrow(MapFrame + "Correction" + "/" + "robot")
                        .fromTo((world_T_map * map_T_map_correct).translation(),
                                latestWorldToRobotPose->translation()));

        {
            std::lock_guard guard{layerMtx};
            arviz.commit(framesLayer);
        }

        // trajectoryLayer.add(viz::Pose(nameWithId("pose", poseId++))
        //                     .pose(latestWorldToRobotPose->translation(), latestWorldToRobotPose->linear()));

        // trimToLatest(trajectoryLayer.data_.elements, 100);

        // {
        //     std::lock_guard guard{layerMtx};
        //     arviz.commit(trajectoryLayer);
        // }

        const auto worldToTrajectoryPose =
            world_T_map * map_T_map_correct * toMM(data.trajectory_pose);

        // currently visible points
        {
            auto localPointsLayer = arviz.layer("local_points_layer");

            auto localPoints = data.local_points;
            std::for_each(localPoints.points.begin(), localPoints.end(), mToMMWithOffset);

            auto pc =
                viz::PointCloud("range_points")
                .pointSizeInPixels(10)
                .pointCloud(localPoints, viz::Color(245, 86, 75))
                .pose(worldToTrajectoryPose.translation(), worldToTrajectoryPose.linear());

            localPointsLayer.add(pc);

            {
                std::lock_guard guard{layerMtx};
                arviz.commit(localPointsLayer);
            }
        }

        // unmatched points
        {
            auto unmatchedPointsLayer = arviz.layer("unmatched_points");

            auto missesPoints = data.misses;
            std::for_each(missesPoints.points.begin(), missesPoints.points.end(), mToMMWithOffset);

            auto pcMisses =
                viz::PointCloud("range_points")
                .pointSizeInPixels(10)
                .pointCloud(missesPoints, viz::Color(75, 237, 245))
                .pose(worldToTrajectoryPose.translation(), worldToTrajectoryPose.linear());

            unmatchedPointsLayer.add(pcMisses);

            {
                std::lock_guard guard{layerMtx};
                arviz.commit(unmatchedPointsLayer);
            }
        }
    }

    void ArVizDrawer::onLaserSensorData(const LaserScannerData& laserData)
    {
        ARMARX_TRACE;
        // this function draws the raw laser scanner data.
        // this is only meant for debugging purpose
        return;

        // latestWorldToRobotPose should be set once the initial localization pose is available.
        if (not latestWorldToRobotPose)
        {
            return;
        }

        if (laserData.points.points.empty())
        {
            ARMARX_WARNING << "Empty point cloud received";
        }

        auto laserScannerData = laserData; // copy

        // currently visible points
        auto rawLaserPoints = arviz.layer("laser_sensor_data");

        std::for_each(
            laserScannerData.points.points.begin(), laserScannerData.points.points.end(), mToMM);

        auto pc = viz::PointCloud("range_points");
        pc.pointSizeInPixels(10)
        .pointCloud(laserScannerData.points, viz::Color::red(200))
        .pose(latestWorldToRobotPose->translation(), latestWorldToRobotPose->linear());

        rawLaserPoints.add(pc);
        rawLaserPoints.add(viz::Pose("").pose(latestWorldToRobotPose->translation(),
                                              latestWorldToRobotPose->linear()));

        {
            std::lock_guard guard{layerMtx};
            arviz.commit(rawLaserPoints);
        }
    }

    void ArVizDrawer::setMapCorrection(const Eigen::Affine3f& mapCorrection)
    {
        ARMARX_TRACE;
        std::lock_guard guard{layerMtx};
        this->map_T_map_correct = mapCorrection;
    }
    void ArVizDrawer::setWorldToMapTransform(const Eigen::Affine3f& world_T_map)
    {
        ARMARX_TRACE;
        std::lock_guard g{layerMtx};
        this->world_T_map = world_T_map;
    }
} // namespace armarx::cartographer
