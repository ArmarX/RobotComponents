
set(LIB_NAME ${PROJECT_NAME}Cartographer)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

# cartographer depends on absl. if you remove the find_package(absl, ...), then find_package(cartographer) likely fails.
find_package(absl QUIET)
find_package(cartographer QUIET)

find_package(PCL QUIET COMPONENTS io common)
find_package(OpenCV QUIET COMPONENTS core imgcodecs)

find_package(Eigen3 QUIET)

armarx_build_if(absl_FOUND "absl not available")
armarx_build_if(cartographer_FOUND "cartographer not available")
armarx_build_if(PCL_FOUND "PCL not available")
armarx_build_if(OpenCV_FOUND "OpenCV not available")
armarx_build_if(Eigen3_FOUND "OpenCV not available")


# add_subdirectory(util)

armarx_add_library(
        LIBS
                # ArmarX
                ArViz
                ${PROJECT_NAME}Interfaces 
                RobotAPICore
                ArmarXCoreLogging
                ArmarXCore
                ArmarXCoreInterfaces
                RobotComponentsInterfaces
                # external deps
                cartographer
                Eigen3::Eigen
                ${OpenCV_LIBS} # to my future me: opencv::core
                ${PCL_LIBRARIES}
        SOURCES
                ./CartographerAdapter.cpp
                ./MessageQueue.cpp
                ./ArVizDrawerMapBuilder.cpp
                ./ArVizDrawer.cpp
                ./InterpolatingTimeQueue.cpp
                ./ApproximateTimeQueue.cpp
                ./grid_conversion.cpp
                ./utils.cpp
                # ./FrequencyReporter.cpp
                #$<TARGET_OBJECTS:CartographerUtil>
                util/cartographer_utils.cpp
                util/eigen_conversions.cpp
                util/pcl_conversions.cpp
        HEADERS
                ./CartographerAdapter.h
                ./MessageQueue.h
                ./ArVizDrawerMapBuilder.h
                ./ArVizDrawer.h
                ./InterpolatingTimeQueue.h
                ./ApproximateTimeQueue.h
                ./grid_conversion.h
                ./utils.h
                # ./FrequencyReporter.h
                # utils
                util/cartographer_utils.h
                util/eigen_conversions.h
                util/pcl_conversions.h
)

# SYSTEM include silences several compiler warnings
target_include_directories(${PROJECT_NAME}Cartographer 
        SYSTEM PUBLIC 
                ${PCL_INCLUDE_DIRS} 
                ${OpenCV_INCLUDE_DIRS}
)

# target_compile_options(CartographerLib
#         PUBLIC 
#                 ${PCL_DEFINITIONS}
# )

add_library(RobotComponents::Cartographer ALIAS ${PROJECT_NAME}Cartographer)

##########################


armarx_add_test(
        TEST_NAME 
                ApproximateTimeQueueTest
        TEST_FILE
                test/approximateTimeQueueTest.cpp
        LIBS
                RobotComponents::Cartographer    
)
