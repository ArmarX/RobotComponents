/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::core
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <algorithm>
#include <atomic>
#include <cstdint>
#include <thread>
#include <vector>

#include <boost/test/tools/old/interface.hpp>

#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/interface/units/LaserScannerUnit.h>

#include "RobotComponents/libraries/cartographer/interfaces.h"
#include <RobotComponents/libraries/cartographer/ApproximateTimeQueue.h>


// test includes and other stuff
#define BOOST_TEST_MODULE RobotComponents::ArmarXLibraries::cartographer
#define ARMARX_BOOST_TEST

#include <RobotComponents/Test.h>


BOOST_AUTO_TEST_CASE(testConcurrency)
{
    const int N = 1'000;


    class MessageCalleeDummy : public armarx::cartographer::MessageCallee
    {
    public:
        int messagesProcessed{0};

        std::vector<int64_t> timestamps;

        void
        onTimedDataAvailable(const armarx::cartographer::TimedData& data) override
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            messagesProcessed++;

            // timestamps should increase
            if (not timestamps.empty())
            {
                BOOST_CHECK_GT(data.timestamp, timestamps.back());
            }

            timestamps.push_back(data.timestamp);
        };
    };

    MessageCalleeDummy dummy;

    armarx::cartographer::ApproximateTimeQueue timeQueue(
        -1, -1, std::set<std::string>{"front", "back"}, dummy);

    std::atomic_bool runOdomTask;
    runOdomTask.store(true);

    const auto insertOdomData = [&]()
    {
        while (runOdomTask)
        {
            armarx::cartographer::PoseStamped odomPose{Eigen::Affine3f::Identity(),
                                                  IceUtil::Time::now().toMicroSeconds()};
            timeQueue.insertOdomData(odomPose);
        }

        ARMARX_INFO << "insertOdomData finished";
    };

    const auto insertLaserData1 = [&]()
    {
        armarx::LaserScan scan(1000);
        //std::fill(scan.begin(), scan.end(),
        //            armarx::LaserScanStep{.angle = 0.F, .distance = 0.F, .intensity = 0.F});

        for (int i = 0; i < N; i++)
        {
            armarx::cartographer::LaserScannerMessage laserScan{
                "front", scan, armarx::Clock::Now().toMicroSecondsSinceEpoch()};
            timeQueue.insertLaserData(laserScan);
        }

        ARMARX_INFO << "insertLaserData1 finished";
    };

    const auto insertLaserData2 = [&]()
    {
        armarx::LaserScan scan(1000);
        //std::fill(scan.begin(), scan.end(),
        //            armarx::LaserScanStep{.angle = 0.F, .distance = 0.F, .intensity = 0.F});

        while (runOdomTask)
        {
                armarx::cartographer::LaserScannerMessage laserScan{
                    "back", scan, armarx::Clock::Now().toMicroSecondsSinceEpoch()};
            timeQueue.insertLaserData(laserScan);
        }

        ARMARX_INFO << "insertLaserData2 finished";
    };

    ARMARX_INFO << "Starting threads";

    std::thread laserInsertThread1(insertLaserData1);
    std::thread laserInsertThread2(insertLaserData2);
    std::thread odomInsertThread(insertOdomData);

    ARMARX_INFO << "Waiting for threads to terminate";
    laserInsertThread1.join();
    runOdomTask.store(false);

    laserInsertThread2.join();
    odomInsertThread.join();

    ARMARX_INFO << dummy.messagesProcessed << " messages processed";
    BOOST_CHECK_GT(dummy.messagesProcessed, 0);

    BOOST_CHECK_LE(dummy.messagesProcessed, N);

    ARMARX_INFO << "Done.";

}
