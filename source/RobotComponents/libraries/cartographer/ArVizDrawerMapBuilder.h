/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Geometry>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <RobotAPI/components/ArViz/Client/ScopedClient.h>

namespace cartographer::mapping
{
    class MapBuilderInterface;
} // namespace cartographer::mapping

namespace armarx::cartographer
{

    /**
     * @brief The ArVizDrawerMapBuilder class.
     *
     * Draws
     *  - the pose graph
     *  - the occupancy grid map
     *  - the frames (global & map)
     */
    class ArVizDrawerMapBuilder // : public armarx::viz::ScopedClient
    {

    public:
        ArVizDrawerMapBuilder(armarx::viz::Client& arviz, ::cartographer::mapping::MapBuilderInterface& mapBuilder, const Eigen::Affine3f& world_T_map);
        ~ArVizDrawerMapBuilder();
    protected:
        void draw();

    private:
        void run();

        armarx::viz::Client& arviz;

        void drawOptimizedPoseGraph();
        void drawGridMap();
        void drawFrames();

        ::cartographer::mapping::MapBuilderInterface& mapBuilder;

        RunningTask<ArVizDrawerMapBuilder>::pointer_type runningTask;

        const Eigen::Affine3f world_T_map;

    };


}  // namespace armarx::cartographer
