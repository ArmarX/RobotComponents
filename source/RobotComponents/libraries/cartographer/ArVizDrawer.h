/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <Eigen/Geometry>
#include <Eigen/src/Geometry/Transform.h>

#include <cartographer/mapping/map_builder_interface.h>

#include "RobotAPI/components/ArViz/Client/ScopedClient.h"
#include <RobotAPI/components/ArViz/Client/ScopedClient.h>
#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/components/ArViz/Client/elements/PointCloud.h>

#include <RobotComponents/libraries/cartographer/interfaces.h>
#include <RobotComponents/libraries/cartographer/types.h>


namespace cartographer::mapping
{
    class MapBuilderInterface;
} // namespace cartographer::mapping

namespace armarx
{
    // forward declaration
    namespace viz
    {
        class Client;
    }

    namespace cartographer
    {

        class ArVizDrawer : virtual public SlamDataCallable
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW

            ArVizDrawer(armarx::viz::Client& arviz, const Eigen::Affine3f& world_T_map);
            virtual ~ArVizDrawer();

            // SlamDataCallable interface
            void onLocalSlamData(const LocalSlamData& slamData) override;
            // void onGraphOptimized(const OptimizedGraphData& graphData) override;
            // void onGridMap(const SubMapDataVector& submapData) override;
            void onLaserSensorData(const LaserScannerData& laserData) override;

            void onOdomPose(const Eigen::Affine3f& odomPose);

            void setMapCorrection(const Eigen::Affine3f& mapCorrection);
            void setWorldToMapTransform(const Eigen::Affine3f& world_T_map);

        private:
            viz::Client& arviz;

            viz::Layer trajectoryLayer;

            std::optional<Eigen::Affine3f> latestWorldToRobotPose;

            struct LaserVisuData
            {
                std::shared_ptr<viz::PointCloud> cloud; // already in robot frame!
                Eigen::Affine3f world_T_sensor = Eigen::Affine3f::Identity();
            };

            std::mutex layerMtx;
            Eigen::Affine3f world_T_map;

            Eigen::Affine3f map_T_map_correct = Eigen::Affine3f::Identity();
        };

    } // namespace cartographer

} // namespace armarx
