#include "utils.h"

#include <cartographer/mapping/2d/submap_2d.h>
#include <cartographer/mapping/map_builder.h>

#include "ArmarXCore/core/logging/Logging.h"

#include "grid_conversion.h"
#include "util/cartographer_utils.h"
#include "util/eigen_conversions.h"
#include "util/pcl_conversions.h"

namespace armarx::cartographer::utils
{

    OptimizedGraphData optimizedGraphData(::cartographer::mapping::MapBuilderInterface& mapBuilder)
    {
        const auto trajectoryNodes = mapBuilder.pose_graph()->GetTrajectoryNodes();

        OptimizedGraphData optimizedGraphData;
        optimizedGraphData.nodes_data.reserve(trajectoryNodes.size());

        std::transform(trajectoryNodes.begin(),
                       trajectoryNodes.end(),
                       std::back_inserter(optimizedGraphData.nodes_data),
                       [&](const ::cartographer::mapping::MapById <
                           ::cartographer::mapping::NodeId,
                           ::cartographer::mapping::TrajectoryNode >::IdDataReference & idRef)
        {
            const auto& nodeData = idRef.data;

            OptimizedNodeData el;
            const auto data = nodeData.constant_data; // hold ref to shared ptr

            el.node_id     = idRef.id.node_index;
            el.global_pose = toEigen(nodeData.global_pose);
            el.node_cloud =
                toPCL(data->filtered_gravity_aligned_point_cloud.points());
            el.local_pose = toEigen(data->local_pose);
            el.timestamp  = fromCarto(nodeData.constant_data->time);

            return el;
        });

        return optimizedGraphData;
    }

    SubMapDataVector submapData(::cartographer::mapping::MapBuilderInterface& mapBuilder,
                                const int trajectoryId)
    {
        SubMapDataVector submapDataOut;

        const auto graphSubmapData = mapBuilder.pose_graph()->GetAllSubmapData();

        for (const auto& [submap_id, submap_data] : graphSubmapData)
        {
            ARMARX_DEBUG << "Submap id: " << std::to_string(submap_id.submap_index);

            // FIXME enable
            if (submap_id.trajectory_id != 0) // trajectoryId)
            {
                ARMARX_DEBUG << "Dropping submap as trajectory id does not match.";
                continue;
            }

            // if (submap_id.submap_index != 0)
            // {
            //     continue;
            // }

            const std::shared_ptr<const ::cartographer::mapping::Submap> submap =
                submap_data.submap;
            const auto* const submap2d =
                dynamic_cast<const ::cartographer::mapping::Submap2D*>(submap.get());

            if (submap2d != nullptr)
            {
                SubMapData data;
                data.id            = submap_id.submap_index;
                data.trajectory_id = submap_id.trajectory_id;

                data.mesh_grid = toMeshGrid(*submap2d->grid());
                data.submap    = toImage(*submap2d->grid());

                data.pose =
                    toEigen(mapBuilder.pose_graph()->GetAllSubmapPoses().at(submap_id).pose);
                data.slice_pose = toEigen(submap2d->local_pose()).inverse() *
                                  Eigen::Translation3f(data.mesh_grid.innerSlicePosition);

                submapDataOut.emplace_back(std::move(data));
            }
            else
            {
                ARMARX_WARNING << "Cannot cast to Submap2D. Check the implementation!";
            }
        }
        return submapDataOut;
    }

    std::vector<armem::vision::OccupancyGrid> occupancyGrids(::cartographer::mapping::MapBuilderInterface& mapBuilder,
            int trajectoryId)

    {
        return {};

        std::vector<armem::vision::OccupancyGrid> occs;

        const auto graphSubmapData = mapBuilder.pose_graph()->GetAllSubmapData();

        for (const auto& [submap_id, submap_data] : graphSubmapData)
        {
            ARMARX_DEBUG << "Submap id: " << std::to_string(submap_id.submap_index);

            // FIXME enable
            if (submap_id.trajectory_id != 0) // trajectoryId)
            {
                ARMARX_DEBUG << "Dropping submap as trajectory id does not match.";
                continue;
            }

            // if (submap_id.submap_index != 0)
            // {
            //     continue;
            // }

            const std::shared_ptr<const ::cartographer::mapping::Submap> submap =
                submap_data.submap;
            const auto* const submap2d =
                dynamic_cast<const ::cartographer::mapping::Submap2D*>(submap.get());

            if (submap2d != nullptr)
            {
                auto occ = toOccupancyGrid(*submap2d->grid());
                
                const auto map_T_submap =
                    toEigen(mapBuilder.pose_graph()->GetAllSubmapPoses().at(submap_id).pose);

                const auto submap_T_submap_crop = toEigen(submap2d->local_pose()).inverse() *
                                  occ.pose;

                occ.pose = map_T_submap * submap_T_submap_crop;

                occs.emplace_back(occ);
            }
            else
            {
                ARMARX_WARNING << "Cannot cast to Submap2D. Check the implementation!";
            }
        }
        return occs;
    }
} // namespace armarx::cartographer::utils
