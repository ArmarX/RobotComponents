#include "ApproximateTimeQueue.h"

#include <algorithm>
#include <cstdint>
#include <mutex>

#include <ArmarXCore/core/exceptions/LocalException.h>

#include "RobotComponents/libraries/cartographer/interfaces.h"


namespace armarx::cartographer
{

    ApproximateTimeQueue::ApproximateTimeQueue(const int64_t dt, const int64_t dtHistoryLength, const std::set<std::string>& laserSensorIds, MessageCallee& messageCallee)
        : dtEps(std::max<int64_t>(dt, 0)), dtHistoryLength(dtHistoryLength), messageCallee(messageCallee)
    {
        for (const auto& laserSensorId : laserSensorIds)
        {
            laserQueues.insert({laserSensorId, LaserTimeQueue()});
        }
    }


    void ApproximateTimeQueue::insertLaserData(LaserScannerMessage message)
    {
        std::lock_guard g{processingMtx};
        // ARMARX_INFO << "Laser data " << message->frame << " ts: " << message->timestamp;

        // ARMARX_CHECK(laserQueues.count(message.frame) > 0);
        LaserTimeQueue& queue = laserQueues.at(message.frame);

        try {
            queue.insert(std::move(message));
            processAllAvailableData(message.timestamp);
        } catch (const armarx::LocalException& e) {
            ARMARX_WARNING << "Unable to insert message into queue. Reason: " << e.what();
            queue.clear();
        }
        
    }


    void ApproximateTimeQueue::insertOdomData( armarx::cartographer::PoseStamped odomPose)
    {
        // ARMARX_INFO << "odom data ts:" << odomPose->timestamp;
        std::lock_guard g{processingMtx};

        const auto timestamp = odomPose.timestamp;

        odomQueue.insert(std::move(odomPose));
        processAllAvailableData(timestamp);
    }

    bool ApproximateTimeQueue::allDataAvailable(const int64_t timestamp) const
    {
        auto hasTimestamp = [timestamp](const auto & q)
        {
            return q.has(timestamp);
        };

        if (not hasTimestamp(odomQueue))
        {
            // ARMARX_INFO << "odom data missing";
            return false;
        }

        // TODO(fabian.reister): all_of(..., hasTimestamp)
        for (const auto& [sensorName, sensorQueue] : laserQueues)
        {
            if (not hasTimestamp(sensorQueue))
            {
                // ARMARX_INFO << sensorName << " data missing";

                return false;
            }
        }

        return true;
    }

    void ApproximateTimeQueue::processAllAvailableData(const int64_t timestamp)
    {
        ARMARX_TRACE;

        // This is inspired by ROS' approximate time synchronizer, but a bit simpler.
        // It works as follows:
        // (1) Try to find a pivot element. That is an element that has matches in other queues.
        //     The pivot's timestamp is larger than the timestamp of the matches.
        // (2) Start at the beginning of the queues, traverse them and check for matches
        // (3) Once a match is found, it is processed.
        // (4) All messages older than the pivot element are deleted.

        std::set<int64_t> timestamps;

        const auto insertTimestamps = [&timestamps](const std::vector<int64_t>& ts)
        {
            std::copy(ts.begin(), ts.end(), std::inserter(timestamps, timestamps.end()));
        };

        // insertTimestamps(odomQueue.timestamps());

        for (const auto& [_, q] : laserQueues)
        {
            insertTimestamps(q.timestamps());
        }

        // std::set is sorted :)

        // this will now go through the queues. whenever a match is found, it is processed
        // and older messages are dropped.
        std::for_each(timestamps.begin(), timestamps.end(), [&](const auto & ts)
        {
            processAvailableData(ts);
        });

    }

    void ApproximateTimeQueue::processAvailableData(const int64_t timestamp)
    {
        ARMARX_TRACE;

        // return; // TODO remove
        const int64_t dt = timestamp - lastProcessedTimestamp;

        // only process data at a certain frequency
        if (dt <= dtEps)
        {
            return;
        }

        // process no messages that are too old
        // trimUntil(timestamp - dtHistoryLength);

        if (not allDataAvailable(timestamp))
        {
            return;
        }

        ARMARX_DEBUG << "Processing approximate time data " << timestamp;

        lastProcessedTimestamp = timestamp;

        try
        {
            ARMARX_TRACE;
            const auto& odom_T_lookup = odomQueue.lookupInterpolate(timestamp).pose;

            TimedData timedData;
            timedData.timestamp = timestamp;

            timedData.odomPose.timestamp = timestamp;
            timedData.odomPose.pose = odom_T_lookup;

            for (const auto& [sensorName, sensorQueue] : laserQueues)
            {
                ARMARX_TRACE;
                const auto& message = sensorQueue.lookupAt(timestamp);

                // The lookup timestamp and the message timestamp do not match exaclty.
                // Therefore, we use the odometry information to "project" the sensor data.
                const auto odom_T_measurement = odomQueue.lookupInterpolate(message.timestamp).pose;
                const Eigen::Affine3f measurement_T_lookup = odom_T_measurement.inverse() * odom_T_lookup;

                LaserMessage laserData;
                laserData.data = message;
                laserData.robotPoseCorrection = measurement_T_lookup;

                timedData.laserData.push_back(laserData);
            }

            ARMARX_TRACE;
            messageCallee.onTimedDataAvailable(timedData);

            // house keeping
            trimUntil(timestamp);

        }
        catch (...)
        {
            return;
        }

    }

    void ApproximateTimeQueue::trimUntil(const int64_t& timestamp)
    {
        ARMARX_TRACE;

        odomQueue.trimUntil(timestamp - 1'000'000);
        for (auto& [sensorName, sensorQueue] : laserQueues)
        {
            sensorQueue.trimUntil(timestamp);
        }
    }

}  // namespace armarx::cartographer
