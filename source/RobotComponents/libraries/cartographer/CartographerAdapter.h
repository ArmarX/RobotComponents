/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cstdint>
#include <memory>
#include <vector>
#include <filesystem>
#include <optional>
#include <mutex>

#include <opencv2/core/mat.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <cartographer/mapping/trajectory_builder_interface.h>

#include <RobotAPI/interface/units/LaserScannerUnit.h>

#include <RobotComponents/libraries/cartographer/types.h>
#include <RobotComponents/libraries/cartographer/interfaces.h>

namespace cartographer::mapping
{
    class MapBuilderInterface;
} // namespace cartographer


namespace armarx::cartographer
{
    struct PoseStamped;
}

namespace armarx::cartographer
{
    class ApproximateTimeQueue;
    struct LaserMessage;


    class CartographerAdapter: virtual public MessageCallee
    {
    public:

        struct Config
        {
            bool useOdometry{false};
            bool useLaserScanner{true};
            bool useImu{false};

            float frequency{10.F};

            std::set<std::string> laserScanners{"LaserScannerFront", "LaserScannerBack"};
        };

        CartographerAdapter(
            const std::filesystem::path& mapPath,
            const std::filesystem::path& configPath,
            SlamDataCallable& slamDataCallable,
            const Config& config,
            const std::optional<std::filesystem::path>& mapToLoad = std::nullopt,
            const std::optional<Eigen::Affine3f>& map_T_robot_prior = std::nullopt
        );

        virtual ~CartographerAdapter();

        void processSensorValues(LaserScannerMessage data);

        void onTimedDataAvailable(const TimedData& data) override;


        struct OdomData
        {
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Eigen::Vector3f position;
            Eigen::Quaternionf orientation;

            //! timestamp in [ms]
            int64_t timestamp;
        };

        void processOdometryPose(OdomData odomData);

        using SensorId =
            ::cartographer::mapping::TrajectoryBuilderInterface::SensorId;


        class LaserScannerSensor : public LaserScannerSensorBase
        {
        public:
            SensorId id;

            [[nodiscard]] auto origin() const -> Eigen::Vector3f
            {
                return pose.translation();
            }

            explicit LaserScannerSensor(const SensorId& sensorId, Eigen::Affine3f pose)
                : LaserScannerSensorBase(sensorId.id, pose), id(sensorId) {}
        };


        OptimizedGraphData optimizedGraphData() const;

        bool createMap();
        void setSensorPose(const std::string& sensorId, const Eigen::Affine3f& pose);
        bool hasReceivedSensorData() const noexcept;

        std::vector<std::string> laserSensorIds() const;

        // void triggerCallbacks() const;

        ::cartographer::mapping::MapBuilderInterface& getMapBuilder()
        {
            return *mapBuilder;
        }

    private:
        void pushInOdomData(const armarx::cartographer::PoseStamped& odomPose);
        void pushInLaserData(const std::vector<LaserMessage>& laserMessages);

        // laser scanners will be merged
        const SensorId sensorIdLaser{SensorId::SensorType::RANGE, "laser"};
        const SensorId sensorIdIMU{SensorId::SensorType::IMU, "imu"};
        const SensorId sensorIdOdom{SensorId::SensorType::ODOMETRY, "odom"};

        const LaserScannerSensor& lookupSensor(const std::string& device) const;

        void
        getLocalSlamResultCallback(const int /*trajectory_id*/,
                                   const ::cartographer::common::Time /*time*/,
                                   const ::cartographer::transform::Rigid3d localPose,
                                   ::cartographer::sensor::RangeData rangeDataInLocal,
                                   const std::unique_ptr<const ::cartographer::mapping::
                                   TrajectoryBuilderInterface::InsertionResult>
                                   insertionResult);

        std::unique_ptr<::cartographer::mapping::MapBuilderInterface> mapBuilder;

        std::map<std::string, int64_t> lastSensorDataTimestamp;

        const Config config;

        int trajectoryId{0};

        std::filesystem::path mapPath;

        std::set<SensorId> sensorSet;

        SlamDataCallable& slamDataCallable;

        ::cartographer::mapping::proto::TrajectoryBuilderOptions trajectoryBuilderOptions;

        mutable std::mutex cartoInsertMtx;

        std::unique_ptr<ApproximateTimeQueue> approxTimeQueue;

        std::unordered_map<std::string, LaserScannerSensor> laserSensorMap;



    };
} // namespace armarx::cartographer
