/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "RobotComponents/libraries/cartographer/types.h"

#include "RobotAPI/libraries/armem_vision/types.h"
#include <RobotAPI/libraries/armem_vision/types.h>

namespace cartographer::mapping
{
    class MapBuilderInterface;
}

namespace armarx::cartographer::utils
{

    OptimizedGraphData optimizedGraphData(::cartographer::mapping::MapBuilderInterface& mapBuilder);

    SubMapDataVector submapData(::cartographer::mapping::MapBuilderInterface& mapBuilder, int trajectoryId);

    // armem::vision::OccupancyGrid occupancyGrid(::cartographer::mapping::MapBuilderInterface& mapBuilder, int trajectoryId);
    std::vector<armem::vision::OccupancyGrid> occupancyGrids(::cartographer::mapping::MapBuilderInterface& mapBuilder, int trajectoryId);

}  // namespace armarx::cartographer::utils
