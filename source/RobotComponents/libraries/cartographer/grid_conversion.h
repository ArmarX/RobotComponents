/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <Eigen/Geometry>

#include <opencv2/core/mat.hpp>

#include "RobotAPI/libraries/armem_vision/types.h"

#include <RobotComponents/libraries/cartographer/types.h>

namespace cartographer::mapping
{
    class Grid2D;
    class ProbabilityGrid;
} // namespace cartographer::mapping

namespace armarx::cartographer
{
    cv::Mat2b toImage(const ::cartographer::mapping::Grid2D& grid);

    ColoredMeshGrid toMeshGrid(const ::cartographer::mapping::Grid2D& grid);

    armem::vision::OccupancyGrid toOccupancyGrid(const ::cartographer::mapping::Grid2D& grid);
    armem::vision::OccupancyGrid toOccupancyGrid(const ::cartographer::mapping::ProbabilityGrid& grid);

} // namespace armarx::cartographer
