#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <RobotComponents/interface/components/GraspingManager/GraspGeneratorInterface.h>


namespace armarx
{
    namespace plugins
    {

        class GraspGeneratorComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            GraspGeneratorInterfacePrx getGraspGenerator();

        private:
            static constexpr const char* PROPERTY_NAME = "GraspGeneratorName";
            GraspGeneratorInterfacePrx _graspGenerator;
        };

    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use GraspGenerator.
     */
    class GraspGeneratorComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        GraspGeneratorComponentPluginUser();
        GraspGeneratorInterfacePrx getGraspGenerator();

    private:
        armarx::plugins::GraspGeneratorComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using GraspGeneratorComponentPluginUser = armarx::GraspGeneratorComponentPluginUser;
    }
}
