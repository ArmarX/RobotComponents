armarx_component_set_name("GraspingManagerApp")

set(COMPONENT_LIBS
    GraspingManager
)


set(EXE_SOURCE main.cpp GraspingManagerApp.h)

armarx_add_component_executable("${EXE_SOURCE}")
