armarx_component_set_name("CollisionCheckerApp")

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    CollisionCheckerComponent
)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
