/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::View::Perspectives
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef PERSPECTIVES_H
#define PERSPECTIVES_H


#include <RobotComponents/gui-plugins/RobotTrajectoryDesignerGuiPlugin/View/ui_Perspectives.h>
#include <QWidget>
#include <QListWidget>
#include <memory>

namespace Ui
{
    class Perspectives;
}

class Perspectives : public QWidget
{
    Q_OBJECT

public:
    explicit Perspectives(QWidget* parent = 0);
    ~Perspectives();

    Ui::Perspectives* getPerspectives();
    void setPerspectives(Ui::Perspectives* perspectives);

private:
    Ui::Perspectives* perspectives;
};

using PerspectivesPtr = std::shared_ptr<Perspectives>;

#endif // PERSPECTIVES_H
