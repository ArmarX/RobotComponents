
/* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
* @author     Timo Birr
* @date       2018
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef VisualizationSubject_H
#define VisualizationSubject_H

#include "VisualizationObserver.h"

#include <Eigen/Eigen>


namespace armarx
{
    /**
     * @brief The AdvancedVisualizationFactory class is the abstract decorator of the Decorator-Pattern and decorates the VisualizationFactoy in Simox
     *
     * The additional Functionality is that it can also generate the Visualization for a Manipulator
     */
    class VisualizationSubject
    {

    public:
        /**
         * @brief getManipulatorPose
         * @return the position of the manipulator in the gui
         */
        virtual Eigen::Matrix4f getManipulatorPose() = 0;
        /**
         * @brief getSelectedWaypoint
         * @return the waypoint selected in the GUI
         */
        virtual int getSelectedWaypoint() = 0;
        /**
         * @brief setObserver adds a new observer to this subject, everytime the subjet changes the observer should be notified by calling its refresh method
         * @param observer the observer to be added
         */
        void setObserver(VisualizationObserverPtr observer);

        void removeObservers();

    protected:
        std::weak_ptr<VisualizationObserver> observer;
    };
}

#endif
