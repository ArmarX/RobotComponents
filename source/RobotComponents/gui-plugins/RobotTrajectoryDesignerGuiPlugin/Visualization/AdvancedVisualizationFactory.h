
/* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
* @author     Timo Birr
* @date       2018
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef ADVANCEDVISUALIZATIONFACTORY_H
#define ADVANCEDVISUALIZATIONFACTORY_H

#include <memory>

#include <VirtualRobot/Robot.h>

#include <VirtualRobot/Visualization/VisualizationFactory.h>

#include "AbstractManipulatorVisualization.h"

#include <VirtualRobot/Visualization/VisualizationNode.h>

#include <VirtualRobot/Primitive.h>

#include "VirtualRobot/MathTools.h"

#include "VirtualRobot/Trajectory.h"

#include "VirtualRobot/Visualization/TriMeshModel.h"

#include "VirtualRobot/BoundingBox.h"

namespace armarx
{
    /**
     * @brief The AdvancedVisualizationFactory class is the abstract decorator of the Decorator-Pattern and decorates the VisualizationFactoy in Simox
     *
     * The additional Functionality is that it can also generate the Visualization for a Manipulator
     */
    class AdvancedVisualizationFactory : public VirtualRobot::VisualizationFactory
    {


    public:
        /**
         * @brief createManipulator
         * @param kc
         * @param robot
         * @return
         */
        AbstractManipulatorVisualizationPtr createManipulator(VirtualRobot::RobotNodeSetPtr kc, VirtualRobot::RobotPtr robot);

        virtual void init(int& argc, char* argv[], const std::string& appName) override
        {
        }

        virtual VirtualRobot::VisualizationNodePtr getVisualizationFromPrimitives(const std::vector<VirtualRobot::Primitive::PrimitivePtr>& primitives, bool boundingBox = false, Color color = Color::Gray()) override
        {
            return component.getVisualizationFromPrimitives(primitives, boundingBox, color);
        }
        virtual VirtualRobot::VisualizationNodePtr getVisualizationFromFile(const std::string& filename, bool boundingBox = false, float scaleX = 1.0f, float scaleY = 1.0f, float scaleZ = 1.0f) override
        {
            return component.getVisualizationFromFile(filename, boundingBox, scaleX, scaleY, scaleZ);
        }
        virtual VirtualRobot::VisualizationNodePtr getVisualizationFromFile(const std::ifstream& ifs, bool boundingBox = false, float scaleX = 1.0f, float scaleY = 1.0f, float scaleZ = 1.0f) override
        {
            return component.getVisualizationFromFile(ifs, boundingBox, scaleX, scaleY, scaleZ);
        }

        virtual VirtualRobot::VisualizationNodePtr createBox(float width, float height, float depth, float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f) override
        {
            return component.createBox(width, height, depth, colorR, colorG, colorB);
        }
        virtual VirtualRobot::VisualizationNodePtr createLine(const Eigen::Vector3f& from, const Eigen::Vector3f& to, float width = 1.0f, float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f) override
        {
            return component.createLine(from, to, width, colorR, colorG, colorB);
        }
        virtual VirtualRobot::VisualizationNodePtr createLine(const Eigen::Matrix4f& from, const Eigen::Matrix4f& to, float width = 1.0f, float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f) override
        {
            return component.createLine(from, to, width, colorR, colorG, colorB);
        }
        virtual VirtualRobot::VisualizationNodePtr createSphere(float radius, float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f) override
        {
            return component.createSphere(radius, colorR, colorG, colorB);
        }
        virtual VirtualRobot::VisualizationNodePtr createCircle(float radius, float circleCompletion, float width, float colorR = 1.0f, float colorG = 0.5f, float colorB = 0.5f, size_t numberOfCircleParts = 30) override
        {
            return component.createCircle(radius, circleCompletion, width, colorR, colorG, colorB, numberOfCircleParts);
        }

        virtual VirtualRobot::VisualizationNodePtr createTorus(float radius, float tubeRadius, float completion = 1.0f, float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f, float transparency = 0.0f, int sides = 8, int rings = 30) override
        {
            return component.createTorus(radius, tubeRadius, completion, colorR, colorG, colorB, transparency, sides, rings);
        }

        virtual VirtualRobot::VisualizationNodePtr createCircleArrow(float radius, float tubeRadius, float completion = 1, float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f, float transparency = 0.0f, int sides = 8, int rings = 30) override
        {
            return component.createCircleArrow(radius, tubeRadius, completion, colorR, colorG, colorB, transparency, sides, rings);
        }

        virtual VirtualRobot::VisualizationNodePtr createCylinder(float radius, float height, float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f) override
        {
            return component.createCylinder(radius, height, colorR, colorG, colorB);
        }
        virtual VirtualRobot::VisualizationNodePtr createCoordSystem(float scaling = 1.0f, std::string* text = NULL, float axisLength = 100.0f, float axisSize = 3.0f, int nrOfBlocks = 10) override
        {
            return component.createCoordSystem(scaling, text, axisLength, axisSize, nrOfBlocks);
        }
        virtual VirtualRobot::VisualizationNodePtr createBoundingBox(const VirtualRobot::BoundingBox& bbox, bool wireFrame = false) override
        {
            return component.createBoundingBox(bbox, wireFrame);
        }
        virtual VirtualRobot::VisualizationNodePtr createVertexVisualization(const Eigen::Vector3f& position, float radius, float transparency,  float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f) override
        {
            return component.createVertexVisualization(position, radius, transparency, colorR, colorG, colorB);
        }

        virtual VirtualRobot::VisualizationNodePtr createTriMeshModelVisualization(VirtualRobot::TriMeshModelPtr model, Eigen::Matrix4f& pose, float scaleX = 1.0f, float scaleY = 1.0f, float scaleZ = 1.0f) override
        {
            return component.createTriMeshModelVisualization(model, pose, scaleX, scaleY, scaleZ);
        }

        virtual VirtualRobot::VisualizationNodePtr createTriMeshModelVisualization(VirtualRobot::TriMeshModelPtr model, bool showNormals, Eigen::Matrix4f& pose, bool showLines = true) override
        {
            return component.createTriMeshModelVisualization(model, showNormals, pose, showLines);
        }
        virtual VirtualRobot::VisualizationNodePtr createPlane(const Eigen::Vector3f& position, const Eigen::Vector3f& normal, float extend, float transparency,  float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f) override
        {
            return component.createPlane(position, normal, extend, transparency, colorR, colorG, colorB);
        }
        virtual VirtualRobot::VisualizationNodePtr createPlane(const VirtualRobot::MathTools::Plane& plane, float extend, float transparency,  float colorR = 0.5f, float colorG = 0.5f, float colorB = 0.5f) override
        {
            return component.createPlane(plane, extend, transparency, colorR, colorG, colorB);
        }
        virtual VirtualRobot::VisualizationNodePtr createArrow(const Eigen::Vector3f& n, float length = 50.0f, float width = 2.0f, const Color& color = Color::Gray()) override
        {
            return component.createArrow(n, length, width, color);
        }
        virtual VirtualRobot::VisualizationNodePtr createTrajectory(VirtualRobot::TrajectoryPtr t, Color colorNode = Color::Blue(), Color colorLine = Color::Gray(), float nodeSize = 15.0f, float lineSize = 4.0f) override
        {
            return component.createTrajectory(t, colorNode, colorLine, nodeSize, lineSize);
        }
        virtual VirtualRobot::VisualizationNodePtr createText(const std::string& text, bool billboard = false, float scaling = 1.0f, Color c = Color::Black(), float offsetX = 20.0f, float offsetY = 20.0f, float offsetZ = 0.0f) override
        {
            return component.createText(text, billboard, scaling, c, offsetX, offsetY, offsetZ);
        }

        virtual VirtualRobot::VisualizationNodePtr createEllipse(float x, float y, float z, bool showAxes = true, float axesHeight = 4.0f, float axesWidth = 8.0f) override
        {
            return component.createEllipse(x, y, z, showAxes, axesHeight, axesWidth);
        }
        virtual void applyDisplacement(VirtualRobot::VisualizationNodePtr o, Eigen::Matrix4f& m)  override {}

        virtual VirtualRobot::VisualizationNodePtr createVisualization() override
        {
            return component.createVisualization();
        }

        virtual VirtualRobot::VisualizationNodePtr createUnitedVisualization(const std::vector<VirtualRobot::VisualizationNodePtr>& visualizations) const override
        {
            return component.createUnitedVisualization(visualizations);
        }
        /**
         * @brief createCurve creates the visualization of a curve that goes through the Waypoints in transition
         * @param transition the curve/transition to visualize
         * @param highligt true when the Transition should be highlighted(red) , false if not (blue)
         * @return the visualization node in the visualization method of this factory
         */
        virtual VirtualRobot::VisualizationNodePtr createCurve(const std::vector<PoseBasePtr> transition, bool highligt);



    protected:
        VisualizationFactory component;


    };

    using AdvancedVisualizationFactoryPtr = std::shared_ptr<AdvancedVisualizationFactory>;
}

#endif
