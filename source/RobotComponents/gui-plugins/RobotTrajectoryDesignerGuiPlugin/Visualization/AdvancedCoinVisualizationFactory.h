/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef ADVANCEDCOINVISUALIZATIONFACTORY_H
#define ADVANCEDCOINVISUALIZATIONFACTORY_H

#include "AdvancedVisualizationFactory.h"



#include <memory>

namespace armarx
{
    class AdvancedCoinVisualizationFactory : public AdvancedVisualizationFactory
    {


    public:
        /**
         * @brief AdvancedCoinVisualizationFactory
         * Initializes the CoinVisualizationFactory
         */
        AdvancedCoinVisualizationFactory();

        VirtualRobot::VisualizationNodePtr createCurve(const std::vector<PoseBasePtr> points, bool highlight) override;

        //CoinManipulatorVisualizationAdapterPtr createManipulator();
    };

    using AdvancedCoinVisualizationFactoryPtr = std::shared_ptr<AdvancedCoinVisualizationFactory>;
}


#endif
