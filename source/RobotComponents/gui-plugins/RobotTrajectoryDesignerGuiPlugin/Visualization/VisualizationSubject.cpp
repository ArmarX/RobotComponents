#include "VisualizationSubject.h"

#include "VisualizationObserver.h"

using namespace armarx;

void VisualizationSubject::setObserver(VisualizationObserverPtr observer)
{
    this->observer = std::weak_ptr<VisualizationObserver> (observer);
}

void VisualizationSubject::removeObservers()
{
    //
}
