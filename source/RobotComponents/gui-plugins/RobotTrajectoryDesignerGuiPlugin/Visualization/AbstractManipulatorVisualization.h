/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef ABSTRACTMANIPULATORVISUALIZATION_H
#define ABSTRACTMANIPULATORVISUALIZATION_H

#include <Eigen/Eigen>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <RobotAPI/interface/core/PoseBase.h>

#include <memory>

namespace armarx
{
    /**
         * @brief The AbstractManipulatorVisualization class
         * Abstraction of a Manipulator in any 3D format
         * Info: not present in current design as the abstraction is minimal an it makes things rather complicated than easier
         */
    class AbstractManipulatorVisualization
    {
    public:
        virtual Eigen::MatrixXf getUserDesiredPose() = 0;
        virtual void setVisualization(VirtualRobot::RobotPtr robot, VirtualRobot::RobotNodeSetPtr nodeSet) = 0;
    };

    using AbstractManipulatorVisualizationPtr = std::shared_ptr<AbstractManipulatorVisualization>;
}


#endif
