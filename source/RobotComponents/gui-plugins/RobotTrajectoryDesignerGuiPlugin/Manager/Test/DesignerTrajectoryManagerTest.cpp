#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::DesignerTrajectoryManager
#define ARMARX_BOOST_TEST


#include <RobotComponents/Test.h>
#include "../Manager/DesignerTrajectoryManager.h"
#include <VirtualRobot/XML/RobotIO.h>
#include "../Util/OrientationConversion.h"
#include "../Interpolation/LinearInterpolation.h"

#include <ArmarXCore/core/logging/LogSender.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct PosePkg
{
public:
    Vector3BasePtr pos;
    QuaternionBasePtr ori;
    PoseBasePtr pose;
};

PosePkg createPosePkg(float x, float y, float z,
                      float qw, float qx, float qy, float qz)
{
    PosePkg posePkg;
    posePkg.pos = Vector3BasePtr(new Vector3(x, y, z));
    posePkg.ori = QuaternionBasePtr(new Quaternion(qw, qx, qy, qz));
    PoseBasePtr tmp = PoseBasePtr(new Pose(posePkg.pos, posePkg.ori));
    posePkg.pose = tmp;
    return posePkg;
}

double roundTo(double d, int accuracy)
{
    return floor(d * pow10(accuracy)) / pow10(accuracy);
}

bool equalPoseBase(PoseBasePtr p1, PoseBasePtr p2)
{
    int accuracy = 5;

    // check position
    if (roundTo(p1->position->x, accuracy) != roundTo(p2->position->x, accuracy))
    {
        return false;
    }
    if (roundTo(p1->position->y, accuracy) != roundTo(p2->position->y, accuracy))
    {
        return false;
    }
    if (roundTo(p1->position->z, accuracy) != roundTo(p2->position->z, accuracy))
    {
        return false;
    }

    // check orientation
    if (roundTo(p1->orientation->qw, accuracy) != roundTo(p2->orientation->qw, accuracy))
    {
        return false;
    }

    if (roundTo(p1->orientation->qx, accuracy) != roundTo(p2->orientation->qx, accuracy))
    {
        return false;
    }

    if (roundTo(p1->orientation->qy, accuracy) != roundTo(p2->orientation->qy, accuracy))
    {
        return false;
    }
    if (roundTo(p1->orientation->qz, accuracy) != roundTo(p2->orientation->qz, accuracy))
    {
        return false;
    }

    return true;
}

bool equalTrajectory(armarx::TrajectoryPtr t1, armarx::TrajectoryPtr t2)
{
    // check dimension
    if (t1->dim() != t2->dim())
    {
        return false;
    }

    // check data (joint angles)
    for (unsigned int dim = 0; dim < t1->dim(); dim++)
    {
        if (t1->getDimensionData(dim) != t2->getDimensionData(dim))
        {
            return false;
        }
    }

    // check timestamps
    if (t1->getTimestamps() != t2->getTimestamps())
    {
        return false;
    }

    return true;
}

bool equalUserWaypoint(armarx::UserWaypointPtr u1, armarx::UserWaypointPtr u2)
{
    // check poses
    if (equalPoseBase(u1->getPose(), u2->getPose()) == false)
    {
        return false;
    }

    // check joint angles
    if (u1->getJointAngles() != u2->getJointAngles())
    {
        return false;
    }

    // check ik selection
    if (u1->getIKSelection() != u2->getIKSelection())
    {
        return false;
    }

    // check is time optimal breakpoint
    if (u1->getIsTimeOptimalBreakpoint() != u2->getIsTimeOptimalBreakpoint())
    {
        return false;
    }

    // check time optimal timestamp
    if (u1->getTimeOptimalTimestamp() != u2->getTimeOptimalTimestamp())
    {
        return false;
    }

    // check user timestamp
    if (u1->getUserTimestamp() != u2->getUserTimestamp())
    {
        return false;
    }

    return true;
}

bool equalTransition(armarx::TransitionPtr t1, armarx::TransitionPtr t2)
{
    // check start user waypoints
    if (!equalUserWaypoint(t1->getStart(), t2->getStart()))
    {
        return false;
    }

    // check end user waypoints
    if (!equalUserWaypoint(t1->getEnd(), t2->getEnd()))
    {
        return false;
    }

    // check time optimal duration
    if (t1->getTimeOptimalDuration() != t2->getTimeOptimalDuration())
    {
        return false;
    }

    // check user duration
    if (t1->getUserDuration() != t2->getUserDuration())
    {
        return false;
    }

    // check interpolation type
    if (t1->getInterpolationType() != t2->getInterpolationType())
    {
        return false;
    }

    // check trajectory
    if (!equalTrajectory(t1->getTrajectory(), t2->getTrajectory()))
    {
        return false;
    }

    return true;
}

bool equalDesignerTrajectory(DesignerTrajectoryPtr dt1, DesignerTrajectoryPtr dt2)
{
    // check inter breakpoint trajectories
    std::vector<armarx::TrajectoryPtr> dt1_interBPT = dt1->getInterBreakpointTrajectories();
    std::vector<armarx::TrajectoryPtr> dt2_interBPT = dt2->getInterBreakpointTrajectories();
    if (dt1_interBPT.size() != dt2_interBPT.size())
    {
        return false;
    }

    for (unsigned int i = 0; i < dt1_interBPT.size(); i++)
    {
        if (!equalTrajectory(dt1_interBPT[i], dt2_interBPT[i]))
        {
            return false;
        }
    }

    // check user waypoints
    std::vector<UserWaypointPtr> dt1_uwp = dt1->getAllUserWaypoints();
    std::vector<UserWaypointPtr> dt2_uwp = dt2->getAllUserWaypoints();
    if (dt1_uwp.size() != dt2_uwp.size())
    {
        return false;
    }

    for (unsigned int i = 0; i < dt1_uwp.size(); i++)
    {
        if (!equalUserWaypoint(dt1_uwp[i], dt2_uwp[i]))
        {
            return false;
        }
    }

    // check transitions
    // the two designer trajectories have the same amount of transitions because
    // they have the same amount of user waypoints (has been checked above)
    for (unsigned int i = 0; i < dt1_uwp.size() - 1; i++)
    {
        if (!equalTransition(dt1->getTransition(i), dt2->getTransition(i)))
        {
            return false;
        }
    }

    // check rns
    if (dt1->getRns()->getName() != dt2->getRns()->getName())
    {
        return false;
    }

    return true;
}

PoseBasePtr poseToLocal(PoseBasePtr pose, RobotPtr robot, std::string& rnsName)
{
    return PoseBasePtr(
               new Pose(
                   robot->getRobotNodeSet(rnsName)->getKinematicRoot()->toLocalCoordinateSystem(
                       (new Pose(pose->position, pose->orientation))->toEigen())));
}

BOOST_AUTO_TEST_CASE(testEqualityOfPoseBases)
{
    PosePkg pp4 = createPosePkg(-316.302246093750, 777.949218750, 1194.246459960938, 0.5907033681869507, -0.5503066778182983, 0.4992305040359497, 0.3146440684795380);
    PosePkg pp2 = createPosePkg(-182.23925781250, 580.074218750, 1034.98925781250, 0.4338901340961456, -0.4268467724323273, 0.5642792582511902, 0.5577904582023621);//reachable

    BOOST_CHECK(equalPoseBase(pp2.pose, pp2.pose));
    BOOST_CHECK(equalPoseBase(pp4.pose, pp4.pose));
    BOOST_CHECK(!equalPoseBase(pp2.pose, pp4.pose));
    BOOST_CHECK(!equalPoseBase(pp4.pose, pp2.pose));

    // pose with minimal difference to pp4
    PosePkg pp4_1 = createPosePkg(-316.302246093, 777.9492187, 1194.246459960746, 0.5907046681869507, -0.55030667789183983, 0.4992304564358497, 0.3146440488984465480);

    BOOST_CHECK(equalPoseBase(pp4.pose, pp4_1.pose));
}

BOOST_AUTO_TEST_CASE(wrongUsage)
{
    std::string robotFile = "/RobotAPI/robots/Armar3/ArmarIII.xml";
    CMakePackageFinder finder("RobotAPI");
    RobotPtr robot;
    if (finder.packageFound())
    {
        robot = VirtualRobot::RobotIO::loadRobot(finder.getDataDir() + robotFile);
    }
    else
    {
        robot = RobotIO::loadRobot(
                    "../../../../../data/RobotTrajectoryDesigner/Resources/ArmarIII.xml");
    }


    // Get RobotNodeSet: LeftArm
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSets()[10];
    //(0): Shoulder 1 L, (1): Shoulder 2 L, (2): Upperarm L, (3): Elbow L, (4): Underarm L
    //(5): Wrist 1 L, (6): Wrist 2 L

    // Create Pose Packages
    PosePkg pp1_r = createPosePkg(4111, 7280, 1067,
                                  0.6987, 0.1106, 0.7, -0.0969);

    vector<double> pp1_r_ja = {0.0540, 0.0398, 0.3851, 0.1253, -0.2440, 0.1100, -0.0342};

    IKSolver::CartesianSelection ikSelection = IKSolver::CartesianSelection::All;
    InterpolationType interpolationType = InterpolationType::eLinearInterpolation;
    EnvironmentPtr environment = std::shared_ptr<Environment>(new Environment());
    environment->setRobot(robot);
    DesignerTrajectoryManagerPtr m = std::shared_ptr<DesignerTrajectoryManager>(new DesignerTrajectoryManager(rns->getName(), environment));
    //m.initializeDesignerTrajectory(pp1_r_ja);

    /////////////////////////////////////////////////////////////////////////////////////
    //Not initialized Test
    BOOST_CHECK_THROW(m->addWaypoint(pp1_r.pose), NotInitializedException);
    BOOST_CHECK_THROW(m->insertWaypoint(0, pp1_r.pose), NotInitializedException);
    BOOST_CHECK_THROW(m->editWaypointPoseBase(0, pp1_r.pose), NotInitializedException);
    BOOST_CHECK_THROW(m->editWaypointIKSelection(0, ikSelection), NotInitializedException);
    BOOST_CHECK_THROW(m->deleteWaypoint(0), NotInitializedException);
    BOOST_CHECK_THROW(m->setTransitionInterpolation(0, interpolationType), NotInitializedException);
    BOOST_CHECK_THROW(m->setWaypointAsBreakpoint(0, true), NotInitializedException);
    BOOST_CHECK_THROW(m->setTransitionUserDuration(0, 10.0), NotInitializedException);
}

BOOST_AUTO_TEST_CASE(basicTest)
{
    // Get Robot
    std::string robotFile = "/RobotAPI/robots/Armar3/ArmarIII.xml";
    CMakePackageFinder finder("RobotAPI");
    RobotPtr robot;
    if (finder.packageFound())
    {
        robot = VirtualRobot::RobotIO::loadRobot(finder.getDataDir() + robotFile);
    }
    else
    {
        robot = RobotIO::loadRobot(
                    "../../../../../data/RobotTrajectoryDesigner/Resources/ArmarIII.xml");
    }

    KinematicSolverPtr kc = KinematicSolver::getInstance(NULL, robot);

    // Get RobotNodeSet: LeftArm
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSets()[10];

    // Create DesignerTrajectoryManager
    EnvironmentPtr environment = std::make_shared<Environment>();
    environment->setRobot(robot);
    DesignerTrajectoryManagerPtr dtm = std::make_shared<DesignerTrajectoryManager>(rns->getName(), environment);

    // Create Poses
    std::vector<double> p1ja = { 0.464789, 0.357171, -0.303694, -0.067161, -0.445988, -0.204292, -0.070153};

    PosePkg pp4 = createPosePkg(-316.302246093750, 777.949218750, 1194.246459960938, 0.5907033681869507, -0.5503066778182983, 0.4992305040359497, 0.3146440684795380);
    PosePkg pp2 = createPosePkg(-182.23925781250, 580.074218750, 1034.98925781250, 0.4338901340961456, -0.4268467724323273, 0.5642792582511902, 0.5577904582023621);//reachable
    PosePkg pp3 = createPosePkg(-226.792480468750, 580.723144531250, 1186.157348632812, 0.4336481690406799, -0.4273631870746613, 0.5638203620910645, 0.5580471754074097);//reachable
    PosePkg pp5 = createPosePkg(-348.304718, 580.476440, 712.264465, 0.4453609585762024, -0.4538218379020691, 0.5429607033729553, 0.5485371351242065);//reachable

    std::string rnsName = rns->getName();
    PoseBasePtr p1 = poseToLocal(kc->doForwardKinematic(rns, p1ja), robot, rnsName);
    PoseBasePtr p2 = poseToLocal(pp2.pose, robot, rnsName);
    PoseBasePtr p3 = poseToLocal(pp3.pose, robot, rnsName);
    PoseBasePtr p4 = poseToLocal(pp4.pose, robot, rnsName);
    //PoseBasePtr p5nr;
    //PoseBasePtr p6col;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialize DesignerTrajectoryManager
    dtm->initializeDesignerTrajectory(p1ja);

    // test result
    DesignerTrajectoryPtr dt = dtm->getDesignerTrajectory();
    BOOST_CHECK_EQUAL(p1ja, dt->getUserWaypoint(0)->getJointAngles());
    BOOST_CHECK(equalPoseBase(p1, dt->getUserWaypoint(0)->getPose()));

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Add/Insert Waypoint
    dtm->addWaypoint(p2);
    dtm->insertWaypoint(1, p3);
    dtm->insertWaypoint(0, p4);

    // p4, p1, p3, p2
    dt = dtm->getDesignerTrajectory();
    BOOST_CHECK(equalPoseBase(dt->getUserWaypoint(0)->getPose(), p4));
    BOOST_CHECK(equalPoseBase(dt->getUserWaypoint(1)->getPose(), p1));
    BOOST_CHECK(equalPoseBase(dt->getUserWaypoint(2)->getPose(), p3));
    BOOST_CHECK(equalPoseBase(dt->getUserWaypoint(3)->getPose(), p2));

    BOOST_CHECK(dt->getUserWaypoint(0)->getTimeOptimalTimestamp() == 0);


    BOOST_CHECK(dt->getUserWaypoint(1)->getTimeOptimalTimestamp() != 0);
    BOOST_CHECK(dt->getUserWaypoint(2)->getTimeOptimalTimestamp() != 0);
    BOOST_CHECK(dt->getUserWaypoint(3)->getTimeOptimalTimestamp() != 0);


    BOOST_CHECK(dt->getUserWaypoint(0)->getJointAngles().size() != 0);
    BOOST_CHECK(dt->getUserWaypoint(1)->getJointAngles().size() != 0);
    BOOST_CHECK(dt->getUserWaypoint(2)->getJointAngles().size() != 0);
    BOOST_CHECK(dt->getUserWaypoint(3)->getJointAngles().size() != 0);



    BOOST_CHECK_EQUAL(dt->getTransition(0)->getTimeOptimalDuration(),
                      dt->getTransition(0)->getEnd()->getTimeOptimalTimestamp()
                      - dt->getTransition(0)->getStart()->getTimeOptimalTimestamp());

    BOOST_CHECK_EQUAL(dt->getTransition(1)->getTimeOptimalDuration(),
                      dt->getTransition(1)->getEnd()->getTimeOptimalTimestamp()
                      - dt->getTransition(1)->getStart()->getTimeOptimalTimestamp());

    BOOST_CHECK_EQUAL(dt->getTransition(2)->getTimeOptimalDuration(),
                      dt->getTransition(2)->getEnd()->getTimeOptimalTimestamp()
                      - dt->getTransition(2)->getStart()->getTimeOptimalTimestamp());

    BOOST_CHECK(dt->getTimeOptimalTrajectory()->size() > 1);

    //////////////////////////////////////////////////////////////////////////////////////
    dtm->setTransitionUserDuration(0, 5.0);
    dt = dtm->getDesignerTrajectory();
    BOOST_CHECK(dt->getUserWaypoint(0)->getUserTimestamp() == 0);
    BOOST_CHECK(dt->getUserWaypoint(1)->getUserTimestamp() == 5);
    BOOST_CHECK(dt->getUserWaypoint(2)->getUserTimestamp() > 5);
    BOOST_CHECK(dt->getUserWaypoint(3)->getUserTimestamp() > 5);

    dtm->setTransitionUserDuration(1, 10.0);
    dt = dtm->getDesignerTrajectory();

    BOOST_CHECK(dt->getUserWaypoint(0)->getUserTimestamp() == 0.0);
    BOOST_CHECK(dt->getUserWaypoint(1)->getUserTimestamp() == 5.0);
    BOOST_CHECK(dt->getUserWaypoint(2)->getUserTimestamp() == 15.0);
    BOOST_CHECK(dt->getUserWaypoint(3)->getUserTimestamp() > 15.0);

    dtm->setTransitionUserDuration(2, 15.0);
    dt = dtm->getDesignerTrajectory();

    BOOST_CHECK(dt->getUserWaypoint(0)->getUserTimestamp() == 0.0);
    BOOST_CHECK(dt->getUserWaypoint(1)->getUserTimestamp() == 5.0);
    BOOST_CHECK(dt->getUserWaypoint(2)->getUserTimestamp() == 15.0);
    BOOST_CHECK(dt->getUserWaypoint(3)->getUserTimestamp() == 30.0);


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Check if isInitialized is resetted
    dtm->deleteWaypoint(0);
    dtm->deleteWaypoint(0);
    dtm->deleteWaypoint(0);
    dtm->deleteWaypoint(0);

    BOOST_CHECK(dtm->getIsInitialized() == false);
}

BOOST_AUTO_TEST_CASE(undoRedoTest1)
{
    // Get Robot
    std::string robotFile = "/RobotAPI/robots/Armar3/ArmarIII.xml";
    CMakePackageFinder finder("RobotAPI");
    RobotPtr robot;
    if (finder.packageFound())
    {
        robot = VirtualRobot::RobotIO::loadRobot(finder.getDataDir() + robotFile);
    }
    else
    {
        robot = RobotIO::loadRobot(
                    "../../../../../data/RobotTrajectoryDesigner/Resources/ArmarIII.xml");
    }

    KinematicSolverPtr kc = KinematicSolver::getInstance(NULL, robot);

    // Get RobotNodeSet: LeftArm
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSets()[10];

    // Create DesignerTrajectoryManager
    EnvironmentPtr environment = std::make_shared<Environment>();
    environment->setRobot(robot);
    DesignerTrajectoryManagerPtr dtm = std::make_shared<DesignerTrajectoryManager>(rns->getName(), environment);

    // Create Poses
    std::vector<double> p1ja = { 0.464789, 0.357171, -0.303694, -0.067161, -0.445988, -0.204292, -0.070153};

    PosePkg pp4 = createPosePkg(-316.302246093750, 777.949218750, 1194.246459960938, 0.5907033681869507, -0.5503066778182983, 0.4992305040359497, 0.3146440684795380);
    PosePkg pp2 = createPosePkg(-182.23925781250, 580.074218750, 1034.98925781250, 0.4338901340961456, -0.4268467724323273, 0.5642792582511902, 0.5577904582023621);//reachable
    PosePkg pp3 = createPosePkg(-226.792480468750, 580.723144531250, 1186.157348632812, 0.4336481690406799, -0.4273631870746613, 0.5638203620910645, 0.5580471754074097);//reachable
    PosePkg pp5 = createPosePkg(-348.304718, 580.476440, 712.264465, 0.4453609585762024, -0.4538218379020691, 0.5429607033729553, 0.5485371351242065);//reachable

    std::string rnsName = rns->getName();
    PoseBasePtr p1 = poseToLocal(kc->doForwardKinematic(rns, p1ja), robot, rnsName);
    PoseBasePtr p2 = poseToLocal(pp2.pose, robot, rnsName);
    PoseBasePtr p3 = poseToLocal(pp3.pose, robot, rnsName);
    PoseBasePtr p4 = poseToLocal(pp4.pose, robot, rnsName);
    //PoseBasePtr p5nr;
    //PoseBasePtr p6col;

    // Initialize DesignerTrajectoryManager
    dtm->initializeDesignerTrajectory(p1ja);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Undo

    dtm->addWaypoint(p2);
    dtm->addWaypoint(p3);
    dtm->undo();
    dtm->redo();
    dtm->undo();
    dtm->addWaypoint(p4);
    dtm->undo();
    dtm->redo();
    dtm->editWaypointIKSelection(2, VirtualRobot::IKSolver::CartesianSelection::Position);
    dtm->undo();




    //BOOST_CHECK(equalDesignerTrajectory(dtm->getDesignerTrajectory(), dtptr1));
    //dtm->redo();
    //BOOST_CHECK(equalDesignerTrajectory(dtm->getDesignerTrajectory(), dtptr2));
    //dtm->redo();
    //BOOST_CHECK(equalDesignerTrajectory(dtm->getDesignerTrajectory(), dtptr2));
}

BOOST_AUTO_TEST_CASE(undoRedoTest2)
{
    // Get Robot
    std::string robotFile = "/RobotAPI/robots/Armar3/ArmarIII.xml";
    CMakePackageFinder finder("RobotAPI");
    RobotPtr robot;
    if (finder.packageFound())
    {
        robot = VirtualRobot::RobotIO::loadRobot(finder.getDataDir() + robotFile);
    }
    else
    {
        robot = RobotIO::loadRobot(
                    "../../../../../data/RobotTrajectoryDesigner/Resources/ArmarIII.xml");
    }

    KinematicSolverPtr kc = KinematicSolver::getInstance(NULL, robot);

    // Get RobotNodeSet: LeftArm
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSets()[10];

    // Create DesignerTrajectoryManager
    EnvironmentPtr environment = std::make_shared<Environment>();
    environment->setRobot(robot);
    DesignerTrajectoryManagerPtr dtm = std::make_shared<DesignerTrajectoryManager>(rns->getName(), environment);

    // Create Poses
    std::vector<double> p1ja = { 0.464789, 0.357171, -0.303694, -0.067161, -0.445988, -0.204292, -0.070153};

    PosePkg pp4 = createPosePkg(-316.302246093750, 777.949218750, 1194.246459960938, 0.5907033681869507, -0.5503066778182983, 0.4992305040359497, 0.3146440684795380);
    PosePkg pp2 = createPosePkg(-182.23925781250, 580.074218750, 1034.98925781250, 0.4338901340961456, -0.4268467724323273, 0.5642792582511902, 0.5577904582023621);//reachable
    PosePkg pp3 = createPosePkg(-226.792480468750, 580.723144531250, 1186.157348632812, 0.4336481690406799, -0.4273631870746613, 0.5638203620910645, 0.5580471754074097);//reachable
    PosePkg pp5 = createPosePkg(-348.304718, 580.476440, 712.264465, 0.4453609585762024, -0.4538218379020691, 0.5429607033729553, 0.5485371351242065);//reachable

    PoseBasePtr p1 = kc->doForwardKinematic(rns, p1ja);
    std::string rnsName = rns->getName();
    PoseBasePtr p2 = poseToLocal(pp2.pose, robot, rnsName);
    PoseBasePtr p3 = poseToLocal(pp3.pose, robot, rnsName);
    PoseBasePtr p4 = poseToLocal(pp4.pose, robot, rnsName);
    //PoseBasePtr p5nr;
    //PoseBasePtr p6col;


    // Initialize DesignerTrajectoryManager
    dtm->initializeDesignerTrajectory(p1ja);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Undo

    dtm->addWaypoint(p2);
    DesignerTrajectoryPtr dt1 = dtm->getDesignerTrajectory();

    dtm->addWaypoint(p3);
    DesignerTrajectoryPtr dt2 = dtm->getDesignerTrajectory();
    dtm->undo();

    BOOST_CHECK(equalDesignerTrajectory(dtm->getDesignerTrajectory(), dt1));

    dtm->redo();
    BOOST_CHECK(equalDesignerTrajectory(dtm->getDesignerTrajectory(), dt2));
    dtm->redo();
    BOOST_CHECK(equalDesignerTrajectory(dtm->getDesignerTrajectory(), dt2));
}

BOOST_AUTO_TEST_CASE(breakpointTest)
{
    // Get Robot
    std::string robotFile = "/RobotAPI/robots/Armar3/ArmarIII.xml";
    CMakePackageFinder finder("RobotAPI");
    RobotPtr robot;
    if (finder.packageFound())
    {
        robot = VirtualRobot::RobotIO::loadRobot(finder.getDataDir() + robotFile);
    }
    else
    {
        robot = RobotIO::loadRobot(
                    "../../../../../data/RobotTrajectoryDesigner/Resources/ArmarIII.xml");
    }

    KinematicSolverPtr kc = KinematicSolver::getInstance(NULL, robot);

    // Get RobotNodeSet: LeftArm
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSets()[10];

    // Create DesignerTrajectoryManager
    EnvironmentPtr environment = std::make_shared<Environment>();
    environment->setRobot(robot);
    DesignerTrajectoryManagerPtr dtm = std::make_shared<DesignerTrajectoryManager>(rns->getName(), environment);

    // Create Poses
    std::vector<double> p1ja = { 0.464789, 0.357171, -0.303694, -0.067161, -0.445988, -0.204292, -0.070153};

    PosePkg pp4 = createPosePkg(-316.302246093750, 777.949218750, 1194.246459960938, 0.5907033681869507, -0.5503066778182983, 0.4992305040359497, 0.3146440684795380);
    PosePkg pp2 = createPosePkg(-182.23925781250, 580.074218750, 1034.98925781250, 0.4338901340961456, -0.4268467724323273, 0.5642792582511902, 0.5577904582023621);//reachable
    PosePkg pp3 = createPosePkg(-226.792480468750, 580.723144531250, 1186.157348632812, 0.4336481690406799, -0.4273631870746613, 0.5638203620910645, 0.5580471754074097);//reachable
    PosePkg pp5 = createPosePkg(-348.304718, 580.476440, 712.264465, 0.4453609585762024, -0.4538218379020691, 0.5429607033729553, 0.5485371351242065);//reachable

    std::string rnsName = rns->getName();
    PoseBasePtr p1 = poseToLocal(kc->doForwardKinematic(rns, p1ja), robot, rnsName);
    PoseBasePtr p2 = poseToLocal(pp2.pose, robot, rnsName);
    PoseBasePtr p3 = poseToLocal(pp3.pose, robot, rnsName);
    PoseBasePtr p4 = poseToLocal(pp4.pose, robot, rnsName);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    dtm->initializeDesignerTrajectory(p1ja);
    dtm->addWaypoint(p2);
    dtm->addWaypoint(p3);

    dtm->setTransitionInterpolation(0, InterpolationType::eSplineInterpolation);
    dtm->setTransitionInterpolation(1, InterpolationType::eSplineInterpolation);

    dtm->setWaypointAsBreakpoint(2, true);

    BOOST_CHECK_NO_THROW(dtm->addWaypoint(p4));
}
