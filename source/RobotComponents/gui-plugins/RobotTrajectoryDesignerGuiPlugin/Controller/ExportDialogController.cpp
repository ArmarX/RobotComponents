#include "ExportDialogController.h"
#include <QMessageBox>

namespace armarx
{
    void ExportDialogController::onInitComponent()
    {
        exportDialog = new QDialog;
        ui.setupUi(exportDialog);
        fps = ui.selcetFPS->value();
        ARMARX_INFO << "RobotTrajectoryDesigner: ExportDialogController on init";
    }

    void ExportDialogController::onConnectComponent()
    {
        QObject::connect(ui.cancel, SIGNAL(clicked()), exportDialog, SLOT(reject()));
        QObject::connect(ui.exportMMM, SIGNAL(clicked()), exportDialog, SLOT(accept()));

        QObject::connect(ui.selcetFPS, SIGNAL(valueChanged(int)), this, SLOT(setFPS(int)));
    }

    void ExportDialogController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ExportDialogController on disconnect";
    }

    void ExportDialogController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ExportDialogController on exit";
    }

    ExportDialogController::ExportDialogController()
    {
        onInitComponent();
        onConnectComponent();
    }

    void ExportDialogController::open()
    {
        //fps = ui.selcetFPS->value();
        exportDialog->setModal(true);
        exportDialog->exec();
    }

    void ExportDialogController::setFPS(int fps)
    {
        this->fps = fps;
    }

    void ExportDialogController::exportTrajectory(std::vector<armarx::DesignerTrajectoryPtr> trajectories)
    {
        this->trajectories = trajectories;
    }

    void ExportDialogController::exportMMM()
    {
        if (environment == NULL)
        {
            return;
        }
        //fps = ui.selcetFPS->value();
        //exportDialog->setModal(true);
        //exportDialog->exec();
        emit exportDesignerTrajectory(fps);
        if (trajectories.empty())
        {
            QMessageBox* exportMMMfailed = new QMessageBox();
            exportMMMfailed->setWindowTitle(QString::fromStdString("Error Message"));
            exportMMMfailed->setText(QString::fromStdString("Export failed.\nNo trajectory is implemented."));
            exportMMMfailed->exec();
            return;
        }
        QString file = saveToFile();
        //try
        //{
        MMMExporterPtr exporter = std::make_shared<MMMExporter>(MMMExporter(environment));
        //exporter->exportTrajectory(trajectories, file.toStdString());
        if (!file.isEmpty())
        {
            exporter->exportTrajectory(trajectories, file.toStdString());
        }
        else
        {
            ARMARX_INFO << "Export to MMM failed (file.isEmpty)";
        }
    }

    void ExportDialogController::exportTrajectory()
    {
        /*if (environment == NULL)
        {
            return;
        }
        emit exportDesignerTrajectory();
        if (trajectories.empty())
        {
            QMessageBox* exportTrajectoryfailed = new QMessageBox();
            exportTrajectoryfailed->setWindowTitle(QString::fromStdString("Error Message"));
            exportTrajectoryfailed->setText(QString::fromStdString("Export failed.\nNo trajectory is implemented."));
            exportTrajectoryfailed->exec();
            return;
        }
        QString file = saveToFile();
        try
        {
            TrajectoryExporterPtr exporter = std::make_shared<TrajectoryExporter>();
            if (!file.isEmpty())
            {
                exporter->exportTrajectory(trajectories, file.toStdString());
            }
            else
            {
                ARMARX_INFO << "Export to Trajectory failed  (file.isEmpty)";
            }
        }
        catch (...)
        {
            ARMARX_INFO << "Export to Trajectory failed (Exception)";
        }*/
        throw armarx::NotImplementedYetException();
    }

    void ExportDialogController::environmentChanged(EnvironmentPtr environment)
    {
        this->environment = environment;
    }

    QString ExportDialogController::saveToFile()
    {
        QFileDialog dialog(0);
        dialog.setFileMode(QFileDialog::AnyFile);
        dialog.setAcceptMode(QFileDialog::AcceptSave);
        dialog.setDefaultSuffix(QString::fromStdString("xml"));
        dialog.setNameFilter(tr("XML (*.xml)"));
        QString filepath;

        if (dialog.exec())
        {
            if (dialog.selectedFiles().size() != 0)
            {
                filepath = dialog.selectedFiles()[0];
                ARMARX_INFO << filepath.toStdString();
                return filepath;
            }
        }
        return "";
    }


}
