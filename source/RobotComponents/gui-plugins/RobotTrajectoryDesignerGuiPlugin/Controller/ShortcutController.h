#ifndef SHORTCUTCONTROLLER_H
#define SHORTCUTCONTROLLER_H
#include <QShortcut>
#include <QString>
#include <QKeySequence>
#include "AbstractController.h"
#include "../ui_RobotTrajectoryDesignerGuiPluginWidget.h"
#include "../View/ui_ShortcutDialog.h"


namespace armarx
{
    class ShortcutController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onExitComponent() override;

        /**
         * @brief Creates a new ShortcutController
         */
        ShortcutController(QWidget* parent);

        /**
         * @brief Getter for the index of the currently selected waypoint
         * @return Index of the currently selected waypoint
         */
        int getCurrentWaypoint();

    public slots:
        /**
         * @brief Setter for the index of the currently selected waypoint
         * @param currentWaypoint Index of the currently selected waypoint
         */
        void setCurrentWaypoint(int currentWaypoint);

        /**
         * @brief Open the shortcutDialog
         */
        void open();

        /**
         * @brief Enables or disables the delete and change shortcut
         * @param enable Determines whether to enable or disable the delete shortcut
         */
        void enableDeleteChangeShortcut(bool enable);

        /**
         * @brief Enables or disables the add shortcut
         * @param enable Determines whether to enable or disable the add shortcut
         */
        void enableAddShortcut(bool enable);

        /**
         * @brief Enables or disables the prview shortcut
         * @param enable Determines whether to enable or disable the previes shortcut
         */
        void enablePreviewShortcut(bool enable);

        /**
         * @brief Enables or disables the prview all shortcut
         * @param enable Determines whether to enable or disable the previes shortcut
         */
        void enablePreviewAllShortcut(bool enable);

        /**
         * @brief Enables or disables the stop preview shortcut
         * @param enable Determines whether to enable or disable the stop preview shortcut
         */
        void enableStopPreviewShortcut(bool enable);

        /**
         * @brief Enables or disables the redo shortcut
         * @param enable Determines whether to enable or disable the redo shortcut
         */
        void enableRedoShortcut(bool enable);

        /**
         * @brief Enables or disables the undo shortcut
         * @param enable Determines whether to enable or disable the undo shortcut
         */
        void enableUndoShortcut(bool enable);

    private slots:
        void addedWaypointSlot();
        void deletedWaypointSlot();
        void changeWaypointSlot();
        void playPreviewSlot();
        void playPreviewAllSlot();
        void stopPreviewSlot();
        void changedPerspectiveTopSlot();
        void changedPerspectiveFrontSlot();
        void changedPerspectiveBackSlot();
        void changedPerspectiveLeftSlot();
        void changedPerspectiveRightSlot();
        void changedPerspectiveHighAngleSlot();
        void undoOperation();
        void redoOperation();
        void enableSet();
        void disableSet();

    signals:
        /*
         * TODO: change parameterlist to bool, enum, add Index of waypoint?
         */
        /**
         * @brief Add a waypoint at the waypointIndex
         * @param index Index of the last waypoint
         * @param isBreakpoint Boolean determining whether to set a breakpoint or not
         */
        void addedWaypoint(int index, bool insertAfter = true);

        void addedWaypointPressed(bool pressed);

        /**
         * @brief Delete a waypoint at the waypointIndex
         * @param index Index of the currently selected waypoint
         */
        void deletedWaypoint(int index);

        /**
         * @brief Notifies other controllers about the change of the currently
         *          selected waypoint
         * @param index Index of the currently selected waypoint
         */
        void changeWaypoint(int index);

        /**
         * @brief Starts a simulation of the current Trajectory
         */
        void playPreviewSignal();

        /**
         * @brief Plays the preview of all trajectories
         */
        void playPreviewAllSignal();

        /**
         * @brief Stop the preview of all trajectories
         */
        void stopPreviewSignal();

        /**
         * @brief Change the perspective in the robot visualization
         * @param perspective Integer representing an enum for the different perspectives
         */
        void changedPerspective(int perspective);

        /**
         * @brief Notifies other controllers about undoing the lastly executed operation
         */
        void undo();

        /**
         * @brief Notifies other controllers about redoing the lastly undone operation
         */
        void redo();

    private:
        QWidget* parent;
        int currentWaypoint;
        QShortcut* deactivateSetWaypoint;
        QShortcut* setWaypoint;
        QShortcut* deleteWaypoint;
        QShortcut* changeWaypointShortcut;
        QShortcut* playPreview;
        QShortcut* playPreviewAll;
        QShortcut* stopPreview;
        QShortcut* setPerspectiveTop;
        QShortcut* setPerspectiveFront;
        QShortcut* setPerspectiveBack;
        QShortcut* setPerspectiveLeft;
        QShortcut* setPerspectiveRight;
        QShortcut* setPerspectiveHighAngle;
        QShortcut* undoShortcut;
        QShortcut* redoShortcut;
        Ui::ShortcutDialog ui;
        QDialog* shortcutDialog;



    };
    using ShortcutControllerPtr = std::shared_ptr<ShortcutController>;

}


#endif // SHORTCUTCONTROLLER_H
