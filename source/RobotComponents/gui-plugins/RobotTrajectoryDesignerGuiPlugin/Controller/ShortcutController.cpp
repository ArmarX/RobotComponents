#include "ShortcutController.h"
#include "qapplication.h"
namespace armarx
{
    void ShortcutController::onInitComponent()
    {
        deactivateSetWaypoint =  new QShortcut(QKeySequence(Qt::LeftButton), this->parent);
        setWaypoint = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this->parent);
        deleteWaypoint = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_D), this->parent);
        changeWaypointShortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_W), this->parent);
        playPreview = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_R), this->parent);
        playPreviewAll = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_A), this->parent);
        stopPreview = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_F), this->parent);
        undoShortcut = new QShortcut(QKeySequence::Undo, this->parent);
        redoShortcut = new QShortcut(QKeySequence::Redo, this->parent);
        setPerspectiveHighAngle = new QShortcut(QKeySequence(Qt::Key_1), this->parent);
        setPerspectiveTop = new QShortcut(QKeySequence(Qt::Key_2), this->parent);
        setPerspectiveFront = new QShortcut(QKeySequence(Qt::Key_3), this->parent);
        setPerspectiveBack = new QShortcut(QKeySequence(Qt::Key_4), this->parent);
        setPerspectiveLeft = new QShortcut(QKeySequence(Qt::Key_5), this->parent);
        setPerspectiveRight = new QShortcut(QKeySequence(Qt::Key_6), this->parent);
        setWaypoint->setEnabled(false);
        deleteWaypoint->setEnabled(false);
        changeWaypointShortcut->setEnabled(false);
        playPreview->setEnabled(false);
        playPreviewAll->setEnabled(false);
        stopPreview->setEnabled(false);
        shortcutDialog = new QDialog;
        ui.setupUi(shortcutDialog);
        ARMARX_INFO << "RobotTrajectoryDesigner: ShortcutController on init";
    }

    void ShortcutController::onConnectComponent()
    {
        //Connects Gui
        QObject::connect(ui.okButton, SIGNAL(clicked()), shortcutDialog, SLOT(accept()));
        //Connects Shortcuts
        QObject::connect(deactivateSetWaypoint, SIGNAL(activated()), this, SLOT(disableSet()));
        QObject::connect(setWaypoint, SIGNAL(activated()), this, SLOT(addedWaypointSlot()));
        QObject::connect(deleteWaypoint, SIGNAL(activated()), this, SLOT(deletedWaypointSlot()));
        QObject::connect(changeWaypointShortcut, SIGNAL(activated()), this, SLOT(changeWaypointSlot()));
        QObject::connect(playPreview, SIGNAL(activated()), this, SLOT(playPreviewSlot()));
        QObject::connect(playPreviewAll, SIGNAL(activated()), this, SLOT(playPreviewAllSlot()));
        QObject::connect(stopPreview, SIGNAL(activated()), this, SLOT(stopPreviewSlot()));
        QObject::connect(setPerspectiveTop, SIGNAL(activated()), this, SLOT(changedPerspectiveTopSlot()));
        QObject::connect(setPerspectiveFront, SIGNAL(activated()), this, SLOT(changedPerspectiveFrontSlot()));
        QObject::connect(setPerspectiveBack, SIGNAL(activated()), this, SLOT(changedPerspectiveBackSlot()));
        QObject::connect(setPerspectiveLeft, SIGNAL(activated()), this, SLOT(changedPerspectiveLeftSlot()));
        QObject::connect(setPerspectiveRight, SIGNAL(activated()), this, SLOT(changedPerspectiveRightSlot()));
        QObject::connect(setPerspectiveHighAngle, SIGNAL(activated()), this, SLOT(changedPerspectiveHighAngleSlot()));
        QObject::connect(undoShortcut, SIGNAL(activated()), this, SLOT(undoOperation()));
        QObject::connect(redoShortcut, SIGNAL(activated()), this, SLOT(redoOperation()));
        ARMARX_INFO << "RobotTrajectoryDesigner: ShortcutController on connect";
    }

    void ShortcutController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ShortcutController on disconnect";
    }

    void ShortcutController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ShortcutController on exit";
    }

    ShortcutController::ShortcutController(QWidget* parent)
    {
        currentWaypoint = 0;
        this->parent = parent;
        onInitComponent();
        onConnectComponent();
    }

    int ShortcutController::getCurrentWaypoint()
    {
        return currentWaypoint;
    }

    void ShortcutController::setCurrentWaypoint(int currentWaypoint)
    {
        if (currentWaypoint >= 0)
        {
            this->currentWaypoint = currentWaypoint;
        }
    }

    void ShortcutController::open()
    {
        shortcutDialog->setModal(true);
        shortcutDialog->exec();
    }

    void ShortcutController::enablePreviewAllShortcut(bool enable)
    {
        playPreviewAll->setEnabled(enable);
    }

    void ShortcutController::enableStopPreviewShortcut(bool enable)
    {
        stopPreview->setEnabled(enable);
    }

    void ShortcutController::enableRedoShortcut(bool enable)
    {
        redoShortcut->setEnabled(enable);
    }

    void ShortcutController::enableUndoShortcut(bool enable)
    {
        undoShortcut->setEnabled(enable);
    }

    void ShortcutController::enableDeleteChangeShortcut(bool enable)
    {
        deleteWaypoint->setEnabled(enable);
        changeWaypointShortcut->setEnabled(enable);
    }

    void ShortcutController::enableAddShortcut(bool enable)
    {
        setWaypoint->setEnabled(enable);
    }

    void ShortcutController::enablePreviewShortcut(bool enable)
    {
        playPreview->setEnabled(enable);
    }

    void ShortcutController::addedWaypointSlot()
    {
        //cannot add a waypoint while mouse is held, as programm crashes when manipulator is moved while adding waypoints
        if (QApplication::mouseButtons() == Qt::NoButton)
        {
            emit addedWaypoint(currentWaypoint);
        }
        else
        {
            ARMARX_ERROR << "MOUSE HELD";
        }
    }

    void ShortcutController::deletedWaypointSlot()
    {
        emit deletedWaypoint(currentWaypoint);
    }

    void ShortcutController::changeWaypointSlot()
    {
        emit changeWaypoint(currentWaypoint);
    }

    void ShortcutController::playPreviewSlot()
    {
        emit playPreviewSignal();
    }

    void ShortcutController::playPreviewAllSlot()
    {
        emit playPreviewAllSignal();
    }

    void ShortcutController::stopPreviewSlot()
    {
        emit stopPreviewSignal();
    }

    void ShortcutController::changedPerspectiveHighAngleSlot()
    {
        emit changedPerspective(0);
    }

    void ShortcutController::changedPerspectiveTopSlot()
    {
        emit changedPerspective(1);
    }

    void ShortcutController::changedPerspectiveFrontSlot()
    {
        emit changedPerspective(2);
    }

    void ShortcutController::changedPerspectiveBackSlot()
    {
        emit changedPerspective(3);
    }

    void ShortcutController::changedPerspectiveLeftSlot()
    {
        emit changedPerspective(4);
    }

    void ShortcutController::changedPerspectiveRightSlot()
    {
        emit changedPerspective(5);
    }

    void ShortcutController::undoOperation()
    {
        emit undo();
    }

    void ShortcutController::redoOperation()
    {
        emit redo();
    }

    void ShortcutController::enableSet()
    {
        this->enableAddShortcut(true);
    }

    void ShortcutController::disableSet()
    {
        this->enableAddShortcut(false);
    }
}
