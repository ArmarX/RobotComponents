#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::Trajectorycontroller
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../TrajectoryController.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with trajectory and controller");

    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    TrajectoryControllerPtr trajectoryController;
};

BOOST_FIXTURE_TEST_CASE(TrajectoryControllerTest, F)
{

}
