#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::TCPSelectioncontroller
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../TCPSelectionController.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with TCP selection tab and controller");

    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    TCPSelectionControllerPtr tcpSelectionController;
};

BOOST_FIXTURE_TEST_CASE(TCPSelectionControllerTest, F)
{

}
