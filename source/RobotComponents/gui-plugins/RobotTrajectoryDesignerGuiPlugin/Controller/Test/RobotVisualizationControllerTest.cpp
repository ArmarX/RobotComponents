#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::Robotvisualizationcontroller
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../RobotVisualizationController.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with export dialog and controller");

    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    RobotVisualizationControllerPtr robotVisualizationController;
};

BOOST_FIXTURE_TEST_CASE(RobotVisualizationControllerTest, F)
{

}
