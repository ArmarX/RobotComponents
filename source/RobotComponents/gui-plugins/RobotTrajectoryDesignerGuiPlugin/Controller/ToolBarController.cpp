/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::ToolBarController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "ToolBarController.h"

namespace armarx
{
    void ToolBarController::onInitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ToolBarController on init";
        enableDeleteChangeButton(false);
        enableAddButton(false);
        enablePreviewButton(false);
        enablePreviewAllButton(false);
        enableStopButton(false);
    }

    void ToolBarController::onConnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ToolBarController on connect";

        // Set waypoint: clicked
        QObject::connect(guiToolbar.get()->getToolBar()->setWaypointButton, SIGNAL(clicked()),
                         this, SLOT(setWaypoint()));

        // Delete waypoint: clicked
        QObject::connect(guiToolbar.get()->getToolBar()->deleteWaypointButton, SIGNAL(clicked()),
                         this, SLOT(deleteWaypoint()));

        // Change Waypoint: clicked
        QObject::connect(guiToolbar.get()->getToolBar()->changeWaypointButton, SIGNAL(clicked()),
                         this, SLOT(changeWaypoint()));

        // Play preview: clicked
        QObject::connect(guiToolbar.get()->getToolBar()->playPreviewButton, SIGNAL(clicked()),
                         this, SLOT(playPreview()));

        // Play all preview: clicked
        QObject::connect(guiToolbar.get()->getToolBar()->playPreviewAllButton, SIGNAL(clicked()),
                         this, SLOT(playAllPreview()));

        // Stop preview: clicked
        QObject::connect(guiToolbar.get()->getToolBar()->stopPreviewButton, SIGNAL(clicked()),
                         this, SLOT(stopPreview()));

    }

    void ToolBarController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ToolBarController on disconnect";
    }

    void ToolBarController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ToolBarController on exit";
    }

    ToolBarController::ToolBarController(ToolBarPtr guiToolbar) :
        guiToolbar(guiToolbar),
        currentWaypoint(0)
    {
        onInitComponent();
        onConnectComponent();
    }

    ToolBarPtr ToolBarController::getGuiToolbar()
    {
        return this->guiToolbar;
    }

    void ToolBarController::setGuiToolbar(ToolBarPtr guiToolbar)
    {
        this->guiToolbar = guiToolbar;
    }

    int ToolBarController::getCurrentWaypoint()
    {
        return this->currentWaypoint;
    }

    void ToolBarController::setCurrentWaypoint(int currentWaypoint)
    {
        if (currentWaypoint >= 0)
        {
            this->currentWaypoint = currentWaypoint;
        }
    }

    void ToolBarController::setWaypoint()
    {
        emit addedWaypoint(this->currentWaypoint);
    }

    void ToolBarController::deleteWaypoint()
    {
        emit deletedWaypoint(this->currentWaypoint);
    }

    void ToolBarController::changeWaypoint()
    {
        emit changeWaypoint(this->currentWaypoint);
    }

    void ToolBarController::playAllPreview()
    {
        emit notifyAllPreview();
    }

    void ToolBarController::playPreview()
    {
        emit notifyPreview();
    }

    void ToolBarController::stopPreview()
    {
        emit notifyStopPreview();
    }

    void ToolBarController::retranslateGui()
    {
        throw ("not yet implemented");
    }

    void ToolBarController::enableDeleteChangeButton(bool enable)
    {
        guiToolbar->getToolBar()->changeWaypointButton->setEnabled(enable);
        guiToolbar->getToolBar()->deleteWaypointButton->setEnabled(enable);
    }

    void ToolBarController::enableAddButton(bool enable)
    {
        guiToolbar->getToolBar()->setWaypointButton->setEnabled(enable);
    }

    void ToolBarController::enablePreviewAllButton(bool enable)
    {
        guiToolbar->getToolBar()->playPreviewAllButton->setEnabled(enable);
    }

    void ToolBarController::enablePreviewButton(bool enable)
    {
        guiToolbar->getToolBar()->playPreviewButton->setEnabled(enable);
    }

    void ToolBarController::enableStopButton(bool enable)
    {
        guiToolbar->getToolBar()->stopPreviewButton->setEnabled(enable);
    }

}
