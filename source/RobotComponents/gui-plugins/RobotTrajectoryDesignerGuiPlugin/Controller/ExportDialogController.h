#ifndef EXPORTDIALOGCONTROLLER_H
#define EXPORTDIALOGCONTROLLER_H
#include "AbstractController.h"
#include <QDialog>
#include "../View/ui_ExportDialog.h"
#include "../Model/DesignerTrajectory.h"
#include <QErrorMessage>
#include <QString>
#include <QFileDialog>
#include <QFile>
#include "../ImportExport/MMMExporter.h"
#include "../Environment.h"

namespace armarx
{
    /**
     * @class ExportDialogController
     * @brief Subcontroller which handles all user interaction with the export
     *          dialog in the GUI, communicates with other controllers
     *          via signals and slots
     */
    class ExportDialogController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onExitComponent() override;

        /**
         * @brief Creates a new ExportDialogController and assigns a designer
         *          exporter to handle
         */
        ExportDialogController();

    public slots:
        /**
         * @brief Opens an export dialog
         */
        void open();

        /**
         * @brief Sets the timed distribution of automatically generated waypoints
         *          by assigning how many waypoints there are per second
         * @param value Number of waypoints per second
         */
        void setFPS(int fps);

        /**
         * @brief Exports the trajectory
         * @param trajectory DesignerTrajectory fpr export
         */
        void exportTrajectory(std::vector<DesignerTrajectoryPtr> trajectories);

        /**
         * @brief Start export to MMM
         */
        void exportMMM();

        /**
         * @brief Start export to trajectory
         */
        void exportTrajectory();

        /**
         * @brief Set the enviroment
         * @param The new enviroment
         */
        void environmentChanged(EnvironmentPtr environment);

    signals:
        /*
         * Changed method name because 'export' is a keyword
         */
        /**
         * @brief Requests a designer trajectory to a given index to export
         * @param index Index of the trajectory
         */
        void exportDesignerTrajectory(int fps);

        /**
         * @brief Requests a designer trajectory to a given index to export
         */
        void exportDesignerTrajectory();

    private:
        QString saveToFile();
        EnvironmentPtr environment;
        //DesignerExporterPtr exporter;
        QDialog* exportDialog;
        Ui::ExportDialog ui;
        std::vector<armarx::DesignerTrajectoryPtr> trajectories;
        int fps;
    };

    using ExportDialogControllerPtr = std::shared_ptr<ExportDialogController>;
}

#endif // EXPORTDIALOGCONTROLLER_H
