/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::WaypointController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef WAYPOINTCONTROLLER_H
#define WAYPOINTCONTROLLER_H
#include "AbstractController.h"
#include "../View/WaypointTab.h"
#include "../Util/WheelEventFilter.h"

#include <QListWidget>
#include <QMetaType>
#include <QVariant>
#include <QDoubleValidator>

namespace armarx
{

    /*
     * TODO: Change method signatures to support start time identifier
     */
    /**
     * @brief Struct which allows storing relevant data to display within
     *          a list widget item as QVariant
     */
    typedef struct GuiWaypoint
    {
        std::vector<double> values;
        int cartesianSelection;
        bool isBreakpoint;
    } GuiWaypoint;

    /**
     * @class WaypointController
     * @brief Subcontroller which handles all user interaction with the waypoint
     *          tab in the GUI, communicates with other controllers via signals
     *          and slots
     */
    class WaypointController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onExitComponent() override;

        /**
         * @brief Creates a new WaypointController and assigns a WaypointTab to handle
         * @param guiWaypointTab Pointer to the WaypointTab whose user interaction
         *                         is handled by the WaypointController
         */
        WaypointController(WaypointTabPtr guiWaypointTab);

        /**
         * @brief Getter for the WaypointTab pointer to guiWaypointTab
         * @return A Pointer to the guiWayointTab
         */
        WaypointTabPtr getGuiWaypointTab();

        /**
         * @brief Setter for the WaypointTab pointer to guiWaypointTab
         * @param guiWaypointTab Pointer to the WaypointTab whose user interaction
         *                         is handled by the WaypointController
         */
        void setGuiWaypointTab(WaypointTabPtr guiWaypointTab);

    public slots:
        /**
         * @brief Adds a new waypoint to the list widget
         * @param values Array containting x, y, z, coordinates as well as a, b, g
         *               euler angles of the waypoint
         * @param index Index of the waypoint
         * @param values x, y, z coordinate and roll, pitch, yaw euler angles of a waypoint
         * @param cartesianSelection Integer representing the cartesian selection
         * @param isBreakpoint Boolean determining whether the waypoint is a breakpoint
         */
        void addWaypoint(int index, std::vector<double> values, int cartesianSelection,
                         bool isBreakpoint);

        /**
         * @brief Removes the waypoint at a given index
         * @param index Index of the waypoint
         */
        void removeWaypoint(int index);

        /**
         * @brief Updates the currently selected waypoint
         * @param index Index of the currently selected waypoint
         */
        void updateSelectedWaypoint(int index);

        /**
         * @brief Connected with signals from other controllers, sets all
         *        values of the waypoint at a given index
         * @param index Index of the waypoint
         * @param values Array containing the x, y, z coordinates and the
         *                  roll, pitch, yaw euler angles
         * @param constraints Constraints of the waypoint: isBreakpoint, ikConstraints
         */
        void setWaypointData(int index, std::vector<double> values, int cartesianSelection,
                             bool isBreakpoint);

        /**
         * @brief Retranslates the guiWaypointTab
         */
        void retranslateGui();

        /**
         * @brief Removes all items of the waypoint list
         */
        void clearWaypointList();

        /**
         * @brief Enables or disables the delete button
         * @param enable Determines whether to enable or disable the delete button
         */
        void enableDeleteButton(bool enable);

        /**
         * @brief Enables or disables the add button
         * @param enable Determines whether to enable or disable the add button
         */
        void enableAddButton(bool enable);

        /**
         * @brief Enables or disables the waypoint list and line edit
         * @param enable Determines whether to enable or disable the waypoint list and line edit
         */
        void enableWaypointListLineEdit(bool enable);

    private slots:
        /**
         * @brief Updates the currently selected waypoint
         * @param item Currently selected waypoint in as list widget item
         */
        void updateSelectedWaypoint(QListWidgetItem* item);

        /**
         * @brief Sets the x-coordinate of the currently selected waypoint
         */
        void setXCoordinate();

        /**
         * @brief Sets the y-coordinate of the currently selected waypoint
         */
        void setYCoordinate();

        /**
         * @brief Sets the z-coordinate of the currently selected waypoint
         */
        void setZCoordinate();

        /**
         * @brief Sets the roll euler angle of the currently selected waypoint
         */
        void setEulerAngleR();

        /**
         * @brief Sets the pitch euler angle of the currently selected waypoint
         */
        void setEulerAngleP();

        /**
         * @brief Sets the yaw euler angle of the currently selected waypoint
         */
        void setEulerAngleY();

        /**
         * @brief Sets the cartesian selection of the currently selected waypoint
         * @param cs Index of the cartesian selection
         */
        void setCartesianSelection(int cs);

        /**
         * @brief Sets whether the waypoint is a breakpoint
         * @param isBreakpoint Boolean value determining whether the waypoint
         *                      is a breakpoint
         */
        void setBreakpoint(bool isBreakpoint);

        /**
         * @brief Adds a new waypoint relative to the currently selected waypoint
         */
        void addWaypoint();

        /**
         * @brief Removes the currently selected waypoint
         */
        void removeWaypoint();

    signals:
        /**
         * @brief Notifies other controllers about changes of the given waypoint
         * @param waypoint Index of the waypoint
         * @param values Vector containing the x, y, z coordinates and the
         *                  roll, pitch, yaw euler angles
         */
        void changedWaypoint(int waypoint, std::vector<double> values);

        /**
         * @brief Notifies other controllers about changes of the given waypoint
         * @param waypoint Index of the waypoint
         * @param cartesianSelection Integer representing the cartesian selection
         */
        void changedWaypoint(int waypoint, int cartesianSelection);

        /**
         * @brief Notifies other controllers about changes of the given waypoint
         * @param waypoint Index of the waypoint
         * @param isBreakpoint Boolean determining whether the waypoint is a breakpoint
         */
        void changedWaypoint(int waypoint, bool isBreakpoint);

        /**
         * @brief Notifies other controllers about the addition of a new waypoint
         *          with given constraints
         * @param waypoint Index of the waypoint
         * @param insertAfter Boolean determining whether to insert after the waypoint
         *                      or before
         */
        void addedWaypoint(int waypoint, bool insertAfter);

        /**
         * @brief Notifies other controllers about the deletion of a given waypoint
         * @param waypoint Index of the deleted waypoint
         */
        void deletedWaypoint(int waypoint);

        /**
         * @brief Notifies other controllers whether to enable or disable the button for
         *        a new IK solution
         * @param enable Determines whether to enable or disable the IK solution button
         */
        void enableIKSolutionButton(bool enable);

        /**
         * @brief Notifies other controllers about changes of the current waypoint
         * @param index Index of the current waypoint
         */
        void setCurrentIndex(int index);

        /**
         * @brief Notifies RobotVisualizationController about changes of the current waypoint
         * @param index Index of the current waypoint
         */
        void setCurrentIndexRobotVisualization(int index);

    private:
        WaypointTabPtr guiWaypointTab;

        /**
         * @brief Initializes the cartesian selection combo box
         */
        void initCSComboBox();

        /**
         * @brief Initializes and sets the validator for each line edit
         * @param bottom Smallest number accepted by the validator
         * @param top Greatest number accepted by the validator
         * @param decimals Number of digits behind the decimal point
         */
        void initValidator(double bottom, double top, int decimals);

        /**
         * @brief Updates all Gui elements corresponding to waypoint data
         * @param waypoint GuiWaypoint providing waypoint data
         */
        void updateWaypointElements(GuiWaypoint waypoint);

        //@Max eingefügt von @Tim dadurch: Reihenfolge Item == Text Item
        void changeTextListWidgetItems();
    };

    using WaypointControllerPtr = std::shared_ptr<WaypointController>;
}

Q_DECLARE_METATYPE(armarx::GuiWaypoint)

#endif // WAYPOINTCONTROLLER_H
