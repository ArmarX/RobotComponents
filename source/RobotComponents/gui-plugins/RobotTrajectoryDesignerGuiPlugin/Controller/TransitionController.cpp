/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::TransitionController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "TransitionController.h"
#include "../Interpolation/InterpolationType.h"
#include <iostream>

namespace armarx
{

    void TransitionController::onInitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TransitionController on init";

        // Fill interpolation combo box with items
        initInterpolationComboBox();

        // Set single selection and clear transition list
        this->guiTransitionTab->getTransitionTab()->transitionList->
        setSelectionMode(QAbstractItemView::SingleSelection);
        this->guiTransitionTab->getTransitionTab()->transitionList->clear();

        // Add double validator to duration line edit
        QDoubleValidator* validator = new QDoubleValidator(
            -1000.000,
            1000.000,
            3,
            guiTransitionTab.get());
        validator->setNotation(QDoubleValidator::StandardNotation);
        this->guiTransitionTab->getTransitionTab()->durationLineEdit->setValidator(validator);
    }

    void TransitionController::onConnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TransitionController on connect";

        // Transition list: changed selected item
        QObject::connect(guiTransitionTab->getTransitionTab()->transitionList,
                         SIGNAL(itemClicked(QListWidgetItem*)), this,
                         SLOT(updateSelectedTransition(QListWidgetItem*)));

        // Transition duration: changed duration
        QObject::connect(guiTransitionTab->getTransitionTab()->durationLineEdit,
                         SIGNAL(editingFinished()),
                         this, SLOT(setDurationValue()));

        // Transition interpolation: changed interpolation
        QObject::connect(guiTransitionTab->getTransitionTab()->interpolationComboBox,
                         SIGNAL(activated(int)),
                         this, SLOT(setInterpolation(int)));
    }

    void TransitionController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TransitionController on disconnect";
    }

    void TransitionController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TransitionController on exit";
    }

    TransitionController::TransitionController(TransitionTabPtr guiTransitionTab) :
        guiTransitionTab(guiTransitionTab)
    {
        onInitComponent();
        onConnectComponent();
    }

    TransitionTabPtr TransitionController::getGuiTransitionTab()
    {
        return this->guiTransitionTab;
    }

    void TransitionController::setGuiTransitionTab(TransitionTabPtr guiTransitionTab)
    {
        if (guiTransitionTab != NULL)
        {
            this->guiTransitionTab = guiTransitionTab;
        }
    }

    void TransitionController::addTransition(
        int index,
        double duration,
        double start,
        int interpolation)
    {
        // Create struct representing transition
        struct GuiTransition transition;
        transition.duration = duration;
        transition.start = start;
        transition.it = interpolation;

        if (transition.isValid())
        {
            // Create qvariant from struct
            QVariant data;
            data.setValue(transition);

            // add data to item and insert
            QListWidgetItem* item = new QListWidgetItem(QString(("Transition: " + std::to_string(index)).c_str()));
            item->setData(Qt::UserRole, data);
            QListWidget* transitions = guiTransitionTab->getTransitionTab()->transitionList;

            transitions->insertItem(index, item);
            updateSelectedTransition(index);
        }
        changeTextListWidgetItems();
    }

    void TransitionController::removeTransition(int index)
    {
        QListWidget* transitions = guiTransitionTab->getTransitionTab()->transitionList;

        // If index is within range, delete transition at index
        if (transitions->count() > index)
        {
            delete transitions->takeItem(index);
        }
        changeTextListWidgetItems();
    }

    void TransitionController::updateSelectedTransition(QListWidgetItem* item)
    {
        GuiTransition transition = item->data(Qt::UserRole).value<GuiTransition>();

        // Duration line edit
        guiTransitionTab->getTransitionTab()->durationLineEdit->
        setText(QString::number(transition.duration));
        // Start value label
        guiTransitionTab->getTransitionTab()->startValueLabel->
        setText(QString::number(transition.start));
        // End value label
        guiTransitionTab->getTransitionTab()->EndValueLabel->
        setText(QString::number(transition.start + transition.duration));
        // Interpolation combo box
        guiTransitionTab->getTransitionTab()->interpolationComboBox->
        setCurrentIndex(transition.it);

        // Notify other controllers about changes of the selected transition
        QListWidget* transitions = guiTransitionTab->getTransitionTab()->transitionList;
        emit selectedTransitionChanged(transitions->currentIndex().row());

    }

    void TransitionController::updateSelectedTransition(int index)
    {
        GuiTransition transition = guiTransitionTab->getTransitionTab()->transitionList->
                                   item(index)->data(Qt::UserRole).value<GuiTransition>();

        // Duration line edit
        guiTransitionTab->getTransitionTab()->durationLineEdit->
        setText(QString::number(transition.duration));
        // Start value label
        guiTransitionTab->getTransitionTab()->startValueLabel->
        setText(QString::number(transition.start));
        // End value label
        guiTransitionTab->getTransitionTab()->EndValueLabel->
        setText(QString::number(transition.start + transition.duration));
        // Interpolation combo box
        guiTransitionTab->getTransitionTab()->interpolationComboBox->
        setCurrentIndex(transition.it);
    }

    void TransitionController::setDurationValue()
    {
        if (this->guiTransitionTab->getTransitionTab()->durationLineEdit->hasAcceptableInput())
        {
            QListWidget* transitions = guiTransitionTab->getTransitionTab()->transitionList;
            QListWidgetItem* item = transitions->currentItem();

            // Check if list widget has any item
            if (item != NULL)
            {
                GuiTransition transition = item->data(Qt::UserRole).value<GuiTransition>();
                double dur = this->guiTransitionTab->getTransitionTab()->
                             durationLineEdit->text().toDouble();

                // Check if duration of item has changed
                if ((dur >= 0) && (dur != transition.duration))
                {
                    emit changedDuration(transitions->currentRow(), dur);
                }
            }

        }
    }

    void TransitionController::setInterpolation(int index)
    {
        QListWidget* transitions = guiTransitionTab->getTransitionTab()->transitionList;
        QListWidgetItem* item = transitions->currentItem();
        // Check if list widget has any item
        if (item != NULL)
        {
            GuiTransition transition = item->data(Qt::UserRole).value<GuiTransition>();

            // Check if interpolation of item has changed
            if ((index >= 0) && (index != transition.it))
            {
                emit changedInterpolation(transitions->currentRow(), index);
            }
        }
    }

    void TransitionController::enableAll(bool enable)
    {
        this->guiTransitionTab->getTransitionTab()->durationLineEdit->setEnabled(enable);
        this->guiTransitionTab->getTransitionTab()->interpolationComboBox->setEnabled(enable);
        this->guiTransitionTab->getTransitionTab()->transitionList->setEnabled(enable);
    }

    void TransitionController::setTransitionData(
        int index,
        double duration,
        double start,
        int it)
    {
        // Create struct holding relevant data
        struct GuiTransition transition;
        transition.duration = duration;
        transition.start = start;
        transition.it = it;

        if (transition.isValid())
        {
            // Create QVariant from struct
            QVariant data;
            data.setValue(transition);

            QListWidget* transitions = guiTransitionTab->getTransitionTab()->transitionList;
            QListWidgetItem* item = transitions->item(index);

            // Update data
            item->setData(Qt::UserRole, data);
            // Display updated data
            if (transitions->currentRow() == index)
            {
                updateSelectedTransition(index);
            }
        }
    }

    void TransitionController::retranslateGui()
    {
        throw ("not yet implemented");
    }

    void TransitionController::clearTransitionList()
    {
        // Clear transition list
        this->guiTransitionTab->getTransitionTab()->transitionList->clear();
        // Clear duration line edit
        this->guiTransitionTab->getTransitionTab()->durationLineEdit->clear();
        // Clear start and end label
        this->guiTransitionTab->getTransitionTab()->startValueLabel->clear();
        this->guiTransitionTab->getTransitionTab()->EndValueLabel->clear();
        // Reset interpolation index
        this->guiTransitionTab->getTransitionTab()->interpolationComboBox->setCurrentIndex(0);
    }

    bool TransitionController::contains(QListWidget* list, GuiTransition transition)
    {
        for (int i = 0; i < list->count(); i++)
        {
            if (list->item(i)->data(Qt::UserRole).value<GuiTransition>() == transition)
            {
                return true;
            }
        }
        return false;
    }

    void TransitionController::initInterpolationComboBox()
    {
        QComboBox* interpolation = guiTransitionTab->getTransitionTab()->
                                   interpolationComboBox;

        // Set strong focus and add wheel event filter
        interpolation->setFocusPolicy(Qt::StrongFocus);
        interpolation->installEventFilter(new WheelEventFilter(this));

        // Clear combo box and insert items
        interpolation->clear();
        interpolation->addItem(QString::fromStdString("Linear Interpolation"));
        interpolation->addItem(QString::fromStdString("Spline interpolation"));
        interpolation->setEnabled(true);
        interpolation->setCurrentIndex(0);
    }

    void TransitionController::changeTextListWidgetItems()
    {
        QListWidget* transitions = this->guiTransitionTab->getTransitionTab()->transitionList;
        for (int i = 0; i < transitions->count(); i++)
        {
            transitions->item(i)->setText(QString::fromStdString(("Transition: " + std::to_string(i))));
        }
    }
}
