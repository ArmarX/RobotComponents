#include "TimedTrajectory.h"
#include <memory.h>

armarx::TimedTrajectory::TimedTrajectory(armarx::TrajectoryPtr trajectory, std::vector<double> userPoints)
{
    this->trajectory = trajectory;
    this->userPoints = userPoints;
}

const armarx::TrajectoryPtr armarx::TimedTrajectory::getTrajectory() const
{
    return trajectory;
}

std::vector<double> armarx::TimedTrajectory::getUserPoints() const
{
    return userPoints;
}
