#ifndef DESIGNERTRAJECTORYCALCULATOR_H
#define DESIGNERTRAJECTORYCALCULATOR_H

#include <Eigen/Core>
#include "TimedTrajectory.h"
#include <VirtualRobot/Robot.h>
#include "../Environment.h"

namespace armarx
{
    class DesignerTrajectoryCalculator;
    /**
     * @class DesignerTrajectoryCalculator
     * @brief Offers functionality to create TimedTrajectories from supplied Trajectories and points set by the user.
    */
    class DesignerTrajectoryCalculator
    {
    private:
        EnvironmentPtr environment;

    public:

        //TimedTrajectory calculateTimeOptimalTrajectory(std::vector<Eigen::VectorXf>& trajectory, std::vector<Eigen::VectorXf>& userPoints, double maxDeviation);
        /*
         * HOW TO CONVERT vector<vector<double>> to vector<Eigen::VectorXf>
         *
               // convert ikSolutions from vector<vector<double>> to vector<Eigen::VectorXf>
                std::vector<Eigen::VectorXf> ikSolutionsEigen;
                for (std::vector<double> ikSolution : ikSolutions)
                {
                    std::vector<float> ikSolutionFloat(ikSolution.begin(), ikSolution.end());
                    ikSolutionsEigen.push_back(
                    Eigen::VectorXf::Map(ikSolutionFloat.data(), ikSolutionFloat.size()));
                }
         *
         */

        DesignerTrajectoryCalculator(EnvironmentPtr environment);
        /**
         * @brief Calculates a TimedTrajectory from the supplied trajectory and points set by user.
         * @param trajectory A vector of floats representing the trajectory.
         * @param userPoints The set of points defined by the user.
         * @param rns The RobotNodeSet used by the trajectory.
         * @param maxDeviation The maximum deviation of the points along the trajectory.
         * @param accuracyFactor The factor with which to scale the accuracy of the time optimal trajectory.
         * @return The created TimedTrajectory.
         */
        TimedTrajectory calculateTimeOptimalTrajectory(std::vector<std::vector<double>>& trajectory, std::vector<std::vector<double>>& userPoints, VirtualRobot::RobotNodeSetPtr rns, double maxDeviation, double accuracyFactor = 1.0);
    };
}
#endif // DESIGNERTRAJECTORYCALCULATOR_H
