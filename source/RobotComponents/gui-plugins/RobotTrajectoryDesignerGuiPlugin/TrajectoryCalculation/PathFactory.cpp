#include "PathFactory.h"

VirtualRobot::Path armarx::PathFactory::createPath(std::vector<std::vector<double>>& nodes, double maxDeviation)
{
    //Convert the input vector of vectors to a list of Eigen::VectorXd
    std::list<Eigen::VectorXd> path;
    for (unsigned int i = 0; i < nodes.size(); i++)
    {
        //Map the vector accordingly
        Eigen::VectorXd doubleNode = Eigen::VectorXd::Map(&nodes[i][0], nodes[i].size());
        path.push_back(doubleNode);
    }
    //Return the result
    return VirtualRobot::Path(path, maxDeviation);
}
