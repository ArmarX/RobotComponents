#include "../Controller/AbstractController.h"
//#include "../Controller/CollisionDialogController.h"
#include "../Controller/ExportDialogController.h"
#include "../Controller/ImportDialogController.h"
//#include "../Controller/PopUpController.h"
//#include "../Controller/ScenarioDialogController.h"
#include "../Controller/SettingController.h"
#include "../Controller/ShortcutController.h"
#include "../Controller/TCPInformationController.h"
#include "../Controller/TCPSelectionController.h"
#include "../Controller/ToolBarController.h"
#include "../Controller/TrajectoryController.h"
#include "../Controller/TransitionController.h"
#include "../Controller/ViewController.h"
#include "../Controller/WaypointController.h"

#include "../RobotTrajectoryDesignerGuiPluginGuiPlugin.h"
#include "../RobotTrajectoryDesignerGuiPluginWidgetController.h"

#include "ui_Perspectives.h"
#include "ui_SettingTab.h"
#include "ui_TCPInformationTab.h"
#include "ui_TransitionTab.h"
#include "ui_WaypointTab.h"

#include <iostream>

class Test
{
public :
    Test()
    {
        std::cout << "This is a test!";
    }
};
