/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Interpolation
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "SplineInterpolationSegment.h"

#include "../exceptions/InterpolationNotDefinedException.h"

using namespace armarx;

SplineInterpolationSegment::SplineInterpolationSegment(int number, AbstractInterpolationPtr parent)
{
    if (number >= parent->getNumberOfControlPoints() - 1  || number < 0)
    {
        throw new exceptions::local::InterpolationNotDefinedException(number);
    }
    this->parent = parent;
    this->poseAccesFactor = 1.0 / (parent.get()->getNumberOfControlPoints() - 1);
    this->poseAccesStart = poseAccesFactor * number;
}

PoseBasePtr SplineInterpolationSegment::getPoseAt(double time)
{
    if (time < 0 || time > 1)
    {
        throw new exceptions::local::InterpolationNotDefinedException(time);
    }
    return parent->getPoseAt(poseAccesStart + poseAccesFactor * time);
}

int SplineInterpolationSegment::getNumberOfControlPoints()
{
    return 2;
}
