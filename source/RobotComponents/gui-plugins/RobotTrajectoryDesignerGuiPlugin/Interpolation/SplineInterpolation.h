/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Interpolation
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef SPLINEINTERPOLATION_H
#define SPLINEINTERPOLATION_H

#include "AbstractInterpolation.h"

#include <interpolation.h>


namespace armarx
{
    /**
     * @brief The SplineInterpolation class represents a linear Interpolation between a series of control points
     * Spline means that the position is calcualed by a function with the form of the ploynom
     */
    class SplineInterpolation : public AbstractInterpolation
    {
    public:
        /**
         * @brief SplineInterpolation creates a new SplineInterpolation through a series of conntrol points
         * @param controlPoints a vector of Poses that define the Interpolation-Function
         */
        SplineInterpolation(std::vector<PoseBasePtr> controlPoints);

        /**
         * @brief getPoseAt returns the Pose defined by f(time)
         * @param time a time between 0 and 1 with getPoseAt(0) being the startingPose and getPoseAt(1) being the ending Pose
         * @return the pose of the interpolation-function at time
         */
        PoseBasePtr getPoseAt(double time) override;

        /**
         * @brief getInterPolationSegment returns a segment of the current interpolation
         * @param start the Pose where the Interval of the InterpolationSegment starts
         * @return the shared_ptr to an interpolation Function that is a scaled Interval of this Interpolation
         */
        AbstractInterpolationPtr getInterPolationSegment(PoseBasePtr start);

        /**
         * @brief getInterPolationSegment returns a segment of the current interpolation
         * @param start the Pose where the Interval of the InterpolationSegment starts
         * @return the shared_ptr to an interpolation Function that is a scaled Interval of this Interpolation
         */
        AbstractInterpolationPtr getInterPolationSegment(int segmentNumber);

    private:
        alglib::real_2d_array getCoordinateArray(int coordinate);

        alglib::pspline2interpolant xInterpolation;
        alglib::pspline2interpolant yInterpolation;
        alglib::pspline2interpolant zInterpolation;

    };

    using SplineInterpolationPtr = std::shared_ptr<SplineInterpolation>;
}

#endif
