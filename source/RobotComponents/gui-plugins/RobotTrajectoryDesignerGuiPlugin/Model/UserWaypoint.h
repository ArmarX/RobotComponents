/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Model
 * @author     Lukas Christ
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef USERWAYPOINT_H
#define USERWAYPOINT_H

#include <RobotAPI/interface/core/PoseBase.h>
#include <Eigen/Core>
#include <RobotComponents/interface/components/RobotIK.h>
#include <memory>
#include <VirtualRobot/IK/GenericIKSolver.h>
#include <ArmarXCore/core/util/PropagateConst.h>
#include <RobotAPI/libraries/core/FramedPose.h>


namespace armarx
{
    using IKSelection = VirtualRobot::IKSolver::CartesianSelection;
    /**
     * @brief The UserWaypoint class represents a waypoint of the trajectory.
     */
    class UserWaypoint
    {
    private:
        PoseBasePtr pose;
        std::vector<double> jointAngles;
        IKSelection iKSelection;
        bool isTimeOptimalBreakpoint;
        double timeOptimalTimestamp;
        double userTimestamp;

    public:
        /**
         * @brief UserWaypoint
         * @param newPose
         */
        UserWaypoint(PoseBasePtr newPose);

        UserWaypoint(const UserWaypoint& source);

        /**
         * @brief Returns the PoseBasePtr of the UserWaypoint
         * @return PoseBasePtr
         */
        PoseBasePtr getPose();

        /**
         * @brief Returns the jointAngles of the UserWaypoint
         * @return jointAngles
         */
        std::vector<double> getJointAngles() const;

        /**
         * @brief Returns the VirtualRobot::IKSolver::CartesianSelection of the UserWaypoint
         * @return IKSelections
         */
        IKSelection getIKSelection() const;

        /**
         * @brief Returns if the UserWaypoint is breakpoint
         * @return
         */
        bool getIsTimeOptimalBreakpoint() const;

        /**
         * @brief Returns the timeOptimalTimestamp of the UserWaypoint which is calculate
         *by the TrajectoryCalculation
         * @return timeOptimalTimestamp
         */
        double getTimeOptimalTimestamp() const;

        /**
         * @brief Returns the UserTimestamp
         * @return userTimestamp
         */
        double getUserTimestamp() const;

        ////////SET///////////////////////////////////////////////////////
        /**
         * @brief Set the new PoseBase of the UserWaypoint
         * @param new PoseBasePtr
         */
        void setPose(const PoseBasePtr& value);

        /**
         * @brief Set the new JointAngles of the UserWaypoint
         * @param new JointAngles
         */
        void setJointAngles(const std::vector<double>& value);

        /**
         * @brief Set the new IKSelection of the UserWaypoint
         * @param iKSelection
         */
        void setIKSelection(const IKSelection& value);

        /**
         * @brief Set isTimeOptimalBreakpoint
         * @param is breakpoint or not
         */
        void setIsTimeOptimalBreakpoint(bool value);

        /**
         * @brief Set the time optimal timestamp of the UserWaypoint calculate by
         * TrajectoryCalculation
         * @param time optimal Timestamp
         */
        void setTimeOptimalTimestamp(double value);

        /**
         * @brief Set the new UserDuration and tests if its greater than timeOptimalTimestamp
         * @param user timestamp
         */
        void setUserTimestamp(double value);

    };
    using UserWaypointPtr = std::shared_ptr<UserWaypoint>;
}
#endif // USERWAYPOINT_H
