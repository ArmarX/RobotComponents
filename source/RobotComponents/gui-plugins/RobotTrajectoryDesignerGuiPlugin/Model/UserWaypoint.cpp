#include "UserWaypoint.h"

using namespace armarx;

std::vector<double> UserWaypoint::getJointAngles() const
{
    return jointAngles;
}

void UserWaypoint::setJointAngles(const std::vector<double>& value)
{
    if (value.size() != 0)
    {
        jointAngles = value;
    }
    else
    {
        throw InvalidArgumentException("Can not setJointAngles with empty vector");
    }
}

IKSelection UserWaypoint::getIKSelection() const
{
    return iKSelection;
}

void UserWaypoint::setIKSelection(const IKSelection& value)
{
    iKSelection = value;
}

bool UserWaypoint::getIsTimeOptimalBreakpoint() const
{
    return isTimeOptimalBreakpoint;
}

void UserWaypoint::setIsTimeOptimalBreakpoint(bool value)
{
    isTimeOptimalBreakpoint = value;
}

double UserWaypoint::getTimeOptimalTimestamp() const
{
    return timeOptimalTimestamp;
}

void UserWaypoint::setTimeOptimalTimestamp(double value)
{
    if (value >= 0)
    {
        timeOptimalTimestamp = value;
        if (timeOptimalTimestamp > userTimestamp)
        {
            userTimestamp = timeOptimalTimestamp;
        }
    }
    else
    {
        throw InvalidArgumentException("TimeOptimal timestamp must be greater than or equal 0");
    }
}

double UserWaypoint::getUserTimestamp() const
{
    return userTimestamp;
}

void UserWaypoint::setUserTimestamp(double value)
{
    if (value >= timeOptimalTimestamp)
    {
        userTimestamp = value;
    }
    else
    {
        throw InvalidArgumentException("User timestamp must be greater than or equal timeOptimal timestamp");
    }
}

PoseBasePtr UserWaypoint::getPose()
{
    return pose;
}

void UserWaypoint::setPose(const PoseBasePtr& value)
{
    pose = value;
}

UserWaypoint::UserWaypoint(PoseBasePtr newPose):
    iKSelection(VirtualRobot::IKSolver::CartesianSelection::All),
    isTimeOptimalBreakpoint(false),
    timeOptimalTimestamp(0), userTimestamp(0)
{
    pose = newPose;
}

UserWaypoint::UserWaypoint(const UserWaypoint& source) :
    pose(IceInternal::Handle<PoseBase>(new Pose(*PosePtr::dynamicCast(source.pose)))),
    jointAngles(source.jointAngles),
    iKSelection(source.iKSelection), isTimeOptimalBreakpoint(source.isTimeOptimalBreakpoint),
    timeOptimalTimestamp(source.timeOptimalTimestamp), userTimestamp(source.userTimestamp)
{
}



