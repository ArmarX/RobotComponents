#include "Transition.h"

using namespace armarx;

double Transition::getUserDuration() const
{
    return userDuration;
}

void Transition::setUserDuration(double value)
{
    if (value >= 0)
    {
        if (value < timeOptimalDuration)
        {
            throw InvalidArgumentException("User duration must be greater than or equal timeOptimalDuration");
        }
        else
        {
            userDuration = value;
            getEnd()->setUserTimestamp(getStart()->getUserTimestamp() + userDuration);
        }
    }
    else
    {
        throw InvalidArgumentException("User duration must be greater than or equal 0");
    }
}

double Transition::getTimeOptimalDuration() const
{
    return timeOptimalDuration;
}

void Transition::setTimeOptimalDuration(double value)
{
    if (value >= 0)
    {
        timeOptimalDuration = value;
        getEnd()->setTimeOptimalTimestamp(getStart()->getTimeOptimalTimestamp()
                                          + timeOptimalDuration);
        if (userDuration == 0 || userDuration < timeOptimalDuration)
        {
            setUserDuration(timeOptimalDuration);
        }
    }
    else
    {
        throw InvalidArgumentException("TimeOptimal duration must be greater than or equal 0");
    }
}

InterpolationType Transition::getInterpolationType() const
{
    return interpolationType;
}

void Transition::setInterpolationType(const InterpolationType& value)
{
    interpolationType = value;
}

TrajectoryPtr Transition::getTrajectory()
{
    return trajectory;
}

void Transition::setTrajectory(const TrajectoryPtr& value)
{
    trajectory = value;
}

UserWaypointPtr Transition::getStart()
{
    return start;
}

void Transition::setStart(const UserWaypointPtr& value)
{
    if (value != nullptr)
    {
        start = value;
    }
    else
    {
        throw InvalidArgumentException("Can not setStart with nullptr");
    }
}

UserWaypointPtr Transition::getEnd()
{
    return end;
}

void Transition::setEnd(const UserWaypointPtr& value)
{
    if (value != nullptr)
    {
        end = value;
    }
    else
    {
        throw InvalidArgumentException("Can not setEnd with nullptr");
    }
}

armarx::Transition::Transition(UserWaypointPtr& newStart, UserWaypointPtr& newEnd)
    : interpolationType(eLinearInterpolation)
{
    if (newStart != nullptr)
    {
        start = newStart;
    }
    else
    {
        throw InvalidArgumentException("Can not construct Transition with newStart = nullptr");
    }
    if (newEnd != nullptr)
    {
        end = newEnd;
    }
    else
    {
        throw InvalidArgumentException("Can not construct Transition with newEnd = nullptr");
    }
    timeOptimalDuration = 0;
    userDuration = 0;
}

Transition::Transition(const Transition& source, const UserWaypointPtr newStart, const UserWaypointPtr newEnd) :
    start(newStart),
    end(newEnd),
    timeOptimalDuration(source.timeOptimalDuration), userDuration(source.userDuration),
    interpolationType(source.interpolationType),
    trajectory(IceInternal::Handle<Trajectory>(new Trajectory(*source.trajectory)))
{
}
