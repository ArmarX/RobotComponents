/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Model
 * @author     Lukas Christ
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef TRANSITION_H
#define TRANSITION_H

#include <RobotAPI/libraries/core/Trajectory.h>
#include "UserWaypoint.h"
#include "../Interpolation/InterpolationType.h"


namespace armarx
{
    using TrajectoryConstPtr = TrajectoryPtr;

    class Transition
    {

    private:
        UserWaypointPtr start;
        UserWaypointPtr end;
        double timeOptimalDuration;
        double userDuration;
        InterpolationType interpolationType;
        TrajectoryPtr trajectory = TrajectoryPtr(new Trajectory());



    public:
        /**
         * @brief Transition
         * @param newStart
         * @param newEnd
         */
        Transition(UserWaypointPtr& newStart, UserWaypointPtr& newEnd);
        /**
         * @brief Deep copy constructor of the class Transition.
         * @param source to copy
         */
        Transition(const Transition& source, const UserWaypointPtr newStart, const UserWaypointPtr newEnd);

        ///////GET///////////////////////////////////////////////////////////////////////
        /**
         * @brief Returns the user duration of the transition.
         * @return user duration
         */
        double getUserDuration() const;

        /**
         * @brief Returns the time optimal duration calculated by TrajectoryCalculation
         * @return time optimal duration
         */
        double getTimeOptimalDuration() const;

        /**
         * @brief Returns the interpolation type of the transition.
         * @see InterpolationType
         * @return
         */
        InterpolationType getInterpolationType() const;

        /**
         * @brief Returns the armarx::Trajectory of the transition
         * @return trajectory
         */
        TrajectoryPtr getTrajectory();

        /**
         * @brief Returns the start UserWaypoint of the transition
         * @return start user waypoint
         */
        UserWaypointPtr getStart();

        /**
         * @brief Returns the end UserWaypoint of the transition
         * @return end user waypoint
         */
        UserWaypointPtr getEnd();


        ////////SET//////////////////////////////////////////////////////////////////////
        /**
         * @brief Set the user duration of the transition and tests if it is greater than
         * the time optimal duration. Updates the user timestamps of the start and end
         * UserWaypoint.
         * @param value
         */
        void setUserDuration(double value);

        /**
         * @brief set the time optimal duration and set the userDuration
         * if its less than time optimal duration
         * @param value time
         */
        void setTimeOptimalDuration(double value);

        /**
         * @brief Set the intpolation type of the transition
         * @param vlaue interpolation type
         */
        void setInterpolationType(const InterpolationType& value);

        /**
         * @brief set the time optimal trajectory of the transition
         * @param value trajectory
         */
        void setTrajectory(const TrajectoryPtr& value);

        /**
         * @brief set the start userwaypoint of the transition
         * @param value start
         */
        void setStart(const UserWaypointPtr& value);

        /**
         * @brief set the end userwaypoint of the transition
         * @param value end
         */
        void setEnd(const UserWaypointPtr& value);
    };
    using TransitionPtr = std::shared_ptr<Transition>;

}


#endif // TRANSITION_H
