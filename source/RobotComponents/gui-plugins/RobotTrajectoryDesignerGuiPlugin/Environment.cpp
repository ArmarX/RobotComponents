#include "Environment.h"
#include <ArmarXCore/core/exceptions/user/NotImplementedYetException.h>
#include <ArmarXCore/core/logging/LogSender.h>
#include <ArmarXCore/core/logging/Logging.h>

using namespace armarx;

Environment::Environment()
{
    this->scene = VirtualRobot::ScenePtr(new VirtualRobot::Scene("empty"));
}

VirtualRobot::RobotPtr Environment::getRobot()
{
    if (robot != nullptr)
    {
        return robot;
    }
    else
    {
        throw NotImplementedYetException("robot null");
    }
}

VirtualRobot::RobotPtr Environment::getCDRobot()
{
    if (cdRobot != nullptr)
    {
        return cdRobot;
    }
    else
    {
        throw NotImplementedYetException("cdRobot null");
    }
}

void Environment::setRobot(const VirtualRobot::RobotPtr& value)
{
    robot = value;
    cdRobot = robot->clone();
}

VirtualRobot::ScenePtr Environment::getScene()
{
    if (scene != nullptr)
    {
        return scene;
    }
    else
    {
        return NULL;
    }
}

void Environment::setScene(const VirtualRobot::ScenePtr& value)
{
    scene = value;
}
