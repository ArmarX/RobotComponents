/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <QFileDialog>
#include <QDialog>

namespace Ui
{
    class MMMPlayerConfigDialog;
}

namespace armarx
{
    class MMMPlayerConfigDialog :
        public QDialog, virtual public ManagedIceObject
    {
        Q_OBJECT
        friend class MMMPlayerWidget;

    public:
        explicit MMMPlayerConfigDialog(QWidget* parent = 0);
        ~MMMPlayerConfigDialog() override;

        // inherited from ManagedIceObject
        std::string getDefaultName() const override
        {
            return "MMMPlayerConfigDialog" + uuid;
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

    private slots:
        void verifyConfiguration();
        void reject() override;


    private:
        Ui::MMMPlayerConfigDialog* ui;

        IceProxyFinderBase* kinematicUnitComponentProxyFinder;
        IceProxyFinderBase* mmmPlayerComponentProxyFinder;
        IceProxyFinderBase* trajPlayerComponentProxyFinder;


        std::string kinematicTopicName;

        bool needtoCreate;

        std::string uuid;

    };
}

