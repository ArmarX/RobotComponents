/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::PointCloudVisualization
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudVisualization.h"

//Coin includes
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/SbVec3f.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoSphere.h>

#include <ArmarXCore/core/logging/Logging.h>

using namespace armarx;

PointCloudVisualization::PointCloudVisualization()
{
    this->ref();
}

PointCloudVisualization::~PointCloudVisualization()
{
    this->removeAllChildren();
    this->unref();
}

void PointCloudVisualization::setVisualization(pcl::PointCloud<PointT>::ConstPtr cloud)
{
    // Clear all previous visualization
    this->removeAllChildren();

    //Insert color information into scene graph
    SoMaterial* materialInfo = new SoMaterial();
    std::vector<SbColor> colorData;
    colorData.reserve(cloud->points.size());
    //Add point coordinates
    SoCoordinate3* coordinates = new SoCoordinate3();
    std::vector<SbVec3f> pointData;
    pointData.reserve(cloud->points.size());
    for (const PointT& p : cloud->points)
    {
        if (std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z))
        {
            SbColor colorContainer;
            if (useOriginalColors)
            {
                float colorArray[3];

                colorArray[0] = (float) p.r / 256.0f;
                colorArray[1] = (float) p.g / 256.0f;
                colorArray[2] = (float) p.b / 256.0f;

                colorContainer.setValue(colorArray);
            }
            else
            {
                colorContainer = this->color;
            }

            colorData.push_back(colorContainer);

            SbVec3f pointContainer;
            //Factor 1000 to match coin unit system
            pointContainer[0] = p.x / 1000;
            pointContainer[1] = p.y / 1000;
            pointContainer[2] = p.z / 1000;
            pointData.push_back(pointContainer);
        }
    }


    materialInfo->diffuseColor.setValues(0, colorData.size(), colorData.data());
    this->addChild(materialInfo);

    //Bind materials to per part
    SoMaterialBinding* binding = new SoMaterialBinding();
    binding->value = SoMaterialBinding::PER_PART;
    this->addChild(binding);

    coordinates->point.setValues(0, pointData.size(), pointData.data());
    this->addChild(coordinates);

    //Set point size
    SoDrawStyle* pointSize = new SoDrawStyle();
    pointSize->pointSize = this->pointSize;
    this->addChild(pointSize);

    //Draw a point set out of all that data
    SoPointSet* pointSet = new SoPointSet();
    this->addChild(pointSet);
}

void PointCloudVisualization::setDrawColor(SbColor color)
{
    this->color = color;
    this->useOriginalColors = false;
}

void PointCloudVisualization::setPointSize(int size)
{
    if (size < 1)
    {
        size = 1;
    }
    this->pointSize = size;
}

void PointCloudVisualization::resetDrawColor()
{
    this->useOriginalColors = true;
}
