
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ObjectLocalizationSaliency)
 
armarx_add_test(ObjectLocalizationSaliencyTest ObjectLocalizationSaliencyTest.cpp "${LIBS}")