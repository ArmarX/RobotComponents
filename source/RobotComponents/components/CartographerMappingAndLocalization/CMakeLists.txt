set(LIB_NAME CartographerMappingAndLocalization)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

armarx_build_if(
    RobotComponents::Cartographer
    "Cartographer lib dependencies not met."
)

armarx_add_component(
    COMPONENT_LIBS
        ArmarXCore
        ArmarXCoreInterfaces # for DebugObserverInterface
        RemoteGui
        MemoryXCore
        MemoryXMemoryTypes
        MemoryXEarlyVisionHelpers
        RobotComponentsInterfaces # for interfaces from this package
        VisionXCore
        VisionXPointCloud
        ArmarXGui
        RobotAPICore
        RobotAPIComponentPlugins
        RobotComponents::Cartographer
        armem_robot_state
        armem_vision
        RobotComponents::SelfLocalization
    SOURCES
        ./CartographerMappingAndLocalization.cpp
        ./MappingRemoteGui.cpp
        ./LocalizationRemoteGui.cpp
    HEADERS
        ./CartographerMappingAndLocalization.h
        ./MappingRemoteGui.h
        ./LocalizationRemoteGui.h
)

# add unit tests
add_subdirectory(test)

# generate the application
armarx_generate_and_add_component_executable(
    COMPONENT_NAMESPACE
        "armarx::cartographer"
)
