/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartographerMappingAndLocalization.h"

#include <algorithm>
#include <chrono>
#include <cstdint>
#include <deque>
#include <filesystem>
#include <memory>
#include <numeric>
#include <optional>
#include <stdexcept>
#include <string>
#include <thread>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>

#include <opencv2/core.hpp>
#include <opencv2/core/persistence.hpp>
#include <opencv2/imgcodecs.hpp>

#include <IceUtil/Time.h>

#include <SimoxUtility/json.h>
#include <SimoxUtility/math/convert/aa_to_rpy.h>

#include "ArmarXCore/core/services/tasks/TaskUtil.h"
#include "ArmarXCore/core/time/Clock.h"
#include "ArmarXCore/libraries/logging/FrequencyReporter.h"
#include <ArmarXCore/core/application/properties/PluginEigen.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder/StaticWidgets.h>

#include "RobotAPI/libraries/armem_robot/types.h"
#include "RobotAPI/libraries/armem_vision/types.h"
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/interface/core/RobotLocalization.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/HeartbeatComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/armem_robot_state/client/localization/TransformReader.h>
#include <RobotAPI/libraries/armem_robot_state/client/localization/TransformWriter.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include "RobotComponents/libraries/SelfLocalization/SelfLocalization.h"
#include "RobotComponents/libraries/cartographer/ArVizDrawer.h"
#include "RobotComponents/libraries/cartographer/ArVizDrawerMapBuilder.h"
#include "RobotComponents/libraries/cartographer/grid_conversion.h"
#include "RobotComponents/libraries/cartographer/utils.h"

namespace fs = std::filesystem;

namespace armarx::cartographer
{

    std::string
    toString(CartographerMappingAndLocalization::Mode mode)
    {
        std::string modeStr;

        switch (mode)
        {
            case CartographerMappingAndLocalization::Mode::Localization:
                modeStr = "localization";
                break;
            case CartographerMappingAndLocalization::Mode::Mapping:
                modeStr = "mapping";
                break;
        }

        return modeStr;
    }

    CartographerMappingAndLocalization::CartographerMappingAndLocalization() :
        /*laserScansMemoryWriter(std::make_unique<armem::vision::laser_scans::client::Writer>(*this)),*/
        occupancyGridMemoryWriter(memoryNameSystem()),
        robotReader(memoryNameSystem())
    {
        addPlugin(heartbeat);
        // addPlugin(robotUnit);
    }

    CartographerMappingAndLocalization::~CartographerMappingAndLocalization() = default;

    std::string
    CartographerMappingAndLocalization::getDefaultName() const
    {
        return "CartographerMappingAndLocalization";
    }

    armarx::PropertyDefinitionsPtr
    CartographerMappingAndLocalization::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = SelfLocalization::createPropertyDefinitions();

        // topic publisher
        def->topic(debugObserver);

        // topic subscriber
        def->topic<LaserScannerUnitListener>(
            "LaserScans", "laser_scanner_topic", "Name of the laser scanner topic.");

        //        def->topic<PlatformUnitListener>(
        //            "PlatformState",
        //            "platform_state",
        //            "Name of the platform + state to use. This property is used to listen to "
        //            "the platform topic");

        // def->topic<OdometryListener>();

        def->component(remoteGui,
                       "RemoteGuiProvider",
                       "remote_gui_provider",
                       "Name of the remote gui provider");

        // defs->optional(member, "doku", "member");
        //   defs->optional(testdim, "my_test_dimensions", "This is documentation stuff.");
        def->optional(properties.mapPath,
                      "map_path",
                      "Path where the map will be created (within a unique subdirectory)");
        def->optional(properties.mapToLoad,
                      "map_to_load",
                      "Name of a map, e.g. 2021-02-15-17-17.carto. Load this map from "
                      "within the 'map_path'");
        def->optional(
            properties.cartographerConfigPath, "config_path", "Path to the lua config files");
        def->optional(properties.mode,
                      "mode",
                      "Either run the component in mapping or localization optimized mode")
            .map(toString(Mode::Localization), Mode::Localization)
            .map(toString(Mode::Mapping), Mode::Mapping);

        def->optional(properties.enableVisualization,
                      "enable_arviz",
                      "If enabled, ArViz layers are created.");

        def->optional(properties.robotName, "robotName", "the name of the robot");

        def->optional(adapterConfig.useOdometry, "use_odometry");
        def->optional(adapterConfig.useLaserScanner, "use_laserscanner");
        def->optional(adapterConfig.useImu, "use_imu");
        def->optional(adapterConfig.frequency,
                      "frequency",
                      "The rate (in Hz) at which the cartographer processes data");
        def->optional(
            adapterConfig.laserScanners, "laser_scanners", "Set of laser scanners to use.");

        def->optional(properties.useNativeOdometryTimestamps, "useNativeOdometryTimestamps", "if disabled, will assume that odometry is always available (this is a bit hacky)");

        def->optional(properties.occupancyGridMemoryUpdatePeriodMs, "occupancyGridMemoryUpdatePeriodMs", "The frequency to store the occupancy grid in the memory.");
        def->optional(properties.odomQueryPeriodMs, "odomQueryPeriodMs", "The polling frequency to retrieve odometry data from the memory.");

        // if (laserScansMemoryWriter)
        // {
        //     laserScansMemoryWriter->registerPropertyDefinitions(def);
        // }

        occupancyGridMemoryWriter.registerPropertyDefinitions(def);

        return def;
    }

    inline Eigen::Affine3f
    toM(Eigen::Affine3f pose)
    {
        pose.translation() /= 1000;
        return pose;
    }

    inline Eigen::Affine3f
    toMM(Eigen::Affine3f pose)
    {
        pose.translation() *= 1000;
        return pose;
    }

    void
    CartographerMappingAndLocalization::setupMappingAdapter()
    {
        ARMARX_TRACE;

        if (mappingAdapter != nullptr)
        {
            ARMARX_INFO << "Mapping adapter already intialized. Skipping setup.";
            return;
        }

        const std::string configSubfolder = toString(properties.mode);
        const fs::path mapPath = ArmarXDataPath::resolvePath(properties.mapPath);

        ARMARX_INFO << "Map will be stored in " << mapPath.string();
        fs::create_directories(mapPath);

        const auto configPath =
            fs::path(ArmarXDataPath::resolvePath(properties.cartographerConfigPath)) /
            configSubfolder;
        ARMARX_IMPORTANT << "Using config files from " << configPath;


        switch (properties.mode)
        {
            case Mode::Mapping:
            {
                // this is the interface to cartographer
                mappingAdapter = std::make_unique<CartographerAdapter>(
                    mapPath, configPath, *this, adapterConfig);
                break;
            }
            case Mode::Localization:
            {
                // load the map in localization mode
                const fs::path mapToLoad = mapPath / properties.mapToLoad;

                ARMARX_INFO << "Trying to load map from " << mapToLoad;

                ARMARX_CHECK(fs::is_regular_file(mapToLoad))
                    << "In localization mode, a valid map has to be provided.";


                std::optional<Eigen::Affine3f> world_T_robot_prior = globalPose(); // [mm]

                // if user didn't set any correction via RemoteGUI, use pose from LTM
                if (not map_T_map_correct.has_value())
                {
                    world_T_robot_prior = globalPoseFromLongTermMemory(); // [mm]
                }

                const auto map_T_robot_prior = [&]() -> std::optional<Eigen::Affine3f> // [m] !!
                {
                    if (not world_T_robot_prior.has_value())
                    {
                        return std::nullopt;
                    }

                    return toM(world_T_map.inverse() * world_T_robot_prior.value());
                }();

                // this is the interface to cartographer
                mappingAdapter = std::make_unique<CartographerAdapter>(
                    mapPath, configPath, *this, adapterConfig, mapToLoad, map_T_robot_prior);

                break;
            }
            default:
                throw std::invalid_argument("Unknown mode");
        }

        // hook up the input queues
        laserMessageQueue.connect(&CartographerAdapter::processSensorValues, mappingAdapter.get());
        odomMessageQueue.connect(&CartographerAdapter::processOdometryPose, mappingAdapter.get());

        auto sensorPose = [=](const std::string& sensorFrame) -> Eigen::Affine3f
        {
            Eigen::Affine3f robot_T_sensor = Eigen::Affine3f::Identity();
            robot_T_sensor.matrix() = getRobot()->getRobotNode(sensorFrame)->getPoseInRootFrame();
            robot_T_sensor.translation() /= 1000.; // to [m]

            ARMARX_DEBUG << "Sensor pose for sensor " << sensorFrame << " is " << std::endl
                         << robot_T_sensor.matrix();

            return robot_T_sensor;
        };

        // set sensor poses
        const auto sensorIds = mappingAdapter->laserSensorIds();
        std::for_each(sensorIds.begin(),
                      sensorIds.end(),
                      [&](const std::string& sensorId)
                      { mappingAdapter->setSensorPose(sensorId, sensorPose(sensorId)); });
    }

    std::optional<Eigen::Affine3f>
    CartographerMappingAndLocalization::globalPose() const
    {
        if (not map_T_robot.has_value())
        {
            return std::nullopt;
        }

        return world_T_map * map_T_robot.value();
    }

    // Lifecycle management methods

    void
    CartographerMappingAndLocalization::onInitComponent()
    {
        SelfLocalization::onInitComponent();
    }

    Eigen::Affine3f
    CartographerMappingAndLocalization::worldToMapTransform() const
    {
        ARMARX_TRACE;

        if (properties.mode == Mode::Mapping)
        {
            ARMARX_IMPORTANT << "Mapping mode. World to map transform set to identity";
            return Eigen::Affine3f::Identity();
        }

        // store data alongside the map file
        const fs::path mapPath = ArmarXDataPath::resolvePath(properties.mapPath);

        fs::path jsonFilename = properties.mapToLoad;
        jsonFilename.replace_extension("json");
        const fs::path jsonLoadPath = mapPath / jsonFilename;

        ARMARX_CHECK(fs::is_regular_file(jsonLoadPath))
            << "In mapping mode, the json registration file " << jsonLoadPath << " has to exist!";

        ARMARX_IMPORTANT << "Loading registration result: " << jsonLoadPath.string();

        if (not fs::is_regular_file(jsonLoadPath))
        {
            return Eigen::Affine3f::Identity();
        }

        std::ifstream ifs(jsonLoadPath.string(), std::ios::in);
        const auto j = nlohmann::json::parse(ifs);

        ARMARX_CHECK(ifs.is_open());
        ARMARX_CHECK(not ifs.fail());

        Eigen::Affine3f world_T_map = Eigen::Affine3f::Identity();
        world_T_map.translation().x() = j["x"];
        world_T_map.translation().y() = j["y"];
        world_T_map.linear() =
            Eigen::AngleAxisf(j["yaw"], Eigen::Vector3f::UnitZ()).toRotationMatrix();

        ARMARX_IMPORTANT << "Loading registration result is " << world_T_map.matrix();

        return world_T_map;
    }

    void
    CartographerMappingAndLocalization::onConnectComponent()
    {
        const bool reconnect = mappingAdapter != nullptr;
        if (reconnect)
        {
            onReconnectComponent();
            return;
        }

        ARMARX_INFO << "SelfLocalization::onConnectComponent";
        ARMARX_TRACE;
        SelfLocalization::onConnectComponent();

        // if (laserScansMemoryWriter)
        // {
        // laserScansMemoryWriter->connect();
        // }

        occupancyGridMemoryWriter.connect();

        // robotUnitReader.reset();

        slamResultReporter.emplace(debugObserver, "CartographerMappingAndLocalization_slam_result");
        laserDataReporter.emplace(debugObserver, "CartographerMappingAndLocalization_laser_data");
        odomDataReporter.emplace(debugObserver, "CartographerMappingAndLocalization_odom_data");

        world_T_map = worldToMapTransform();

        ARMARX_TRACE;
        setupMappingAdapter();
        ARMARX_CHECK_NOT_NULL(mappingAdapter);

        robotReader.connect();

        map_T_map_correct = std::nullopt;

        // Only in the mapping mode, allow the user to create a map.
        // Although this would be possible in localization mode as well, this might
        // be confusing and likely results in suboptimal maps. It's therefore prefered to record
        // the data using the TopicRecorder such that you can create the map afterwards.
        switch (properties.mode)
        {
            case Mode::Mapping:
                mappingRemoteGui = std::make_unique<MappingRemoteGui>(remoteGui, *this);
                break;
            case Mode::Localization:
                localizationRemoteGui = std::make_unique<LocalizationRemoteGui>(remoteGui, *this);
                ARMARX_INFO << "The remote GUI will only be shown in 'MAPPING' mode.";
                break;
        }


        if (properties.enableVisualization)
        {
            arvizDrawer = std::make_unique<ArVizDrawer>(getArvizClient(), world_T_map);
            arVizDrawerMapBuilder = std::make_unique<ArVizDrawerMapBuilder>(
                getArvizClient(), mappingAdapter->getMapBuilder(), world_T_map);
        }


        // occupancyGridTask = new SimplePeriodicTask<>(
        //     [&]()
        //     {
        //         const auto grids =
        //             armarx::cartographer::utils::occupancyGrids(mappingAdapter->getMapBuilder(), 0);

        //         if (grids.empty())
        //         {
        //             return;
        //         }

        //         armem::vision::OccupancyGrid grid = grids.front();
        //         grid.pose.translation() *= 1'000; // [m] to [mm]
        //         grid.resolution *= 1'000; // [m] to [mm]

        //         ARMARX_CHECK_EQUAL(grid.frame, MapFrame) << "Assuming map frame";
        //         grid.pose = world_T_map * grid.pose; // map to global frame
        //         grid.frame = GlobalFrame;

        //         // TODO latest timestamp
        //         occupancyGridMemoryWriter.store(
        //             grid, MapFrame, getName(), IceUtil::Time::now().toMicroSeconds());
        //     },
        //     properties.occupancyGridMemoryUpdatePeriodMs);
            
        // occupancyGridTask->start();

        laserMessageQueue.enable();
        odomMessageQueue.enable();

        // wait a bit such that all threads are initialized
        std::this_thread::sleep_for(std::chrono::milliseconds(100));


        receivingData.store(true);
        ARMARX_INFO << "Will now process incoming messages.";


        // ARMARX_INFO << "Enabling robot unit streaming";
        // robotUnitReader = std::make_unique<components::cartographer_mapping_and_localization::RobotUnitReader>(*robotUnit, odomMessageQueue, *odomDataReporter);

        if(odometryQueryTask)
        {
            odometryQueryTask->stop();
            odometryQueryTask = nullptr;
        }

        odometryQueryTask = new SimplePeriodicTask<>(
            [&]()
            {
                armem::robot::RobotDescription robotDescription{
                    .name = properties.robotName
                };

                const auto timestamp = armarx::Clock::Now();

                ARMARX_TRACE;
                if (not receivingData.load())
                {
                    ARMARX_INFO << deactivateSpam(1) << "Receiving data is deactivated";
                    return;
                }

                if(auto odomPose = robotReader.queryOdometryPose(robotDescription, timestamp))
                {

                    if(properties.useNativeOdometryTimestamps)
                    {
                        // did we retrieve the same data as last time?
                        if(lastTimestamp >= odomPose->header.timestamp.toMicroSecondsSinceEpoch())
                        {
                            return;
                        }

                        lastTimestamp = odomPose->header.timestamp.toMicroSecondsSinceEpoch();
                    }
                    else
                    {
                        lastTimestamp = timestamp.toMicroSecondsSinceEpoch();
                    }


                    CartographerAdapter::OdomData odomData
                    {
                        .position = odomPose->transform.translation() / 1000, // mm -> m
                        .orientation = Eigen::Quaternionf(odomPose->transform.linear()),
                        .timestamp = lastTimestamp
                    };

                    // ARMARX_INFO << deactivateSpam(1) << "received odom data from memory";
                    odomMessageQueue.push(odomData);

                    if (odomDataReporter)
                    {
                        odomDataReporter->add(odomData.timestamp);
                    }
                }else
                {
                    ARMARX_WARNING << deactivateSpam(10) << "Failed to query odom pose";
                }

            },
            properties.odomQueryPeriodMs);

        odometryQueryTask->start();

        ARMARX_DEBUG << "onConnectComponent completed";
    }

    void
    CartographerMappingAndLocalization::onReconnectComponent()
    {
        ARMARX_INFO << "CartographerMappingAndLocalization::reconnecting ...";
        SelfLocalization::onReconnectComponent();

        if (localizationRemoteGui)
        {
            localizationRemoteGui->enable();
        }

        if (mappingRemoteGui)
        {
            mappingRemoteGui->enable();
        }

        slamResultReporter.emplace(debugObserver, "slam_result");
        laserDataReporter.emplace(debugObserver, "laser_data");
        odomDataReporter.emplace(debugObserver, "odom_data");


        map_T_map_correct = std::nullopt;
        if (arvizDrawer)
        {
            ARMARX_TRACE;
            arvizDrawer->setWorldToMapTransform(world_T_map);
            arvizDrawer->setMapCorrection(Eigen::Affine3f::Identity());
        }

        laserMessageQueue.enable();
        odomMessageQueue.enable();

        receivingData.store(true);
        ARMARX_INFO << "Will now process incoming messages.";
        ARMARX_DEBUG << "onReconnectComponent completed";
    }

    void
    CartographerMappingAndLocalization::onDisconnectComponent()
    {
        ARMARX_TRACE;

        ARMARX_WARNING << "Disconnecting ...";

        receivingData.store(false);

        if (localizationRemoteGui)
        {
            localizationRemoteGui->disable();
        }

        if (mappingRemoteGui)
        {
            mappingRemoteGui->disable();
        }

        laserMessageQueue.clear();
        odomMessageQueue.clear();

        laserMessageQueue.waitUntilProcessed();
        odomMessageQueue.waitUntilProcessed();


        slamResultReporter.reset();
        laserDataReporter.reset();
        odomDataReporter.reset();

        SelfLocalization::onDisconnectComponent();
    }

    void
    CartographerMappingAndLocalization::onExitComponent()
    {
        SelfLocalization::onExitComponent();

        ARMARX_DEBUG << "Stopping task";

        // this causes remote guis and arviz layers to be cleaned up
        if (mappingRemoteGui)
        {
            mappingRemoteGui->shutdown();
        }

        if (localizationRemoteGui)
        {
            localizationRemoteGui->shutdown();
        }

        if (arvizDrawer)
        {
            arvizDrawer.reset();
        }

    }

    void
    CartographerMappingAndLocalization::onLocalSlamData(const LocalSlamData& slamData)
    {
        // publish localization
        ARMARX_DEBUG << "Received SLAM result";

        map_T_robot =
            map_T_map_correct.value_or(Eigen::Affine3f::Identity()) * toMM(slamData.global_pose());
        publishSelfLocalization(
            {map_T_robot.value(), IceUtil::Time::microSeconds(slamData.timestamp)});


        heartbeat->heartbeat();

        // publish visualization stuff
        if (arvizDrawer != nullptr && throttlerArviz.check(slamData.timestamp))
        {
            ARMARX_DEBUG << "arvizDrawer->onLocalSlamData";
            arvizDrawer->onLocalSlamData(slamData);
        }

        if (slamResultReporter)
        {
            slamResultReporter->add(slamData.timestamp);
        }
    }

    void
    dumpSubMapsToDisk(const SubMapDataVector& submapData)
    {
        static int i{0};

        for (const auto& sd : submapData)
        {
            cv::Mat2b img;
            cv::flip(sd.submap, img, 1); // horizontally

            std::vector<cv::Mat> channels(img.channels());
            cv::split(img, channels);

            // save the submap (occupancy grid) ...
            std::stringstream ss;
            ss << "/tmp/cartographer_mapping/submap_";
            ss << std::setw(10) << std::setfill('0') << ++i;
            ss << ".png";
            std::string s = ss.str();

            cv::imwrite(s, channels.at(0));

            // ... and its mask
            std::stringstream ssm;
            ssm << "/tmp/cartographer_mapping/submap_mask_";
            ssm << std::setw(10) << std::setfill('0') << i;
            ssm << ".png";
            std::string sm = ssm.str();

            cv::imwrite(sm, channels.at(1));
        }
    }

    void
    CartographerMappingAndLocalization::onLaserSensorData(const LaserScannerData& laserData)
    {
        if (arvizDrawer != nullptr)
        {
            arvizDrawer->onLaserSensorData(laserData);
        }
    }

    void
    CartographerMappingAndLocalization::onCreateMapButtonClicked()
    {
        ARMARX_TRACE;

        ARMARX_CHECK_NOT_NULL(mappingRemoteGui);
        ARMARX_CHECK_NOT_NULL(mappingAdapter);

        // check if sensor data has been received
        if (not mappingAdapter->hasReceivedSensorData())
        {
            ARMARX_WARNING << "No sensor data received yet. Will not create map.";
            return;
        }

        ARMARX_INFO << "Map button clicked which triggers map creation.";

        // ensure that only one map can be created at a time
        // mappingRemoteGui->disable();

        receivingData.store(false);

        ARMARX_INFO << "Waiting until all data is processed.";
        laserMessageQueue.waitUntilProcessed();
        odomMessageQueue.waitUntilProcessed();

        ARMARX_INFO << "Creating map.";
        mappingAdapter->createMap();
        //        mappingRemoteGui->enable();
    }

    void
    CartographerMappingAndLocalization::reportSensorValues(
        const ::std::string&,
        const ::std::string& name,
        const ::armarx::LaserScan& scan,
        const ::armarx::TimestampBasePtr& timestamp,
        const ::Ice::Current&)
    {
        ARMARX_TRACE;

        std::lock_guard g{inputMtx};

        if (not receivingData.load())
        {
            return;
        }

        laserMessageQueue.push(LaserScannerMessage{name, scan, timestamp->timestamp});
        ARMARX_DEBUG << "Inserted laser scanner data into laserMessageQueue";

        if (laserDataReporter)
        {
            laserDataReporter->add(timestamp->timestamp);
        }

        if (properties.mode == Mode::Mapping)
        {
            // if (throttlerLaserScansMemoryWriter.check(timestamp->timestamp) and
            //     laserScansMemoryWriter != nullptr)
            // {
            //     laserScansMemoryWriter->storeSensorData(
            //         scan, name, getAgentName(), timestamp->timestamp);
            // }
        }
    }

    namespace util
    {

        armem::robot_state::Transform
        toTransform(const TransformStamped& transform)
        {
            return {.header = {.parentFrame = transform.header.parentFrame,
                               .frame = transform.header.frame,
                               .agent = transform.header.agent,
                               .timestamp = armarx::Duration::MicroSeconds(
                                   transform.header.timestampInMicroSeconds)},
                    .transform = Eigen::Affine3f(transform.transform)};
        }

    } // namespace util

//    void CartographerMappingAndLocalization::reportOdometryPose(
//        const TransformStamped& odometryPose, const Ice::Current&)
//    {
//        std::lock_guard g{inputMtx};

//        ARMARX_TRACE;
//        if (not receivingData.load())
//        {
//            return;
//        }

//        Eigen::Affine3f odomPose(odometryPose.transform);

//        constexpr float mmToM = 1 / 1000.;

//        CartographerAdapter::OdomData odomData;
//        odomData.position = odomPose.translation() * mmToM;
//        odomData.orientation = Eigen::Quaternionf(odomPose.linear());
//        odomData.timestamp = odometryPose.header.timestampInMicroSeconds;

//        ARMARX_TRACE;
//        odomMessageQueue.push(odomData);
//        ARMARX_DEBUG << "Inserted odom data into odomMessageQueue";

//        Eigen::Affine3f odomVisuPose(odometryPose.transform);
//        odomVisuPose.translation() /= 1000.;

//        if (arvizDrawer)
//        {
//            arvizDrawer->onOdomPose(odomVisuPose);
//        }

//        if (odomDataReporter)
//        {
//            odomDataReporter->add(odomData.timestamp);
//        }

//        // TODO(fabian.reister): this should be published by robot state component
//        // getTransformWriter().commitTransform(util::toTransform(odometryPose));
//    }

    void
    CartographerMappingAndLocalization::onLocalizationCorrection(
        const Eigen::Affine3f& map_T_map_correct)
    {

        if (not receivingData.load())
        {
            ARMARX_INFO << "onLocalizationCorrection() requested but component is deactivated. "
                           "Will not process request.";
            return;
        }

        this->map_T_map_correct = map_T_map_correct;

        if (arvizDrawer)
        {
            arvizDrawer->setMapCorrection(map_T_map_correct);
        }
    }

    void
    CartographerMappingAndLocalization::onApplyLocalizationCorrection()
    {
        if (not receivingData.load())
        {
            ARMARX_INFO << "onApplyLocalizationCorrection() requested but component is "
                           "deactivated. Will not process request.";
            return;
        }

        if(helperTask)
        {
            helperTask->stop();
            helperTask = nullptr;
        }

        // In onDisconnect(), we want to stop the LocalizationRemoteGui, which is the caller of this function.
        // Therefore, delegate this to a new task.
        helperTask = new SimpleRunningTask<>(
            [&]()
            {
                onDisconnectComponent();

                std::this_thread::sleep_for(std::chrono::milliseconds(500));
                mappingAdapter.reset();
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                onConnectComponent();
            },
            "HelperTask");

        helperTask->start();
    }
//    void CartographerMappingAndLocalization::reportOdometryVelocity(
//        const TwistStamped& velocity, const Ice::Current&)
//    {
//    }
} // namespace armarx::cartographer
