/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cstdint>
#include <memory>
#include <optional>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/util/IceReportSkipper.h>
#include <ArmarXCore/core/util/Throttler.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include "RobotAPI/libraries/armem_robot_state/client/common/RobotReader.h"
#include <RobotAPI/libraries/armem_vision/client/laser_scans/Writer.h>
#include <RobotAPI/libraries/armem_vision/client/occupancy_grid/Writer.h>
#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/HeartbeatComponentPlugin.h>

// #include "RobotComponents/components/CartographerMappingAndLocalization/RobotUnitReader.h"
#include "ArmarXCore/libraries/logging/FrequencyReporter.h"
#include <RobotComponents/components/CartographerMappingAndLocalization/LocalizationRemoteGui.h>
#include <RobotComponents/libraries/SelfLocalization/SelfLocalization.h>
#include <RobotComponents/components/CartographerMappingAndLocalization/MappingRemoteGui.h>
#include <RobotComponents/interface/components/CartographerMappingAndLocalizationComponentInterface.h>
#include <RobotComponents/libraries/cartographer/CartographerAdapter.h>
#include <RobotComponents/libraries/cartographer/MessageQueue.h>


// forward decl
namespace cartographer::mapping
{
    class MapBuilderInterface;
} // namespace cartographer::mapping

namespace armarx
{
    class TransformWriterInterface;
    class TransformReaderInterface;
    class FrequencyReporter;
} // namespace armarx

namespace armarx::cartographer
{
    // forward decl
    class ArVizDrawer;
    class ArVizDrawerMapBuilder;

    /**
     * @defgroup Component-CartographerMappingAndLocalization CartographerMappingAndLocalization
     * @ingroup RobotComponents-Components
     * A description of the component CartographerMappingAndLocalization.
     *
     * @class CartographerMappingAndLocalization
     * @ingroup Component-CartographerMappingAndLocalization
     * @brief Brief description of class CartographerMappingAndLocalization.
     *
     * Detailed description of class CartographerMappingAndLocalization.
     */
    class CartographerMappingAndLocalization :
        virtual public CartographerMappingAndLocalizationInterface,
        virtual public ArVizComponentPluginUser,
        virtual public SlamDataCallable,
        virtual public RemoteGuiCallee,
        virtual public LocalizationRemoteGuiCallee,
        virtual public SelfLocalization
    {
    public:
        CartographerMappingAndLocalization();
        virtual ~CartographerMappingAndLocalization();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        // LaserScannerUnitListener interface
        void reportSensorValues(const ::std::string&,
                                const ::std::string&,
                                const ::armarx::LaserScan&,
                                const ::armarx::TimestampBasePtr&,
                                const ::Ice::Current&) override;

        // PlatformUnitListener interface
        // TODO: use streaming (RobotUnit)
//        void reportOdometryPose(const TransformStamped& odometryPose, const Ice::Current&) override;
//        void reportOdometryVelocity(const TwistStamped& velocity, const Ice::Current&) override;

        // SlamDataCallable interface
        void onLocalSlamData(const LocalSlamData& slamData) override;
        // void onGraphOptimized(const OptimizedGraphData& graphData) override;
        // void onGridMap(const SubMapDataVector& submapData) override;
        void onLaserSensorData(const LaserScannerData& laserData) override;

        // RemoteGuiCallee
        void onCreateMapButtonClicked() override;

        enum class Mode
        {
            Mapping,
            Localization
        };

        void onLocalizationCorrection(const Eigen::Affine3f& map_T_map_correct) override;
        void onApplyLocalizationCorrection() override;


    protected:
        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        void onReconnectComponent();

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        Eigen::Affine3f worldToMapTransform() const override;

    private:
        void setupMappingAdapter();

        //! adapter to cartographer
        std::unique_ptr<CartographerAdapter> mappingAdapter;
        CartographerAdapter::Config adapterConfig;

        //! ArViz plotter
        std::unique_ptr<ArVizDrawer> arvizDrawer;

        //! Remote GUI to trigger map creation
        std::unique_ptr<MappingRemoteGui> mappingRemoteGui;
        std::unique_ptr<LocalizationRemoteGui> localizationRemoteGui;

        // queues to handle incoming messages
        MessageQueue<LaserScannerMessage> laserMessageQueue;
        MessageQueue<CartographerAdapter::OdomData> odomMessageQueue;

        //! helper flag to prevent creation of multiple maps
        bool mapCreated{false};

        // Eigen::Affine3f globalPoseFromLongTermMemory(const std::string& agentName, const auto& robot);

        // FIXME remove
        // std::atomic<int64_t> lastTimestamp{0};

        // topic publisher
        RemoteGuiInterfacePrx remoteGui;
        DebugObserverInterfacePrx debugObserver;

        // others
        // armem::vision::laser_scans::client::Writer laserScansMemoryWriter;
        armem::vision::occupancy_grid::client::Writer occupancyGridMemoryWriter;
        armem::robot_state::RobotReader robotReader;

        //! properties
        struct Properties
        {
            std::string mapPath = "./RobotComponents/maps/";
            std::string mapToLoad;
            std::string cartographerConfigPath = "./RobotComponents/cartographer/config/";
            Mode mode = Mode::Mapping;
            bool enableVisualization = true;

            std::string robotName = "Armar6";

            int odomQueryPeriodMs = 20;
            int occupancyGridMemoryUpdatePeriodMs = 1000;

            bool useNativeOdometryTimestamps = true;
        } properties;

        Throttler throttlerLaserScansMemoryWriter{10.F};
        Throttler throttlerOccupancyGridMemoryWriter{10.F};

        Throttler throttlerArviz{10.F};

        std::unique_ptr<ArVizDrawerMapBuilder> arVizDrawerMapBuilder;

        std::optional<FrequencyReporter> laserDataReporter;
        std::optional<FrequencyReporter> odomDataReporter;
        std::optional<FrequencyReporter> slamResultReporter;

        // std::unique_ptr<components::cartographer_mapping_and_localization::RobotUnitReader> robotUnitReader;

        // SimplePeriodicTask<>::pointer_type occupancyGridTask;
        SimplePeriodicTask<>::pointer_type odometryQueryTask;

        // std::unique_ptr<armem::occupancy_grid::client::Writer> occupancyGridWriter;

        SimpleRunningTask<>::pointer_type helperTask;

        Eigen::Affine3f world_T_map  = Eigen::Affine3f::Identity();
        std::optional<Eigen::Affine3f> map_T_map_correct  = std::nullopt;
        std::optional<Eigen::Affine3f> map_T_robot = std::nullopt;

        std::atomic_bool receivingData{false};


        std::optional<Eigen::Affine3f> globalPose() const;

        armarx::plugins::HeartbeatComponentPlugin* heartbeat;
        // armarx::plugins::RobotUnitComponentPlugin* robotUnit;


        std::mutex inputMtx;


        // used within the odometry task
        int64_t lastTimestamp = 0;


    };
} // namespace armarx::cartographer
