/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include "ArmarXCore/core/services/tasks/TaskUtil.h"

#include "RobotAPI/libraries/RobotUnitDataStreamingReceiver/RobotUnitDataStreamingReceiver.h"
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include "ArmarXCore/libraries/logging/FrequencyReporter.h"
#include "RobotComponents/libraries/cartographer/MessageQueue.h"
#include <RobotComponents/libraries/cartographer/CartographerAdapter.h>

namespace armarx::plugins
{
    class RobotUnitComponentPlugin;
    class DebugObserverComponentPlugin;
} // namespace armarx::plugins

namespace armarx::components::cartographer_mapping_and_localization
{

    /**
     * @brief The RobotUnitReader class. The aim of this class is to receive data from the robot unit via streaming.
     * Once data is received, it will be added to the odometry message queue.
     * 
     * 
     */
    class RobotUnitReader
    {
    public:
        RobotUnitReader(
            armarx::plugins::RobotUnitComponentPlugin& robotUnitPlugin,
            MessageQueue<armarx::cartographer::CartographerAdapter::OdomData>& odomMessageQueue,
            FrequencyReporter& odomDataReporter);

        ~RobotUnitReader();

    protected:
        /**
        * @brief Sets up the robot unit streaming channel
        * 
        */
        void connect();

        /**
         * @brief Reads the available data and fills the odomMessageQeueu
         * 
         */
        void readRobotUnitData();

        std::shared_ptr<armarx::cartographer::CartographerAdapter::OdomData>
        convert(const RobotUnitDataStreaming::TimeStep& data);


    private:
        armarx::plugins::RobotUnitComponentPlugin& robotUnitPlugin;

        /// reference to the message queue that will be filled when odometry data is available
        MessageQueue<armarx::cartographer::CartographerAdapter::OdomData>& odomMessageQueue;

        /// logging: will allow introspection of the reliability of the received data
        FrequencyReporter& odomDataReporter;


        // do not change this or mysterious things will happen
        const std::string sensorDataPrefix = "sens.Platform.relativePosition*";


        RobotUnitDataStreamingReceiverPtr receiver;
        RobotUnitDataStreaming::DataStreamingDescription description;


        SimplePeriodicTask<>::pointer_type periodicTask;


        const float updatePeriodMs = 10;

    };
} // namespace armarx::components::cartographer_mapping_and_localization
