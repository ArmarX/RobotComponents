/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

namespace armarx
{

    class RemoteGuiCallee
    {
    public:
        virtual void onCreateMapButtonClicked() = 0;
    };

    class MappingRemoteGui
    {
    public:
        MappingRemoteGui(const RemoteGuiInterfacePrx& remoteGui, RemoteGuiCallee& callee);

        void shutdown();

        void enable();
        void disable();

    private:
        RemoteGuiInterfacePrx remoteGui;

        RunningTask<MappingRemoteGui>::pointer_type runningTask;

        RemoteGuiCallee& callee;

        RemoteGui::TabProxy tab;

        void createRemoteGuiTab();

        // thread function
        void run();

        void handleEvents(RemoteGui::TabProxy& tab);

        const std::string REMOTE_GUI_TAB_MAME = "Mapping";
        const std::string CREATE_MAP_BUTTON = "Create map!";
    };

} // namespace armarx
