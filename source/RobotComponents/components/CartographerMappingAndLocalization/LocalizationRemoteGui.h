/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <Eigen/Geometry>
#include <Eigen/src/Core/Matrix.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include "ArmarXGui/libraries/RemoteGui/Client/Widgets.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Tab.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

namespace armarx
{

    class LocalizationRemoteGuiCallee
    {
    public:
        virtual void onLocalizationCorrection(const Eigen::Affine3f& correction) = 0;

        virtual void onApplyLocalizationCorrection() = 0;
    };

    class LocalizationRemoteGui
    {
    public:
        LocalizationRemoteGui(const RemoteGuiInterfacePrx& remoteGui,
                              LocalizationRemoteGuiCallee& callee);

        ~LocalizationRemoteGui();

        void shutdown();

        void enable();
        void disable();

        void reset();

    private:
        std::mutex mtx;

        std::mutex handleEventsMtx;
        RemoteGuiInterfacePrx remoteGui;

        RunningTask<LocalizationRemoteGui>::pointer_type runningTask;

        LocalizationRemoteGuiCallee& callee;

        RemoteGui::TabProxy tab;

        void createRemoteGuiTab();

        // thread function
        void run();

        void handleEvents(RemoteGui::TabProxy& tab);

        const std::string REMOTE_GUI_TAB_MAME = "Cartographer";

        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::HBoxLayout hbox;

            struct
            {
                armarx::RemoteGui::Client::GroupBox group;

                armarx::RemoteGui::Client::FloatSpinBox x;
                armarx::RemoteGui::Client::FloatSpinBox y;
                armarx::RemoteGui::Client::FloatSlider yaw;
            } localizationCorrection;

            struct
            {
                armarx::RemoteGui::Client::GroupBox group;

                armarx::RemoteGui::Client::Button applyButton;
            } controlGroup;
        };

        Eigen::Vector3f localizationCorrection = Eigen::Vector3f::Zero();

        RemoteGuiTab guiTab;
    };

} // namespace armarx
