#include "RobotComponents/components/CartographerMappingAndLocalization/MappingRemoteGui.h"

#include "ArmarXCore/core/time/CycleUtil.h"
#include "ArmarXCore/statechart/ParameterMapping.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/IntegerWidgets.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/LayoutWidgets.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/StaticWidgets.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

namespace armarx
{

    namespace gui = RemoteGui;

    MappingRemoteGui::MappingRemoteGui(const RemoteGuiInterfacePrx& remoteGui, RemoteGuiCallee& callee) :
        remoteGui(remoteGui),
        runningTask(new RunningTask<MappingRemoteGui>(this, &MappingRemoteGui::run)),
        callee(callee)
    {
        createRemoteGuiTab();

        tab = RemoteGui::TabProxy(remoteGui, REMOTE_GUI_TAB_MAME);

        runningTask->start();
    }

    void MappingRemoteGui::createRemoteGuiTab()
    {
        ARMARX_CHECK_NOT_NULL(remoteGui); // createRemoteGui() has to be called first

        auto boxLayout = gui::makeVBoxLayout();

        const gui::WidgetPtr label = gui::makeTextLabel("Use this button to create a map after you collected enough data.");
        const gui::WidgetPtr button = gui::makeButton(CREATE_MAP_BUTTON).toolTip("Create and save the map.");
        const gui::WidgetPtr line = gui::makeHBoxLayout().children({label, button});


        boxLayout.addChild(line);
        remoteGui->createTab(REMOTE_GUI_TAB_MAME, boxLayout);
    }

    void MappingRemoteGui::run()
    {
        constexpr int kCycleDurationMs = 100;

        CycleUtil c(kCycleDurationMs);
        while (!runningTask->isStopped())
        {
            tab.receiveUpdates();

            handleEvents(tab);

            tab.sendUpdates();

            c.waitForCycleDuration();
        }
    }

    void MappingRemoteGui::handleEvents(RemoteGui::TabProxy& tab)
    {
        // Detect button clicks and update the counter accordingly
        const RemoteGui::ButtonProxy createMapButton = tab.getButton(CREATE_MAP_BUTTON);
        if (createMapButton.clicked())
        {
            callee.onCreateMapButtonClicked();
        }
    }


    void MappingRemoteGui::shutdown()
    {

        if (!runningTask->isStopped())
        {
            runningTask->stop();
        }

        ARMARX_DEBUG << "Removing tab: " << REMOTE_GUI_TAB_MAME;
        remoteGui->removeTab(REMOTE_GUI_TAB_MAME);
    }

    void MappingRemoteGui::enable(){
        tab.internalSetDisabled(CREATE_MAP_BUTTON, false);
    }

    void MappingRemoteGui::disable(){
        tab.internalSetDisabled(CREATE_MAP_BUTTON, true);
    }

} // namespace armarx
