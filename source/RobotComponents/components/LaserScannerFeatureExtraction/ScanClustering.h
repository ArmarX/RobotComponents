/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/LaserScannerUnit.h>

namespace armarx::laser_scanner_feature_extraction
{
    namespace detail
    {
        struct ScanClusteringParams
        {
            // Clustering options to decide whether a point belongs to the cluster
            float distanceThreshold; // [mm]
            float angleThreshold;    // [rad]

            /// Range filter: only consider points that are closer than maxDistance
            float maxDistance; // [mm]
        };
    } // namespace detail

    class ScanClustering
    {
      public:
        using Params   = detail::ScanClusteringParams;
        using Clusters = std::vector<LaserScan>;

        ScanClustering(const Params& params);

        /**
        * @brief Performs cluster detection on a full laser scan.
        *
        * @param scan A full scan
        * @return The input scan split into clusters.
        */
        Clusters detectClusters(const LaserScan& scan);

      protected:
        const LaserScan& cluster() const;

        bool add(const LaserScanStep& scanStep);
        bool supports(const LaserScanStep& scanStep);

        void clear();

      private:
        LaserScan scan;

        const Params params;
    };

} // namespace armarx::laser_scanner_feature_extraction