/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::LaserScannerFeatureExtraction
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ArmarXCore/core/util/Throttler.h"
#include "ArmarXCore/libraries/logging/FrequencyReporter.h"
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include "RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h"
#include "RobotAPI/libraries/armem/client/forward_declarations.h"
#include "RobotAPI/libraries/armem/client/plugins/PluginUser.h"
#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/armem_vision/client/laser_scanner_features/Writer.h>

#include "RobotComponents/components/LaserScannerFeatureExtraction/ChainApproximation.h"
#include "RobotComponents/components/LaserScannerFeatureExtraction/ScanClustering.h"
#include "RobotComponents/components/LaserScannerFeatureExtraction/geometry.h"
#include <RobotComponents/interface/components/LaserScannerFeatureExtraction/LaserScannerFeatureExtraction.h>
#include <RobotComponents/libraries/cartographer/MessageQueue.h>
#include <RobotComponents/libraries/cartographer/types.h>

#include "ArVizDrawer.h"
#include "FeatureExtractor.h"

namespace armarx::laser_scanner_feature_extraction
{

    // class FeatureExtractor;
    // class ArVizDrawer;

    /**
     * @defgroup Component-LaserScannerFeatureExtraction LaserScannerFeatureExtraction
     * @ingroup RobotComponents-Components
     * A description of the component LaserScannerFeatureExtraction.
     *
     * @class LaserScannerFeatureExtraction
     * @ingroup Component-LaserScannerFeatureExtraction
     * @brief Brief description of class LaserScannerFeatureExtraction.
     *
     * Detailed description of class LaserScannerFeatureExtraction.
     */
    class LaserScannerFeatureExtraction :
        virtual public armarx::Component,
        virtual public armarx::DebugObserverComponentPluginUser
        // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        ,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public LaserScannerUnitListener,
        virtual public RobotStateComponentPluginUser,
        virtual public armem::ClientPluginUser

    {
    public:
        LaserScannerFeatureExtraction();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */

        // LaserScannerUnitListener interface
        void reportSensorValues(const std::string& device,
                                const std::string& name,
                                const LaserScan& scan,
                                const TimestampBasePtr& timestamp,
                                const Ice::Current& /*unused*/) override;

    private:
        void onFeatures(const cartographer::LaserScannerMessage& data,
                        const std::vector<Features>& features);

        void publishFeatures(const armem::vision::LaserScannerFeatures& features,
                             const armem::Time& timestamp);

        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */

        // Private member variables go here.
        MessageQueue<cartographer::LaserScannerMessage> laserMessageQueue;

        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            // input message queue size
            size_t queueSize{2};

            //
            float robotConvexHullMargin{50.F};

            //
            struct CableFix
            {
                bool enabled = {true};
                float cableAreaWidth{400};
                float maxAreaTh{50 * 50};
            } cableFix;

            ScanClustering::Params scanClusteringParams{
                .distanceThreshold = 30.F, // [mm]
                // we know the scan resolution which produces a bit more than 1000 points
                .angleThreshold = 2 * M_PIf32 / 1000.F * 5.F, // [rad]
                .maxDistance = 3'000.F // [mm]
            };

            ChainApproximation::Params chainApproximationParams{
                .distanceTh = 40.F // [mm]
            };

            struct ArViz
            {
                bool drawRawPoints = {true};
            } arviz;
        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

        std::unique_ptr<FeatureExtractor> featureExtractor;
        std::unique_ptr<ArVizDrawer> arVizDrawer;

        std::unique_ptr<FrequencyReporter> frequencyReporterPublish;
        std::unique_ptr<FrequencyReporter> frequencyReporterSubscribe;

        std::unordered_map<std::string, Throttler> throttlers;

        VirtualRobot::RobotPtr robot;

        LaserScannerFeaturesListenerPrx featuresTopic;

        VirtualRobot::MathTools::ConvexHull2DPtr robotHull;
        VirtualRobot::MathTools::ConvexHull2DPtr cableArea;


        armem::vision::laser_scanner_features::client::Writer laserScannerFeatureWriter;
    };
} // namespace armarx::laser_scanner_feature_extraction
