/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>
#include "RobotAPI/libraries/armem_vision/types.h"

namespace armarx::laser_scanner_feature_extraction
{

    // struct Ellipsoid
    // {
    //     Eigen::Vector2f center;
    //     float angle;

    //     Eigen::Vector2f radii;

    //     Eigen::Affine2f pose() const noexcept;
    // };

    using Ellipsoid = armem::vision::Ellipsoid;

    /**
     * @brief Minimum volume enclosing ellipsoid (MVEE) for a set of points.
     *
     * See https://de.mathworks.com/matlabcentral/fileexchange/9542-minimum-volume-enclosing-ellipsoid
     *
     */
    class EnclosingEllipsoid : public Ellipsoid
    {
    public:
        using Point  = Eigen::Vector2f;
        using Points = std::vector<Point>;

        EnclosingEllipsoid(const Points& points);

    private:
        /**
         * @brief Computes the enclosing ellipsoid for the given points by using Khachiyan's Algorithm.
         *
         * The implementation is based on
         * https://github.com/minillinim/ellipsoid/blob/master/ellipsoid.py
         *
         * @return true
         * @return false
         */
        bool compute(const Points& points);
    };

} // namespace armarx::laser_scanner_feature_extraction
