/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <SimoxUtility/color/Color.h>
#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/components/ArViz/Client/ScopedClient.h>

#include "RobotComponents/components/LaserScannerFeatureExtraction/FeatureExtractor.h"

namespace armarx::cartographer
{
    struct LaserScannerMessage;
}

namespace armarx::laser_scanner_feature_extraction
{

    struct LineSegment2Df;
    // struct Circle;
    // struct Ellipsoid;

    class ArVizDrawer 
    {
    public:
        using Points = std::vector<Eigen::Vector2f>;

        ArVizDrawer(armarx::viz::Client& arviz) : arviz(arviz)
        {
        }


        void draw(const std::vector<Features>& features,
                  const std::string& frame,
                  const Eigen::Isometry3f& globalSensorPose);

        void draw(const cartographer::LaserScannerMessage& msg,
                  const Eigen::Isometry3f& globalSensorPose,
                  const simox::Color& color);

        void draw(const std::string& layerName,
                  const VirtualRobot::MathTools::ConvexHull2D& robotHull,
                  const Eigen::Isometry3f& robotGlobalPose,
                  const simox::Color& color = simox::Color::red(100, 80));

    private:
        void drawCircles(const std::vector<Features>& features,
                         const std::string& frame,
                         const Eigen::Isometry3f& globalSensorPose);
        void drawCircle(viz::Layer& layer,
                        const Circle& circle,
                        const Eigen::Isometry3f& globalSensorPose);

        void drawConvexHulls(const std::vector<Features>& features,
                             const std::string& frame,
                             const Eigen::Isometry3f& globalSensorPose);
        void drawConvexHull(viz::Layer& layer,
                            const VirtualRobot::MathTools::ConvexHull2D& hull,
                            const Eigen::Isometry3f& globalSensorPose,
                            const simox::Color& color);

        void drawEllipsoids(const std::vector<Features>& features,
                            const std::string& frame,
                            const Eigen::Isometry3f& globalSensorPose);

        void drawEllipsoid(viz::Layer& layer,
                           const Ellipsoid& ellipsoid,
                           const Eigen::Isometry3f& globalSensorPose);

        void drawChains(const std::vector<Features>& features,
                        const std::string& frame,
                        const Eigen::Isometry3f& globalSensorPose);
        void drawChain(viz::Layer& layer,
                       const Points& chain,
                       const Eigen::Isometry3f& globalSensorPose);

        armarx::viz::Client arviz;
    };
} // namespace armarx::laser_scanner_feature_extraction
