/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::LaserScannerFeatureExtraction
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArmarXCore/core/logging/Logging.h"
#include "RobotComponents/components/LaserScannerFeatureExtraction/FeatureExtractor.h"
#include <boost/test/tools/old/interface.hpp>
#define BOOST_TEST_MODULE RobotComponents::ArmarXObjects::LaserScannerFeatureExtraction

#define ARMARX_BOOST_TEST

#include <RobotComponents/Test.h>

#include <pcl/point_types.h>

#include "../EnclosingEllipsoid.h"

#include <iostream>

BOOST_AUTO_TEST_CASE(testExample)
{
    // Ground truth (GT) ellipsoid with params:
    //  center = (0,0)
    //  radii = (4,2)
    //  angle = 0
    armarx::laser_scanner_feature_extraction::FeatureExtractor::Points points;
    points.push_back(Eigen::Vector2f{-4.F, 0});
    points.push_back(Eigen::Vector2f{0, 2.F});
    points.push_back(Eigen::Vector2f{4.F, 0});
    points.push_back(Eigen::Vector2f{0, -2.F});

    const Eigen::Vector2f radii(4, 2);

    armarx::laser_scanner_feature_extraction::EnclosingEllipsoid ee(points);

    // The computed ellipsoid must contain the GT ellipsoid
    BOOST_CHECK_GE(ee.radii.x(), 4.F);
    BOOST_CHECK_GE(ee.radii.y(), 2.F);

    // ... but should not be too large
    BOOST_CHECK_LE(ee.radii.x(), 4.1F);
    BOOST_CHECK_LE(ee.radii.y(), 2.1F);

    // The computed ellipsoid must be roughly at the position of the GT ellipsoid
    BOOST_CHECK_LT(std::fabs(ee.pose.translation().x()), 0.1);
    BOOST_CHECK_LT(std::fabs(ee.pose.translation().y()), 0.1);
}
