#pragma once

#include <vector>

#include <Eigen/Core>

namespace armarx::laser_scanner_feature_extraction
{

    struct Path
    {
        std::vector<Eigen::Vector2f> points;

        using Segment = std::pair<Eigen::Vector2f, Eigen::Vector2f>;

        std::vector<Segment> segments() const noexcept;
    };

} // namespace armarx::laser_scanner_feature_extraction
