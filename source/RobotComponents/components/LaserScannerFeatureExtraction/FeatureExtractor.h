/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>

#include <pcl/point_types.h>

#include <VirtualRobot/MathTools.h>
#include "RobotAPI/libraries/armem_vision/types.h"

#include "RobotComponents/components/LaserScannerFeatureExtraction/ChainApproximation.h"
#include "RobotComponents/libraries/cartographer/types.h"
#include <RobotComponents/interface/components/GraspingManager/RobotPlacementInterface.h>

#include "EnclosingEllipsoid.h"
#include "ScanClustering.h"

namespace armarx::laser_scanner_feature_extraction
{
    using Circle = armem::vision::Circle;
   

    struct Features
    {
        using Points   = std::vector<Eigen::Vector2f>;
        using Chain = Points;

        std::optional<VirtualRobot::MathTools::ConvexHull2D> convexHull;

        std::optional<Circle> circle;
        std::optional<Ellipsoid> ellipsoid;

        std::optional<Chain> chain;

        Points points;
        std::vector<Ellipsoid> linesAsEllipsoids(float axisLength) const;
    };

    class FeatureExtractor
    {
    public:
        using Points   = std::vector<Eigen::Vector2f>;
        using Callback = std::function<void(const cartographer::LaserScannerMessage& data,
                                            const std::vector<Features>& features)>;

        FeatureExtractor(const ScanClustering::Params& scanClusteringParams,
                         const ChainApproximation::Params& chainApproximationParams,
                         const Callback& callback);

        void onData(const cartographer::LaserScannerMessage& data);

    private:
        std::vector<Features> features(const LaserScan& scan) const;

        std::vector<LaserScan> detectClusters(const LaserScan& scan) const;

        std::optional<VirtualRobot::MathTools::ConvexHull2D> convexHull(const Points& points) const;
        std::optional<Ellipsoid>
        ellipsoid(const std::optional<VirtualRobot::MathTools::ConvexHull2D>& hull) const;
        std::optional<Circle> circle(const Points& circle) const;

        std::optional<Points> chainApproximation(
            const Points& points,
            const std::optional<VirtualRobot::MathTools::ConvexHull2D>& convexHull) const;

        const ScanClustering::Params scanClusteringParams;
        const ChainApproximation::Params chainApproximationParams;

        const Callback callback;
    };
} // namespace armarx::laser_scanner_feature_extraction
