/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::LaserScannerFeatureExtraction
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LaserScannerFeatureExtraction.h"

#include <algorithm>
#include <cmath>
#include <iterator>
#include <utility>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <SimoxUtility/algorithm/apply.hpp>
#include <SimoxUtility/color/Color.h>
#include <VirtualRobot/BoundingBox.h>
#include <VirtualRobot/MathTools.h>

#include "ArmarXCore/core/time/DateTime.h"
#include "ArmarXCore/core/time/Duration.h"

#include "RobotAPI/libraries/armem_vision/types.h"
#include "RobotAPI/libraries/core/remoterobot/RemoteRobot.h"

#include "RobotComponents/components/LaserScannerFeatureExtraction/geometry.h"

#include "ArVizDrawer.h"
#include "FeatureExtractor.h"
#include "conversions/eigen.h"
#include "conversions/pcl_eigen.h"

namespace armarx::laser_scanner_feature_extraction
{

    armarx::PropertyDefinitionsPtr
    LaserScannerFeatureExtraction::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        def->topic(featuresTopic);

        // Subscribe to a topic (passing the topic name).
        def->topic<LaserScannerUnitListener>("LaserScans");

        // // Add an optionalproperty.
        def->optional(properties.queueSize,
                      "p.inputQueueSize",
                      "Size of the message queue (max). Should not be too large to avoid delays.");
        def->optional(properties.robotConvexHullMargin,
                      "p.robotConvexHullMargin",
                      "Parameter to increase the robot's convex hull.");

        def->optional(properties.cableFix.enabled,
                      "p.cableFix.enabled",
                      "Try to supress clusters belonging to the power supply cable.");
        def->optional(properties.cableFix.cableAreaWidth,
                      "p.cableFix.cableAreaWidth",
                      "Width of the area where to search for the cable.");

        def->optional(
            properties.cableFix.maxAreaTh,
            "p.cableFix.maxAreaTh",
            "The cable will only be removed if the cluster area is below this threshold.");

        def->optional(
            properties.chainApproximationParams.distanceTh, "p.chainApproximation.distanceTh", "");
        def->optional(properties.chainApproximationParams.maxIterations,
                      "p.chainApproximation.maxIterations",
                      "");

        def->optional(properties.scanClusteringParams.angleThreshold,
                      "p.scanClustering.angleThreshold",
                      "Angular distance between consecutive points in laser scan for clustering.");
        def->optional(properties.scanClusteringParams.distanceThreshold,
                      "p.scanClustering.distanceThreshold",
                      "Radial distance between consecutive points in laser scan for clustering.");
        def->optional(properties.scanClusteringParams.maxDistance,
                      "p.scanClustering.maxDistance",
                      "Maximum radius around sensors to detect clusters.");

        def->optional(properties.arviz.drawRawPoints,
                      "p.arviz.drawRawPoints",
                      "If true, the laser scans will be drawn.");

        laserScannerFeatureWriter.registerPropertyDefinitions(def);

        return def;
    }

    LaserScannerFeatureExtraction::LaserScannerFeatureExtraction() :
        laserScannerFeatureWriter(memoryNameSystem())
    {
    }

    void
    LaserScannerFeatureExtraction::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }

    void
    LaserScannerFeatureExtraction::onConnectComponent()
    {
        ARMARX_INFO << "Connecting";

        frequencyReporterPublish = std::make_unique<FrequencyReporter>(
            getDebugObserver(), "LaserScannerFeatureExtraction_publish");
        frequencyReporterSubscribe = std::make_unique<FrequencyReporter>(
            getDebugObserver(), "LaserScannerFeatureExtraction_subscribe");

        arVizDrawer = std::make_unique<ArVizDrawer>(getArvizClient());

        featureExtractor = std::make_unique<FeatureExtractor>(
            properties.scanClusteringParams,
            properties.chainApproximationParams,
            [&](auto&&... args) { onFeatures(std::forward<decltype(args)>(args)...); });

        laserMessageQueue.setQueueSize(properties.queueSize);
        laserMessageQueue.connect([&](const auto& sharedArg)
                                  { featureExtractor->onData(sharedArg); });

        ARMARX_INFO << "Connected";

        robot = RemoteRobot::createLocalClone(getRobotStateComponent());
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        // initialize robot platform convex hull
        const Eigen::Vector2f ptFrontLeft = conversions::to2D(
            robot->getRobotNode("PlatformCornerFrontLeft")->getPositionInRootFrame());
        const Eigen::Vector2f ptFrontRight = conversions::to2D(
            robot->getRobotNode("PlatformCornerFrontRight")->getPositionInRootFrame());
        const Eigen::Vector2f ptBackRight = conversions::to2D(
            robot->getRobotNode("PlatformCornerBackRight")->getPositionInRootFrame());
        const Eigen::Vector2f ptBackLeft = conversions::to2D(
            robot->getRobotNode("PlatformCornerBackLeft")->getPositionInRootFrame());

        const auto addMargin = [&](const Eigen::Vector2f& pt) -> Eigen::Vector2f
        {
            Eigen::Vector2f ptWithMargin = pt;
            ptWithMargin.x() += std::copysignf(properties.robotConvexHullMargin, pt.x());
            ptWithMargin.y() += std::copysignf(properties.robotConvexHullMargin, pt.y());

            return ptWithMargin;
        };

        {
            std::vector<Eigen::Vector2f> hullPoints;
            hullPoints.push_back(addMargin(ptFrontLeft));
            hullPoints.push_back(addMargin(ptFrontRight));
            hullPoints.push_back(addMargin(ptBackRight));
            hullPoints.push_back(addMargin(ptBackLeft));

            robotHull = VirtualRobot::MathTools::createConvexHull2D(hullPoints);
        }

        if (properties.cableFix.enabled)
        {
            // the cable area and the robot hull overlap slightly. as all points must be within one region,
            // the cable area should start at approx. the position of the wheels

            std::vector<Eigen::Vector2f> cableAreaPoints;
            cableAreaPoints.emplace_back(addMargin(ptBackRight) + 100 * Eigen::Vector2f::UnitY());
            cableAreaPoints.emplace_back(addMargin(ptBackLeft) + 100 * Eigen::Vector2f::UnitY());
            cableAreaPoints.emplace_back(addMargin(ptBackRight) -
                                         properties.cableFix.cableAreaWidth *
                                             Eigen::Vector2f::UnitY());
            cableAreaPoints.emplace_back(addMargin(ptBackLeft) -
                                         properties.cableFix.cableAreaWidth *
                                             Eigen::Vector2f::UnitY());

            cableArea = VirtualRobot::MathTools::createConvexHull2D(cableAreaPoints);
        }

        laserScannerFeatureWriter.connect();
    }

    armem::vision::LaserScannerFeature
    toArmemFeature(const Features& features)
    {
        armem::vision::LaserScannerFeature armemFeature;

        if (features.chain)
        {
            armemFeature.chain = features.chain.value();
        }

        if (features.circle)
        {
            armemFeature.circle = features.circle.value();
        }

        if (features.convexHull)
        {
            armemFeature.convexHull = features.convexHull->vertices;
        }

        if (features.ellipsoid)
        {
            armemFeature.ellipsoid = features.ellipsoid.value();
        }

        armemFeature.points = features.points;

        return armemFeature;
    }

    armem::vision::LaserScannerFeatures
    toArmemFeatures(const std::vector<Features>& features,
                    const Eigen::Isometry3f& global_T_sensor,
                    const std::string& sensorFrame)
    {
        armem::vision::LaserScannerFeatures armemFeatures;
        armemFeatures.frame = sensorFrame;
        armemFeatures.frameGlobalPose = global_T_sensor;


        armemFeatures.features = simox::alg::apply(features, toArmemFeature);

        return armemFeatures;
    }

    std::vector<Features>
    removeInvalidFeatures(std::vector<Features> features)
    {
        const auto isInvalid = [](const Features& features) -> bool
        {
            if (features.points.size() < 2)
            {
                return true;
            }

            if (not features.convexHull.has_value())
            {
                return true;
            }

            // if(not features.circle.has_value())
            // {
            //     return true;
            // }

            // if(not features.ellipsoid.has_value())
            // {
            //     return true;
            // }

            return false;
        };


        features.erase(std::remove_if(features.begin(), features.end(), isInvalid), features.end());

        return features;
    }

    void
    LaserScannerFeatureExtraction::onFeatures(const cartographer::LaserScannerMessage& data,
                                              const std::vector<Features>& featuresFromExtractor)
    {
        ARMARX_DEBUG << "Publishing data";

        // obtain sensor pose
        RemoteRobot::synchronizeLocalCloneToTimestamp(
            robot, getRobotStateComponent(), data.timestamp);
        const Eigen::Isometry3f global_T_sensor(robot->getRobotNode(data.frame)->getGlobalPose());
        const Eigen::Isometry3f global_T_robot(robot->getRootNode()->getGlobalPose());
        const Eigen::Isometry3f robot_T_sensor(
            robot->getRobotNode(data.frame)->getPoseInRootFrame());

        //Eigen::AlignedBox2f box;
        //box.extend(pt1).extend(pt2).extend(pt3).extend(pt4);

        const auto transformPoints =
            [](const std::vector<Eigen::Vector2f>& points, const Eigen::Isometry3f& tf)
        {
            std::vector<Eigen::Vector2f> out;
            out.reserve(points.size());

            std::transform(points.begin(),
                           points.end(),
                           std::back_inserter(out),
                           [&tf](const auto& pt)
                           { return conversions::to2D(tf * conversions::to3D(pt)); });

            return out;
        };

        const auto isPointInsideRobot = [&](const Eigen::Vector2f& pt) -> bool
        { return VirtualRobot::MathTools::isInside(pt, robotHull); };

        const auto isPointInsideCableArea = [&](const Eigen::Vector2f& pt) -> bool
        { return VirtualRobot::MathTools::isInside(pt, cableArea); };

        const auto isClusterInvalid = [&](const Features& features) -> bool
        {
            // either use convex hull (compact representation) or raw points as fallbacck
            const auto points = [&]()
            {
                if (features.convexHull)
                {
                    return transformPoints(features.convexHull->vertices, robot_T_sensor);
                }

                return transformPoints(features.points, robot_T_sensor);
            }();

            const bool allPointsInsideRobot =
                std::all_of(points.begin(), points.end(), isPointInsideRobot);
            if (allPointsInsideRobot)
            {
                return true;
            }

            if (properties.cableFix.enabled)
            {

                const bool allPointsInCableArea =
                    std::all_of(points.begin(), points.end(), isPointInsideCableArea);
                if (allPointsInCableArea)
                {
                    if (geometry::ConvexHull(points).area() < properties.cableFix.maxAreaTh)
                    {
                        return true;
                    }
                }
            }

            return false;
        };

        const auto removeFeaturesOnRobotOrCable = [&](std::vector<Features> features)
        {
            features.erase(std::remove_if(features.begin(), features.end(), isClusterInvalid),
                           features.end());
            return features;
        };

        ARMARX_VERBOSE << featuresFromExtractor.size() << " features from extractor";

        const auto features = removeFeaturesOnRobotOrCable(featuresFromExtractor);
        ARMARX_VERBOSE << features.size() << " features without cable region";

        const auto validFeatures = removeInvalidFeatures(features);
        ARMARX_VERBOSE << validFeatures.size() << " valid features without cable region";

        const auto armemFeatures = toArmemFeatures(validFeatures, global_T_sensor, data.frame);

        ARMARX_VERBOSE << "Reporting " << armemFeatures.features.size() << " features";

        // report the features
        publishFeatures(armemFeatures,
                        armarx::core::time::DateTime(
                            armarx::core::time::Duration::MicroSeconds(data.timestamp)));

        // check if arviz should be triggered
        const auto getOrCreateThrottler = [&]() -> Throttler&
        {
            const auto it = throttlers.find(data.frame);
            if (it == throttlers.end())
            {
                throttlers.emplace(data.frame, Throttler(10.F));
            }

            return throttlers.at(data.frame);
        };

        if (getOrCreateThrottler().check(data.timestamp))
        {
            arVizDrawer->draw(features, data.frame, global_T_sensor);

            if (robotHull != nullptr)
            {
                arVizDrawer->draw(
                    "robot_convex_hull", *robotHull, global_T_robot, simox::Color::azure(255, 80));
            }

            if (cableArea != nullptr)
            {
                arVizDrawer->draw(
                    "cable_area", *cableArea, global_T_robot, simox::Color::blue(255, 80));
            }

            if (properties.arviz.drawRawPoints)
            {
                arVizDrawer->draw(data, global_T_sensor, simox::Color::magenta(255, 100));
            }
        }

        // some debugobserver reporting
        frequencyReporterPublish->add(IceUtil::Time::now().toMicroSeconds());
        setDebugObserverDatafield("numClusters", features.size());
    }

    void
    LaserScannerFeatureExtraction::publishFeatures(
        const armem::vision::LaserScannerFeatures& features,
        const armem::Time& timestamp)
    {

        // store in memory
        laserScannerFeatureWriter.store(features, getName(), timestamp);

        // legacy - topic
        LineSegment2DChainSeq chains;
        for (const auto& feature : features.features)
        {
            if (not feature.chain.empty())
            {
                LineSegment2DChain chain;
                for (const auto& pt : feature.chain)
                {
                    chain.push_back(
                        conversions::to2D(features.frameGlobalPose * conversions::to3D(pt)));
                }
                chains.push_back(chain);
            }
        }

        featuresTopic->reportExtractedLineSegments(chains);
    }

    void
    LaserScannerFeatureExtraction::onDisconnectComponent()
    {
    }

    void
    LaserScannerFeatureExtraction::onExitComponent()
    {
    }

    std::string
    LaserScannerFeatureExtraction::getDefaultName() const
    {
        return "LaserScannerFeatureExtraction";
    }

    void
    LaserScannerFeatureExtraction::reportSensorValues(const std::string& device,
                                                      const std::string& name,
                                                      const LaserScan& scan,
                                                      const TimestampBasePtr& timestamp,
                                                      const Ice::Current& /*unused*/)
    {
        ARMARX_DEBUG << "Receiving data";

        laserMessageQueue.push(cartographer::LaserScannerMessage{
            .frame = name, .scan = scan, .timestamp = timestamp->timestamp});

        ARMARX_DEBUG << "Queue size" << laserMessageQueue.size();

        frequencyReporterSubscribe->add(IceUtil::Time::now().toMicroSeconds());

        setDebugObserverDatafield(name, scan.size());
    }

} // namespace armarx::laser_scanner_feature_extraction
