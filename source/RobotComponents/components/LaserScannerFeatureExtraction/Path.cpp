#include "Path.h"


namespace armarx::laser_scanner_feature_extraction
{

    std::vector<Path::Segment> Path::segments() const noexcept
    {
        if (points.size() <= 1)
        {
            return {};
        }

        std::vector<Segment> segments;
        segments.reserve(static_cast<int>(points.size()) - 1);

        for (int i = 0; i < static_cast<int>(points.size()) - 1; i++)
        {
            segments.emplace_back(points.at(i), points.at(i + 1));
        }

        return segments;
    }
} // namespace armarx::laser_scanner_feature_extraction
