/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <opencv2/core/types.hpp>

namespace armarx::conversions
{

    inline cv::Point2f pcl2cv(const pcl::PointXY& pt)
    {
        return cv::Point2f{pt.x, pt.y};
    }
    inline cv::Point3f pcl2cv(const pcl::PointXYZ& pt)
    {
        return cv::Point3f{pt.x, pt.y, pt.z};
    }

    template <typename PointT, typename CvT = decltype(pcl2cv(PointT()))>
    auto pcl2cv(const pcl::PointCloud<PointT>& pointCloud)
    {
        std::vector<CvT> v;
        v.reserve(pointCloud.points.size());

        std::transform(pointCloud.points.begin(),
                       pointCloud.points.end(),
                       std::back_inserter(v),
                       static_cast<CvT (*)(const PointT&)>(pcl2cv));

        return v;
    }

} // namespace armarx::conversions