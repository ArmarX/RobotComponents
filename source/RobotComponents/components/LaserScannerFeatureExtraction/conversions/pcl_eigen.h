/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <algorithm>

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace armarx::conversions
{
    // pcl2eigen

    inline Eigen::Vector2f pcl2eigen(const pcl::PointXY& pt)
    {
        return Eigen::Vector2f{pt.x, pt.y};
    }
    inline Eigen::Vector3f pcl2eigen(const pcl::PointXYZ& pt)
    {
        return Eigen::Vector3f{pt.x, pt.y, pt.z};
    }

    template <typename PointT,
              typename EigenVector = decltype(pcl2eigen(PointT()))>
    auto pcl2eigen(const pcl::PointCloud<PointT>& pointCloud)
    -> std::vector<EigenVector>
    {
        std::vector<EigenVector> v;
        v.reserve(pointCloud.points.size());

        std::transform(pointCloud.points.begin(),
                       pointCloud.points.end(),
                       std::back_inserter(v),
                       static_cast<EigenVector(*)(const PointT&)>(pcl2eigen));

        return v;
    }

    // eigen2pcl

    inline pcl::PointXY eigen2pcl(const Eigen::Vector2f& pt)
    {
        return pcl::PointXY{pt.x(), pt.y()};
    }

    inline pcl::PointXYZ eigen2pcl(const Eigen::Vector3f& pt)
    {
        return pcl::PointXYZ{pt.x(), pt.y(), 0.F};
    }

    template <typename EigenVector,
              typename PointT = decltype(eigen2pcl(EigenVector()))>
    auto eigen2pcl(const std::vector<EigenVector>& points)
    -> pcl::PointCloud<PointT>
    {
        pcl::PointCloud<PointT> pointCloud;
        pointCloud.points.reserve(points.size());

        std::transform(points.begin(),
                       points.end(),
                       std::back_inserter(pointCloud.points),
                       static_cast<PointT(*)(const EigenVector&)>(eigen2pcl));

        pointCloud.width    = pointCloud.points.size();
        pointCloud.height   = 1;
        pointCloud.is_dense = true;

        return pointCloud;
    }

} // namespace armarx::conversions