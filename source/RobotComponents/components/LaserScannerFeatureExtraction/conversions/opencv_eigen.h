/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <opencv2/core/types.hpp>

namespace armarx::conversions
{
    inline cv::Point2f eigen2cv(const Eigen::Vector2f& pt)
    {
        return {pt.x(), pt.y()};
    }

    template <typename EigenT, typename CvT = decltype(eigen2cv(EigenT()))>
    auto eigen2cv(const std::vector<EigenT>& points)
    {
        std::vector<CvT> v;
        v.reserve(points.size());

        std::transform(
            points.begin(), points.end(), std::back_inserter(v), static_cast<CvT(*)(const EigenT&)>(eigen2cv));

        return v;
    }

    inline Eigen::Vector2f cv2eigen(const cv::Point2f& pt)
    {
        return Eigen::Vector2f{pt.x, pt.y};
    }

} // namespace armarx::conversions