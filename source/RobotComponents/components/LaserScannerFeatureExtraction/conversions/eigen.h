/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace armarx::conversions
{

    inline Eigen::Vector2f to2D(const Eigen::Vector3f& v2)
    {
        return Eigen::Vector2f{v2.x(), v2.y()};
    }

    inline Eigen::Vector3f to3D(const Eigen::Vector2f& v2)
    {
        return Eigen::Vector3f{v2.x(), v2.y(), 0.F};
    }

    inline Eigen::Isometry3f to3D(const Eigen::Affine2f& p2)
    {
        Eigen::Isometry3f pose            = Eigen::Isometry3f::Identity();
        pose.linear().block<2, 2>(0, 0) = p2.linear();
        pose.translation().head<2>()    = p2.translation();

        return pose;
    }

    std::vector<Eigen::Vector3f> to3D(const std::vector<Eigen::Vector2f>& v);

} // namespace armarx::conversions
