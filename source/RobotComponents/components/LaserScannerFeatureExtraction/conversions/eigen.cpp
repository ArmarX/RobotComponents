#include "eigen.h"

#include <algorithm>

namespace armarx::conversions
{

    std::vector<Eigen::Vector3f> to3D(const std::vector<Eigen::Vector2f>& v)

    {
        std::vector<Eigen::Vector3f> v3;
        v3.reserve(v.size());

        std::transform(
            v.begin(),
            v.end(),
            std::back_inserter(v3),
            static_cast<Eigen::Vector3f (*)(const Eigen::Vector2f&)>(&to3D));

        return v3;
    }

} // namespace armarx::conversions