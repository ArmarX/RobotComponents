#include "pcl.h"

#include <algorithm>

namespace armarx::conversions
{
    pcl::PointCloud<pcl::PointXYZ> to3D(const pcl::PointCloud<pcl::PointXY>& v)
    {
        pcl::PointCloud<pcl::PointXYZ> pc;
        pc.points.reserve(v.size());

        std::transform(
            v.begin(),
            v.end(),
            std::back_inserter(pc),
            static_cast<pcl::PointXYZ (*)(const pcl::PointXY&)>(&to3D));

        pc.width    = v.height;
        pc.height   = v.width;
        pc.is_dense = v.is_dense;

        pc.header = v.header;

        return pc;
    }

} // namespace armarx::conversions