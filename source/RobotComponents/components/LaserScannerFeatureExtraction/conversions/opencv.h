/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <opencv2/core/types.hpp>

namespace armarx::conversions
{

    // TODO(fabian.reister): this is a specialized method
    template <typename CvT> CvT cast(const auto &pt) { return CvT(pt.x, pt.y); }

    template <typename PointOutT, typename PointInT>
    std::vector<PointOutT> cast(const std::vector<PointInT> &points)
    {
        std::vector<PointOutT> v;
        v.reserve(points.size());

        std::transform(points.begin(),
                       points.end(),
                       std::back_inserter(v),
                       static_cast<PointOutT (*)(const PointInT &)>(cast));

        return v;
    }
} // namespace armarx::conversions