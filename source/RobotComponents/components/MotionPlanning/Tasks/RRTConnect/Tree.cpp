/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include <algorithm>
#include <limits>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../../util/Metrics.h"

#include "Tree.h"

const armarx::rrtconnect::NodeId armarx::rrtconnect::Tree::ROOT_NODE_ID
{
    0, 0
};

void armarx::rrtconnect::Tree::setRoot(const armarx::rrtconnect::Tree::ConfigType& root)
{
    ARMARX_CHECK_EXPRESSION(!nodes.empty());
    if (nodes.at(0).empty())
    {
        addNode(root, ROOT_NODE_ID, 0);
    }
    else
    {
        nodes.at(0).at(0).cfg = root;
    }
}

std::vector<armarx::rrtconnect::Tree::ConfigType> armarx::rrtconnect::Tree::getReversedPathTo(armarx::rrtconnect::NodeId nodeId) const
{
    std::vector<ConfigType> path;
    while (nodeId != ROOT_NODE_ID)
    {
        const auto& node = getNode(nodeId);
        path.emplace_back(node.cfg);
        nodeId = node.parent;
    }
    path.emplace_back(getNode(ROOT_NODE_ID).cfg);
    return path;
}

std::vector<armarx::rrtconnect::Tree::ConfigType> armarx::rrtconnect::Tree::getPathTo(const armarx::rrtconnect::NodeId& nodeId) const
{
    std::vector<ConfigType> path = getReversedPathTo(nodeId);
    std::reverse(path.begin(), path.end());
    return path;
}

armarx::rrtconnect::NodeId armarx::rrtconnect::Tree::getNearestNeighbour(const armarx::rrtconnect::Tree::ConfigType& cfg)
{
    float minDistance = std::numeric_limits<float>::infinity();
    NodeId nearesNode;
    for (std::size_t workerId = 0; workerId < nodes.size(); ++workerId)
    {
        const auto& workerNodes = nodes.at(workerId);
        for (std::size_t nodeSubId = 0; nodeSubId < workerNodes.size(); ++nodeSubId)
        {
            NodeId currentNodeId {static_cast<Ice::Long>(workerId), static_cast<Ice::Long>(nodeSubId)};
            const auto& node = getNode(currentNodeId);
            float currentDistance = euclideanDistance(cfg.data(), cfg.data() + cfg.size(), node.cfg.data());
            if (currentDistance < minDistance)
            {
                minDistance = currentDistance;
                nearesNode = currentNodeId;
            }
        }
    }
    return nearesNode;
}
