/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#include "Updater.h"

#include <ArmarXCore/core/logging/Logging.h>

void armarx::rrtconnect::Updater::setWorkerId(Ice::Long newId)
{
    ARMARX_CHECK_EXPRESSION(newId >= 0);
    workerId = newId;
    setWorkerCount(workerId + 1);
}

void armarx::rrtconnect::Updater::addPendingUpdate(const armarx::rrtconnect::Update& u)
{
    pendingUpdateLookupTable[UpdateId {u}] = pendingUpdates.size();
    pendingUpdates.emplace_back(u);
}

void armarx::rrtconnect::Updater::clearPendingUpdates()
{
    pendingUpdateLookupTable.clear();
    pendingUpdates.clear();
}

void armarx::rrtconnect::Updater::applyUpdate(const armarx::rrtconnect::Update& u)
{
    ARMARX_CHECK_EXPRESSION(!hasAppliedUpdate(u));
    ARMARX_CHECK_EXPRESSION(canApplyUpdate(u));
    ARMARX_CHECK_EXPRESSION(u.updatesPerTree.size() <= trees.size());

    for (std::size_t index = 0; index < u.updatesPerTree.size(); ++index)
    {
        trees.at(index).get().applyUpdate(u.updatesPerTree.at(index), u.workerId);
    }
    ARMARX_CHECK_EXPRESSION(static_cast<std::size_t>(u.workerId) < appliedUpdateIds.size());
    ++appliedUpdateIds.at(u.workerId);
}

bool armarx::rrtconnect::Updater::canApplyUpdate(const armarx::rrtconnect::Update& u)
{
    for (Ice::Long workerId = 0; static_cast<std::size_t>(workerId) < u.dependetOnUpdateIds.size(); ++workerId)
    {
        if (!hasAppliedUpdate(workerId, u.dependetOnUpdateIds.at(workerId)))
        {
            const UpdateId currUpdateId
            {
                u
            };
            ARMARX_ERROR_S << "[worker " << workerId << "] missing update " << workerId << "/" << u.dependetOnUpdateIds.at(workerId) << " for update " << currUpdateId.workerId << "/" << currUpdateId.updateSubId;
            return false;
        }
    }
    return true;
}
