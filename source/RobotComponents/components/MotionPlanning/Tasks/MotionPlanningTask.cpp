/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "MotionPlanningTask.h"
#include "../util/PlanningUtil.h"

namespace armarx
{
    bool MotionPlanningTask::setTaskStatus(TaskStatus::Status newTaskStatus, const Ice::Current&)
    {
        //loop for compare and swap
        //since states form a DAG and staying on the same state returns from this function,
        //this loop will terminate
        while (true)
        {
            auto  currentTaskStatus = taskStatus.load();

            bool invalidTransition = false;
            switch (newTaskStatus)
            {
                case TaskStatus::eNew:
                case TaskStatus::eTaskStatusSize:
                    invalidTransition = true;
                    [[fallthrough]];

                case TaskStatus::eQueued:
                    if (currentTaskStatus != TaskStatus::eNew)
                    {
                        invalidTransition = true;
                    }
                    break;
                case TaskStatus::ePlanning:
                    if (currentTaskStatus != TaskStatus::eQueued)
                    {
                        invalidTransition = true;
                    }
                    break;
                case TaskStatus::eRefining:
                    if (currentTaskStatus != TaskStatus::ePlanning)
                    {
                        invalidTransition = true;
                    }
                    break;
                case TaskStatus::ePlanningAborted:
                    if (
                        (currentTaskStatus != TaskStatus::eNew) &&
                        (currentTaskStatus != TaskStatus::eQueued) &&
                        (currentTaskStatus != TaskStatus::ePlanning)
                    )
                    {
                        invalidTransition = true;
                    }
                    break;
                case TaskStatus::ePlanningFailed:
                    if (currentTaskStatus != TaskStatus::ePlanning)
                    {
                        invalidTransition = true;
                    }
                    break;
                case TaskStatus::eRefinementAborted:
                    if (currentTaskStatus != TaskStatus::eRefining)
                    {
                        invalidTransition = true;
                    }
                    break;
                case TaskStatus::eDone:
                    if (
                        (currentTaskStatus != TaskStatus::eRefining) &&
                        (currentTaskStatus != TaskStatus::ePlanning)
                    )
                    {
                        invalidTransition = true;
                    }
                    break;
                case TaskStatus::eException:
                    break;
                default:
                    invalidTransition = true;
            }
            if (invalidTransition)
            {
                if (currentTaskStatus != newTaskStatus)
                {
                    ARMARX_WARNING_S << "Tried invalid transition from "
                                     << TaskStatus::toString(currentTaskStatus)
                                     << " to "
                                     << TaskStatus::toString(newTaskStatus);
                }
                return false;
            }

            if (taskStatus.compare_exchange_strong(currentTaskStatus, newTaskStatus))
            {
                if (TaskStatus::finishedRunning(newTaskStatus))
                {
                    waitForFinishedPlanning.setAutoResponse();
                    waitForFinishedRunning.setAutoResponse();
                }
                else
                {
                    if (newTaskStatus == TaskStatus::eRefining)
                    {
                        waitForFinishedPlanning.setAutoResponse();
                    }
                }
                for (auto& cb : taskStatusCallbacks)
                {
                    cb(newTaskStatus);
                }
                return true;
            }
        }
    }
}
