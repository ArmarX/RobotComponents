/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#include <stdexcept>

#include "PlanningUtil.h"

namespace armarx::TaskStatus
{
    std::string toString(Status status)
    {
        switch (status)
        {
            case eNew:
                return "eNew";

            case eQueued:
                return "eQueued";

            case ePlanning:
                return "ePlanning";

            case ePlanningAborted:
                return "ePlanningAborted";

            case ePlanningFailed:
                return "ePlanningFailed";

            case eRefining:
                return "eRefining";

            case eRefinementAborted:
                return "eRefinementAborted";

            case eDone:
                return "eDone";

            case eException:
                return "eException";

            default:
                return "Unknown status: " + std::to_string(status);
        }
    }

    //status information
    bool isRunning(Status status)
    {
        switch (status)
        {
            case ePlanning:
            case eRefining:
                return true;

            case eNew:
            case eQueued:
            case ePlanningAborted:
            case ePlanningFailed:
            case eRefinementAborted:
            case eDone:
            case eException:
                return false;

            case eTaskStatusSize:
                break;
                //do not add a default case. a new status is either a planning step or it is not (but both is possible for a new status).
        }

        //this point should never be reached!
        //if it is a new status was added without changing this function
        throw std::logic_error {__FILE__ " in line " +  std::to_string(__LINE__) + toString(status)};
    }

    bool finishedRunning(Status status)
    {
        switch (status)
        {
            case eNew:
            case eQueued:
            case ePlanning:
            case eRefining:
                return false;

            case ePlanningAborted:
            case ePlanningFailed:
            case eRefinementAborted:
            case eDone:
            case eException:
                return true;

            case eTaskStatusSize:
                break;
                //do not add a default case. a new status is either a finished status or it is not (but both is possible for a new status).
        }

        //this point should never be reached!
        //if it is a new status was added without changing this function
        throw std::logic_error {__FILE__ " in line " +  std::to_string(__LINE__) + toString(status)};
    }

    //status transition
    Status transitionAtKill(Status status)
    {
        switch (status)
        {
            case ePlanning:
                return ePlanningAborted;
                break;

            case eRefining:
                return eRefinementAborted;
                break;

            case ePlanningFailed:
            case eDone:
            case eNew:
            case eQueued:
            case ePlanningAborted:
            case eRefinementAborted:
            case eTaskStatusSize:
            case eException:
                break;
        }

        std::stringstream ss;
        ss << "planningStatusTransitionAtKill: transition from " << toString(status) << " is illegal";
        throw std::invalid_argument {ss.str()};
    }

    Status transitionAtOutoftime(Status status)
    {
        switch (status)
        {
            case ePlanning:
                return ePlanningFailed;
                break;

            case eRefining:
                return eDone;
                break;

            case ePlanningFailed:
            case eDone:
            case eNew:
            case eQueued:
            case ePlanningAborted:
            case eRefinementAborted:
            case eTaskStatusSize:
            case eException:
                break;
        }

        std::stringstream ss;
        ss << "planningStatusTransitionAtOutoftime: transition from " << toString(status) << " is illegal";
        throw std::invalid_argument {ss.str()};
    }

    Status transitionAtDone(Status status)
    {
        switch (status)
        {
            case ePlanning:
                return eDone;
                break;

            case eRefining:
                return eDone;

            case ePlanningFailed:
            case eDone:
            case eNew:
            case eQueued:
            case ePlanningAborted:
            case eRefinementAborted:
            case eTaskStatusSize:
            case eException:
                break;
        }

        std::stringstream ss;
        ss << "planningStatusTransitionAtDone: transition from " << toString(status) << " is illegal";
        throw std::invalid_argument {ss.str()};
    }
}

