/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::StaticAgentReporter
 * @author     Timothee Habra ( timothee dot habra at uclouvain dot be )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StaticAgentReporter.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


using namespace armarx;


void StaticAgentReporter::onInitComponent()
{

    usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
    if (!getProperty<std::string>("WorkingMemoryName").getValue().empty())
    {
        usingProxy(getProperty<std::string>("WorkingMemoryName").getValue());
    }
}


void StaticAgentReporter::onConnectComponent()
{
    robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
    robot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::RobotDescription::eStructure);
    if (!getProperty<std::string>("WorkingMemoryName").getValue().empty())
    {
        memoryx::WorkingMemoryInterfacePrx workingMemory = getProxy<memoryx::WorkingMemoryInterfacePrx>(getProperty<std::string>("WorkingMemoryName").getValue());
        agentInstanceSegment = workingMemory->getAgentInstancesSegment();

        robotAgent = new memoryx::AgentInstance(robotStateComponent->getRobotName());
        robotAgent->setSharedRobot(robotStateComponent->getSynchronizedRobot());
        robotAgent->setName(robotStateComponent->getRobotName());
        robotAgent->setAgentFilePath(robotStateComponent->getRobotFilename());
        robotAgent->setPose(new FramedPose(Eigen::Matrix4f::Identity(), GlobalFrame, ""));

        std::string robotAgentId = agentInstanceSegment->upsertEntityByName(robotAgent->getName(), robotAgent);
        robotAgent->setId(robotAgentId);
    }
}


void StaticAgentReporter::onDisconnectComponent()
{

}


void StaticAgentReporter::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr StaticAgentReporter::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new StaticAgentReporterPropertyDefinitions(
            getConfigIdentifier()));
}

