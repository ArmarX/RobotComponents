/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::SimpleGraspGenerator
 * @author     Valerij Wittenbeck (valerij dot wittenbeck at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotComponents/interface/components/GraspingManager/GraspGeneratorInterface.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/core/GridFileManager.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>

#include <Eigen/Geometry>

namespace armarx
{
    /**
     * @class SimpleGraspGeneratorPropertyDefinitions
     * @brief
     */
    class SimpleGraspGeneratorPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        SimpleGraspGeneratorPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("GraspNameInfix", "Grasp", "All grasp candidates without this infix are filtered out. Case-insensitive.");
            defineOptionalProperty<float>("VisualizationSlowdownFactor", 1.0f, "1.0 is a good value for clear visualization, 0 the visualization should not slow down the process", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<bool>("EnableVisualization", true, "If false no visualization is done.", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("PreposeOffset", 70.0f, "Offset of proposed grasp along negative z axis", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("UpwardsOffset", 50.0f, "Offset of proposed grasp in upwards direction", PropertyDefinitionBase::eModifiable);
            defineRequiredProperty<std::string>("TCPtoGCPMapping", "Format: TCP L:GCP L;TCP R:GCP R", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<std::string>("RobotType", "Armar3", "Type of robot for which the grasps should be generated");

        }
    };

    /**
     * @defgroup Component-SimpleGraspGenerator SimpleGraspGenerator
     * @ingroup RobotComponents-Components
     * @brief The SimpleGraspGenerator component controls the head of the robot with inverse kinematics based on the uncertainty
     * of the current requested object locations.
     * The uncertainty of objects grow based on their motion model and the timed passed since the last localization.
     * It can be activated or deactivated with the Ice interface and given manual target positions to look at.
     */

    /**
     * @ingroup Component-SimpleGraspGenerator
     * @brief The SimpleGraspGenerator class
     */
    class SimpleGraspGenerator :
        virtual public Component,
        virtual public GraspGeneratorInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "SimpleGraspGenerator";
        }

        /**
         * @brief Calculates the framedTCPPose and framedTCPPrepose
         *
         * Creates a SimoxWrapper for the instance of the object and looks through all GraspSets for the objectClass.
         * For all grasps in the graspSet that use the same EEF, the framedTcpPose and framedTcpPrepose is calculated
         * and added to the GeneratedGraspList.
         *
         * @return List of generatedGrasps
         */
        GeneratedGraspList generateGrasps(const std::string& objectInstanceEntityId, const Ice::Current& c = Ice::emptyCurrent) override;
        GeneratedGraspList generateGraspsByObjectName(const std::string& objectName, const Ice::Current& c = Ice::emptyCurrent) override;

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new SimpleGraspGeneratorPropertyDefinitions(getConfigIdentifier()));
        }

    private:

        /**
         * @brief Wrapper for the getter of the property TCPtoGCPMapping
         *
         * Gets the property and iterates through the mapping, creating a dictionary of the mapping.
         *
         * @return StringStringDictionary of the mapping.
         */
        StringStringDictionary getTcpGcpMapping();

        RobotStateComponentInterfacePrx rsc;
        memoryx::WorkingMemoryInterfacePrx wm;
        memoryx::WorkingMemoryEntitySegmentBasePrx objectInstances;
        memoryx::PriorKnowledgeInterfacePrx prior;
        memoryx::PersistentObjectClassSegmentBasePrx objectClasses;
        VirtualRobot::RobotPtr robot;

        memoryx::GridFileManagerPtr fileManager;

        armarx::DebugDrawerInterfacePrx debugDrawerPrx;
    };
}

