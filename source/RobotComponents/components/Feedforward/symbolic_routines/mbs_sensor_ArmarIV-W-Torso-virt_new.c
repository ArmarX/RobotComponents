//
//-------------------------------------------------------------
//
//  ROBOTRAN - Version 6.6 (build : february 22, 2008)
//
//  Copyright
//  Universite catholique de Louvain
//  Departement de Mecanique
//  Unite de Production Mecanique et Machines
//  2, Place du Levant
//  1348 Louvain-la-Neuve
//  http://www.robotran.be//
//
//  ==> Generation Date : Fri Oct 28 11:08:15 2016
//
//  ==> Project name : ArmarIV-W-Torso-virt
//  ==> using XML input file
//
//  ==> Number of joints : 18
//
//  ==> Function : F 6 : Sensors Kinematical Informations (sens)
//  ==> Flops complexity : 1319
//
//  ==> Generation Time :  0.020 seconds
//  ==> Post-Processing :  0.030 seconds
//
//-------------------------------------------------------------
//

#include <math.h>

//#include "mbs_data.h"
//#include "mbs_project_interface.h"
#include "user_hard_param_W_torso.h"
#include "mbs_sensor2.h"

void  mbs_sensor_ArmarIV_W_Torso_virt(MbsSensor* sens,
                                      double* q,
                                      double* qd,
                                      double* qdd,
                                      int isens)
{

#include "mbs_sensor_ArmarIV-W-Torso-virt.h"

    //double *q = s->q;
    //double *qd = s->qd;
    //double *qdd = s->qdd;

    //double **frc = s->frc;
    //double **trq = s->trq;



    // === begin imp_aux ===

    // === end imp_aux ===

    // ===== BEGIN task 0 =====

    // Sensor Kinematics



    // = = Block_0_0_0_0_0_1 = =

    // Trigonometric Variables

    C4 = cos(q[4]);
    S4 = sin(q[4]);
    C5 = cos(q[5]);
    S5 = sin(q[5]);
    C6 = cos(q[6]);
    S6 = sin(q[6]);
    C7 = cos(q[7]);
    S7 = sin(q[7]);
    C8 = cos(q[8]);
    S8 = sin(q[8]);
    C9 = cos(q[9]);
    S9 = sin(q[9]);
    C10 = cos(q[10]);
    S10 = sin(q[10]);
    C11 = cos(q[11]);
    S11 = sin(q[11]);
    C12 = cos(q[12]);
    S12 = sin(q[12]);
    C13 = cos(q[13]);
    S13 = sin(q[13]);
    C14 = cos(q[14]);
    S14 = sin(q[14]);
    C15 = cos(q[15]);
    S15 = sin(q[15]);
    C17 = cos(q[17]);
    S17 = sin(q[17]);
    C18 = cos(q[18]);
    S18 = sin(q[18]);

    // = = Block_0_0_0_1_0_1 = =

    // Trigonometric Variables

    C6p7 = C6 * C7 - S6 * S7;
    S6p7 = C6 * S7 + S6 * C7;

    // ====== END Task 0 ======

    // ===== BEGIN task 1 =====

    switch (isens)
    {

            //
            break;
        case 1:



            // = = Block_1_0_0_1_0_1 = =

            // Sensor Kinematics


            ROcp0_25 = S4 * S5;
            ROcp0_35 = -C4 * S5;
            ROcp0_85 = -S4 * C5;
            ROcp0_95 = C4 * C5;
            ROcp0_26 = ROcp0_25 * C6 + C4 * S6;
            ROcp0_36 = ROcp0_35 * C6 + S4 * S6;
            ROcp0_56 = -(ROcp0_25 * S6 - C4 * C6);
            ROcp0_66 = -(ROcp0_35 * S6 - S4 * C6);
            ROcp0_17 = C6p7 * C5;
            ROcp0_27 = ROcp0_26 * C7 + ROcp0_56 * S7;
            ROcp0_37 = ROcp0_36 * C7 + ROcp0_66 * S7;
            ROcp0_47 = -S6p7 * C5;
            ROcp0_57 = -(ROcp0_26 * S7 - ROcp0_56 * C7);
            ROcp0_67 = -(ROcp0_36 * S7 - ROcp0_66 * C7);
            ROcp0_48 = ROcp0_47 * C8 + S5 * S8;
            ROcp0_58 = ROcp0_57 * C8 + ROcp0_85 * S8;
            ROcp0_68 = ROcp0_67 * C8 + ROcp0_95 * S8;
            ROcp0_78 = -(ROcp0_47 * S8 - S5 * C8);
            ROcp0_88 = -(ROcp0_57 * S8 - ROcp0_85 * C8);
            ROcp0_98 = -(ROcp0_67 * S8 - ROcp0_95 * C8);
            ROcp0_49 = ROcp0_48 * C9 + ROcp0_78 * S9;
            ROcp0_59 = ROcp0_58 * C9 + ROcp0_88 * S9;
            ROcp0_69 = ROcp0_68 * C9 + ROcp0_98 * S9;
            ROcp0_79 = -(ROcp0_48 * S9 - ROcp0_78 * C9);
            ROcp0_89 = -(ROcp0_58 * S9 - ROcp0_88 * C9);
            ROcp0_99 = -(ROcp0_68 * S9 - ROcp0_98 * C9);
            ROcp0_110 = ROcp0_17 * C10 - ROcp0_79 * S10;
            ROcp0_210 = ROcp0_27 * C10 - ROcp0_89 * S10;
            ROcp0_310 = ROcp0_37 * C10 - ROcp0_99 * S10;
            ROcp0_710 = ROcp0_17 * S10 + ROcp0_79 * C10;
            ROcp0_810 = ROcp0_27 * S10 + ROcp0_89 * C10;
            ROcp0_910 = ROcp0_37 * S10 + ROcp0_99 * C10;
            ROcp0_111 = ROcp0_110 * C11 + ROcp0_49 * S11;
            ROcp0_211 = ROcp0_210 * C11 + ROcp0_59 * S11;
            ROcp0_311 = ROcp0_310 * C11 + ROcp0_69 * S11;
            ROcp0_411 = -(ROcp0_110 * S11 - ROcp0_49 * C11);
            ROcp0_511 = -(ROcp0_210 * S11 - ROcp0_59 * C11);
            ROcp0_611 = -(ROcp0_310 * S11 - ROcp0_69 * C11);
            ROcp0_412 = ROcp0_411 * C12 + ROcp0_710 * S12;
            ROcp0_512 = ROcp0_511 * C12 + ROcp0_810 * S12;
            ROcp0_612 = ROcp0_611 * C12 + ROcp0_910 * S12;
            ROcp0_712 = -(ROcp0_411 * S12 - ROcp0_710 * C12);
            ROcp0_812 = -(ROcp0_511 * S12 - ROcp0_810 * C12);
            ROcp0_912 = -(ROcp0_611 * S12 - ROcp0_910 * C12);
            ROcp0_113 = ROcp0_111 * C13 - ROcp0_712 * S13;
            ROcp0_213 = ROcp0_211 * C13 - ROcp0_812 * S13;
            ROcp0_313 = ROcp0_311 * C13 - ROcp0_912 * S13;
            ROcp0_713 = ROcp0_111 * S13 + ROcp0_712 * C13;
            ROcp0_813 = ROcp0_211 * S13 + ROcp0_812 * C13;
            ROcp0_913 = ROcp0_311 * S13 + ROcp0_912 * C13;
            ROcp0_414 = ROcp0_412 * C14 + ROcp0_713 * S14;
            ROcp0_514 = ROcp0_512 * C14 + ROcp0_813 * S14;
            ROcp0_614 = ROcp0_612 * C14 + ROcp0_913 * S14;
            ROcp0_714 = -(ROcp0_412 * S14 - ROcp0_713 * C14);
            ROcp0_814 = -(ROcp0_512 * S14 - ROcp0_813 * C14);
            ROcp0_914 = -(ROcp0_612 * S14 - ROcp0_913 * C14);
            ROcp0_115 = ROcp0_113 * C15 + ROcp0_414 * S15;
            ROcp0_215 = ROcp0_213 * C15 + ROcp0_514 * S15;
            ROcp0_315 = ROcp0_313 * C15 + ROcp0_614 * S15;
            ROcp0_415 = -(ROcp0_113 * S15 - ROcp0_414 * C15);
            ROcp0_515 = -(ROcp0_213 * S15 - ROcp0_514 * C15);
            ROcp0_615 = -(ROcp0_313 * S15 - ROcp0_614 * C15);
            ROcp0_417 = ROcp0_415 * C17 + ROcp0_714 * S17;
            ROcp0_517 = ROcp0_515 * C17 + ROcp0_814 * S17;
            ROcp0_617 = ROcp0_615 * C17 + ROcp0_914 * S17;
            ROcp0_717 = -(ROcp0_415 * S17 - ROcp0_714 * C17);
            ROcp0_817 = -(ROcp0_515 * S17 - ROcp0_814 * C17);
            ROcp0_917 = -(ROcp0_615 * S17 - ROcp0_914 * C17);
            ROcp0_118 = ROcp0_115 * C18 + ROcp0_417 * S18;
            ROcp0_218 = ROcp0_215 * C18 + ROcp0_517 * S18;
            ROcp0_318 = ROcp0_315 * C18 + ROcp0_617 * S18;
            ROcp0_418 = -(ROcp0_115 * S18 - ROcp0_417 * C18);
            ROcp0_518 = -(ROcp0_215 * S18 - ROcp0_517 * C18);
            ROcp0_618 = -(ROcp0_315 * S18 - ROcp0_617 * C18);
            OMcp0_25 = qd[5] * C4;
            OMcp0_35 = qd[5] * S4;
            OMcp0_16 = qd[4] + qd[6] * S5;
            OMcp0_26 = OMcp0_25 + ROcp0_85 * qd[6];
            OMcp0_36 = OMcp0_35 + ROcp0_95 * qd[6];
            OMcp0_17 = OMcp0_16 + qd[7] * S5;
            OMcp0_27 = OMcp0_26 + ROcp0_85 * qd[7];
            OMcp0_37 = OMcp0_36 + ROcp0_95 * qd[7];
            OPcp0_17 = qdd[4] + qdd[6] * S5 + qdd[7] * S5 + qd[5] * qd[6] * C5 + qd[7] * (OMcp0_26 * ROcp0_95 - OMcp0_36 * ROcp0_85);
            OPcp0_27 = ROcp0_85 * (qdd[6] + qdd[7]) + qdd[5] * C4 - qd[4] * qd[5] * S4 + qd[6] * (OMcp0_35 * S5 - ROcp0_95 * qd[4]) - qd[7] * (OMcp0_16 *
                       ROcp0_95 - OMcp0_36 * S5);
            OPcp0_37 = ROcp0_95 * (qdd[6] + qdd[7]) + qdd[5] * S4 + qd[4] * qd[5] * C4 - qd[6] * (OMcp0_25 * S5 - ROcp0_85 * qd[4]) + qd[7] * (OMcp0_16 *
                       ROcp0_85 - OMcp0_26 * S5);
            RLcp0_18 = DPT_3_1 * S5;
            RLcp0_28 = ROcp0_85 * DPT_3_1;
            RLcp0_38 = ROcp0_95 * DPT_3_1;
            OMcp0_18 = OMcp0_17 + ROcp0_17 * qd[8];
            OMcp0_28 = OMcp0_27 + ROcp0_27 * qd[8];
            OMcp0_38 = OMcp0_37 + ROcp0_37 * qd[8];
            ORcp0_18 = OMcp0_27 * RLcp0_38 - OMcp0_37 * RLcp0_28;
            ORcp0_28 = -(OMcp0_17 * RLcp0_38 - OMcp0_37 * RLcp0_18);
            ORcp0_38 = OMcp0_17 * RLcp0_28 - OMcp0_27 * RLcp0_18;
            OPcp0_18 = OPcp0_17 + ROcp0_17 * qdd[8] + qd[8] * (OMcp0_27 * ROcp0_37 - OMcp0_37 * ROcp0_27);
            OPcp0_28 = OPcp0_27 + ROcp0_27 * qdd[8] - qd[8] * (OMcp0_17 * ROcp0_37 - OMcp0_37 * ROcp0_17);
            OPcp0_38 = OPcp0_37 + ROcp0_37 * qdd[8] + qd[8] * (OMcp0_17 * ROcp0_27 - OMcp0_27 * ROcp0_17);
            RLcp0_19 = ROcp0_78 * DPT_3_2;
            RLcp0_29 = ROcp0_88 * DPT_3_2;
            RLcp0_39 = ROcp0_98 * DPT_3_2;
            OMcp0_19 = OMcp0_18 + ROcp0_17 * qd[9];
            OMcp0_29 = OMcp0_28 + ROcp0_27 * qd[9];
            OMcp0_39 = OMcp0_38 + ROcp0_37 * qd[9];
            ORcp0_19 = OMcp0_28 * RLcp0_39 - OMcp0_38 * RLcp0_29;
            ORcp0_29 = -(OMcp0_18 * RLcp0_39 - OMcp0_38 * RLcp0_19);
            ORcp0_39 = OMcp0_18 * RLcp0_29 - OMcp0_28 * RLcp0_19;
            OMcp0_110 = OMcp0_19 + ROcp0_49 * qd[10];
            OMcp0_210 = OMcp0_29 + ROcp0_59 * qd[10];
            OMcp0_310 = OMcp0_39 + ROcp0_69 * qd[10];
            OPcp0_110 = OPcp0_18 + ROcp0_17 * qdd[9] + ROcp0_49 * qdd[10] + qd[10] * (OMcp0_29 * ROcp0_69 - OMcp0_39 * ROcp0_59) + qd[9] * (OMcp0_28 *
                        ROcp0_37 - OMcp0_38 * ROcp0_27);
            OPcp0_210 = OPcp0_28 + ROcp0_27 * qdd[9] + ROcp0_59 * qdd[10] - qd[10] * (OMcp0_19 * ROcp0_69 - OMcp0_39 * ROcp0_49) - qd[9] * (OMcp0_18 *
                        ROcp0_37 - OMcp0_38 * ROcp0_17);
            OPcp0_310 = OPcp0_38 + ROcp0_37 * qdd[9] + ROcp0_69 * qdd[10] + qd[10] * (OMcp0_19 * ROcp0_59 - OMcp0_29 * ROcp0_49) + qd[9] * (OMcp0_18 *
                        ROcp0_27 - OMcp0_28 * ROcp0_17);
            RLcp0_111 = ROcp0_710 * DPT_3_3;
            RLcp0_211 = ROcp0_810 * DPT_3_3;
            RLcp0_311 = ROcp0_910 * DPT_3_3;
            OMcp0_111 = OMcp0_110 + ROcp0_710 * qd[11];
            OMcp0_211 = OMcp0_210 + ROcp0_810 * qd[11];
            OMcp0_311 = OMcp0_310 + ROcp0_910 * qd[11];
            ORcp0_111 = OMcp0_210 * RLcp0_311 - OMcp0_310 * RLcp0_211;
            ORcp0_211 = -(OMcp0_110 * RLcp0_311 - OMcp0_310 * RLcp0_111);
            ORcp0_311 = OMcp0_110 * RLcp0_211 - OMcp0_210 * RLcp0_111;
            OPcp0_111 = OPcp0_110 + ROcp0_710 * qdd[11] + qd[11] * (OMcp0_210 * ROcp0_910 - OMcp0_310 * ROcp0_810);
            OPcp0_211 = OPcp0_210 + ROcp0_810 * qdd[11] - qd[11] * (OMcp0_110 * ROcp0_910 - OMcp0_310 * ROcp0_710);
            OPcp0_311 = OPcp0_310 + ROcp0_910 * qdd[11] + qd[11] * (OMcp0_110 * ROcp0_810 - OMcp0_210 * ROcp0_710);
            RLcp0_112 = ROcp0_710 * DPT_3_4;
            RLcp0_212 = ROcp0_810 * DPT_3_4;
            RLcp0_312 = ROcp0_910 * DPT_3_4;
            OMcp0_112 = OMcp0_111 + ROcp0_111 * qd[12];
            OMcp0_212 = OMcp0_211 + ROcp0_211 * qd[12];
            OMcp0_312 = OMcp0_311 + ROcp0_311 * qd[12];
            ORcp0_112 = OMcp0_211 * RLcp0_312 - OMcp0_311 * RLcp0_212;
            ORcp0_212 = -(OMcp0_111 * RLcp0_312 - OMcp0_311 * RLcp0_112);
            ORcp0_312 = OMcp0_111 * RLcp0_212 - OMcp0_211 * RLcp0_112;
            OPcp0_112 = OPcp0_111 + ROcp0_111 * qdd[12] + qd[12] * (OMcp0_211 * ROcp0_311 - OMcp0_311 * ROcp0_211);
            OPcp0_212 = OPcp0_211 + ROcp0_211 * qdd[12] - qd[12] * (OMcp0_111 * ROcp0_311 - OMcp0_311 * ROcp0_111);
            OPcp0_312 = OPcp0_311 + ROcp0_311 * qdd[12] + qd[12] * (OMcp0_111 * ROcp0_211 - OMcp0_211 * ROcp0_111);
            RLcp0_113 = ROcp0_412 * DPT_2_5;
            RLcp0_213 = ROcp0_512 * DPT_2_5;
            RLcp0_313 = ROcp0_612 * DPT_2_5;
            OMcp0_113 = OMcp0_112 + ROcp0_412 * qd[13];
            OMcp0_213 = OMcp0_212 + ROcp0_512 * qd[13];
            OMcp0_313 = OMcp0_312 + ROcp0_612 * qd[13];
            ORcp0_113 = OMcp0_212 * RLcp0_313 - OMcp0_312 * RLcp0_213;
            ORcp0_213 = -(OMcp0_112 * RLcp0_313 - OMcp0_312 * RLcp0_113);
            ORcp0_313 = OMcp0_112 * RLcp0_213 - OMcp0_212 * RLcp0_113;
            OPcp0_113 = OPcp0_112 + ROcp0_412 * qdd[13] + qd[13] * (OMcp0_212 * ROcp0_612 - OMcp0_312 * ROcp0_512);
            OPcp0_213 = OPcp0_212 + ROcp0_512 * qdd[13] - qd[13] * (OMcp0_112 * ROcp0_612 - OMcp0_312 * ROcp0_412);
            OPcp0_313 = OPcp0_312 + ROcp0_612 * qdd[13] + qd[13] * (OMcp0_112 * ROcp0_512 - OMcp0_212 * ROcp0_412);
            RLcp0_114 = ROcp0_113 * DPT_1_6 + ROcp0_412 * DPT_2_6 + ROcp0_713 * DPT_3_6;
            RLcp0_214 = ROcp0_213 * DPT_1_6 + ROcp0_512 * DPT_2_6 + ROcp0_813 * DPT_3_6;
            RLcp0_314 = ROcp0_313 * DPT_1_6 + ROcp0_612 * DPT_2_6 + ROcp0_913 * DPT_3_6;
            OMcp0_114 = OMcp0_113 + ROcp0_113 * qd[14];
            OMcp0_214 = OMcp0_213 + ROcp0_213 * qd[14];
            OMcp0_314 = OMcp0_313 + ROcp0_313 * qd[14];
            ORcp0_114 = OMcp0_213 * RLcp0_314 - OMcp0_313 * RLcp0_214;
            ORcp0_214 = -(OMcp0_113 * RLcp0_314 - OMcp0_313 * RLcp0_114);
            ORcp0_314 = OMcp0_113 * RLcp0_214 - OMcp0_213 * RLcp0_114;
            OMcp0_115 = OMcp0_114 + ROcp0_714 * qd[15];
            OMcp0_215 = OMcp0_214 + ROcp0_814 * qd[15];
            OMcp0_315 = OMcp0_314 + ROcp0_914 * qd[15];
            OPcp0_115 = OPcp0_113 + ROcp0_113 * qdd[14] + ROcp0_714 * qdd[15] + qd[14] * (OMcp0_213 * ROcp0_313 - OMcp0_313 * ROcp0_213) + qd[15] * (
                            OMcp0_214 * ROcp0_914 - OMcp0_314 * ROcp0_814);
            OPcp0_215 = OPcp0_213 + ROcp0_213 * qdd[14] + ROcp0_814 * qdd[15] - qd[14] * (OMcp0_113 * ROcp0_313 - OMcp0_313 * ROcp0_113) - qd[15] * (
                            OMcp0_114 * ROcp0_914 - OMcp0_314 * ROcp0_714);
            OPcp0_315 = OPcp0_313 + ROcp0_313 * qdd[14] + ROcp0_914 * qdd[15] + qd[14] * (OMcp0_113 * ROcp0_213 - OMcp0_213 * ROcp0_113) + qd[15] * (
                            OMcp0_114 * ROcp0_814 - OMcp0_214 * ROcp0_714);
            RLcp0_116 = ROcp0_415 * q[16];
            RLcp0_216 = ROcp0_515 * q[16];
            RLcp0_316 = ROcp0_615 * q[16];
            POcp0_116 = RLcp0_111 + RLcp0_112 + RLcp0_113 + RLcp0_114 + RLcp0_116 + RLcp0_18 + RLcp0_19 + q[1];
            POcp0_216 = RLcp0_211 + RLcp0_212 + RLcp0_213 + RLcp0_214 + RLcp0_216 + RLcp0_28 + RLcp0_29 + q[2];
            POcp0_316 = RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314 + RLcp0_316 + RLcp0_38 + RLcp0_39 + q[3];
            JTcp0_216_4 = -(RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314 + RLcp0_316 + RLcp0_38 + RLcp0_39);
            JTcp0_316_4 = RLcp0_211 + RLcp0_212 + RLcp0_213 + RLcp0_214 + RLcp0_216 + RLcp0_28 + RLcp0_29;
            JTcp0_116_5 = DPT_3_1 * C5 + C4 * (RLcp0_311 + RLcp0_39) + C4 * (RLcp0_312 + RLcp0_313) + C4 * (RLcp0_314 + RLcp0_316) - S4 * (RLcp0_211 +
                          RLcp0_29) - S4 * (RLcp0_212 + RLcp0_213) - S4 * (RLcp0_214 + RLcp0_216);
            JTcp0_216_5 = S4 * (RLcp0_111 + RLcp0_112 + RLcp0_113 + RLcp0_114 + RLcp0_116 + RLcp0_18 + RLcp0_19);
            JTcp0_316_5 = -C4 * (RLcp0_111 + RLcp0_112 + RLcp0_113 + RLcp0_114 + RLcp0_116 + RLcp0_18 + RLcp0_19);
            JTcp0_116_6 = ROcp0_85 * (RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314 + RLcp0_316 + RLcp0_39) - ROcp0_95 * (RLcp0_211 + RLcp0_29) -
                          ROcp0_95 * (RLcp0_212 + RLcp0_213) - ROcp0_95 * (RLcp0_214 + RLcp0_216);
            JTcp0_216_6 = RLcp0_116 * ROcp0_95 - RLcp0_314 * S5 - RLcp0_316 * S5 + ROcp0_95 * (RLcp0_111 + RLcp0_112 + RLcp0_113 + RLcp0_114 + RLcp0_19)
                          - S5 * (RLcp0_311 + RLcp0_39) - S5 * (RLcp0_312 + RLcp0_313);
            JTcp0_316_6 = RLcp0_214 * S5 - ROcp0_85 * (RLcp0_111 + RLcp0_112 + RLcp0_113 + RLcp0_114 + RLcp0_19) + S5 * (RLcp0_211 + RLcp0_29) + S5 * (
                              RLcp0_212 + RLcp0_213) - RLcp0_116 * ROcp0_85 + RLcp0_216 * S5;
            JTcp0_116_7 = ROcp0_85 * (RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314 + RLcp0_316 + RLcp0_39) - ROcp0_95 * (RLcp0_211 + RLcp0_29) -
                          ROcp0_95 * (RLcp0_212 + RLcp0_213) - ROcp0_95 * (RLcp0_214 + RLcp0_216);
            JTcp0_216_7 = RLcp0_116 * ROcp0_95 - RLcp0_314 * S5 - RLcp0_316 * S5 + ROcp0_95 * (RLcp0_111 + RLcp0_112 + RLcp0_113 + RLcp0_114 + RLcp0_19)
                          - S5 * (RLcp0_311 + RLcp0_39) - S5 * (RLcp0_312 + RLcp0_313);
            JTcp0_316_7 = RLcp0_214 * S5 - ROcp0_85 * (RLcp0_111 + RLcp0_112 + RLcp0_113 + RLcp0_114 + RLcp0_19) + S5 * (RLcp0_211 + RLcp0_29) + S5 * (
                              RLcp0_212 + RLcp0_213) - RLcp0_116 * ROcp0_85 + RLcp0_216 * S5;
            JTcp0_116_8 = ROcp0_27 * (RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314 + RLcp0_316 + RLcp0_39) - ROcp0_37 * (RLcp0_211 + RLcp0_29) -
                          ROcp0_37 * (RLcp0_212 + RLcp0_213) - ROcp0_37 * (RLcp0_214 + RLcp0_216);
            JTcp0_216_8 = -(ROcp0_17 * (RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314 + RLcp0_316 + RLcp0_39) - ROcp0_37 * (RLcp0_111 + RLcp0_19) -
                            ROcp0_37 * (RLcp0_112 + RLcp0_113) - ROcp0_37 * (RLcp0_114 + RLcp0_116));
            JTcp0_316_8 = ROcp0_17 * (RLcp0_211 + RLcp0_212 + RLcp0_213 + RLcp0_214 + RLcp0_216 + RLcp0_29) - ROcp0_27 * (RLcp0_111 + RLcp0_19) -
                          ROcp0_27 * (RLcp0_112 + RLcp0_113) - ROcp0_27 * (RLcp0_114 + RLcp0_116);
            JTcp0_116_9 = ROcp0_27 * (RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314) - ROcp0_37 * (RLcp0_211 + RLcp0_212) - ROcp0_37 * (RLcp0_213 +
                          RLcp0_214) - RLcp0_216 * ROcp0_37 + RLcp0_316 * ROcp0_27;
            JTcp0_216_9 = RLcp0_116 * ROcp0_37 - RLcp0_316 * ROcp0_17 - ROcp0_17 * (RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314) + ROcp0_37 * (
                              RLcp0_111 + RLcp0_112) + ROcp0_37 * (RLcp0_113 + RLcp0_114);
            JTcp0_316_9 = ROcp0_17 * (RLcp0_211 + RLcp0_212 + RLcp0_213 + RLcp0_214) - ROcp0_27 * (RLcp0_111 + RLcp0_112) - ROcp0_27 * (RLcp0_113 +
                          RLcp0_114) - RLcp0_116 * ROcp0_27 + RLcp0_216 * ROcp0_17;
            JTcp0_116_10 = ROcp0_59 * (RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314) - ROcp0_69 * (RLcp0_211 + RLcp0_212) - ROcp0_69 * (RLcp0_213 +
                           RLcp0_214) - RLcp0_216 * ROcp0_69 + RLcp0_316 * ROcp0_59;
            JTcp0_216_10 = RLcp0_116 * ROcp0_69 - RLcp0_316 * ROcp0_49 - ROcp0_49 * (RLcp0_311 + RLcp0_312 + RLcp0_313 + RLcp0_314) + ROcp0_69 * (
                               RLcp0_111 + RLcp0_112) + ROcp0_69 * (RLcp0_113 + RLcp0_114);
            JTcp0_316_10 = ROcp0_49 * (RLcp0_211 + RLcp0_212 + RLcp0_213 + RLcp0_214) - ROcp0_59 * (RLcp0_111 + RLcp0_112) - ROcp0_59 * (RLcp0_113 +
                           RLcp0_114) - RLcp0_116 * ROcp0_59 + RLcp0_216 * ROcp0_49;
            JTcp0_116_11 = ROcp0_810 * (RLcp0_312 + RLcp0_313 + RLcp0_314 + RLcp0_316) - ROcp0_910 * (RLcp0_212 + RLcp0_213) - ROcp0_910 * (
                               RLcp0_214 + RLcp0_216);
            JTcp0_216_11 = -(ROcp0_710 * (RLcp0_312 + RLcp0_313 + RLcp0_314 + RLcp0_316) - ROcp0_910 * (RLcp0_112 + RLcp0_113) - ROcp0_910 * (
                                 RLcp0_114 + RLcp0_116));
            JTcp0_316_11 = ROcp0_710 * (RLcp0_212 + RLcp0_213 + RLcp0_214 + RLcp0_216) - ROcp0_810 * (RLcp0_112 + RLcp0_113) - ROcp0_810 * (
                               RLcp0_114 + RLcp0_116);
            JTcp0_116_12 = ROcp0_211 * (RLcp0_313 + RLcp0_314) - ROcp0_311 * (RLcp0_213 + RLcp0_214) - RLcp0_216 * ROcp0_311 + RLcp0_316 * ROcp0_211;
            JTcp0_216_12 = RLcp0_116 * ROcp0_311 - RLcp0_316 * ROcp0_111 - ROcp0_111 * (RLcp0_313 + RLcp0_314) + ROcp0_311 * (RLcp0_113 + RLcp0_114);
            JTcp0_316_12 = ROcp0_111 * (RLcp0_213 + RLcp0_214) - ROcp0_211 * (RLcp0_113 + RLcp0_114) - RLcp0_116 * ROcp0_211 + RLcp0_216 * ROcp0_111;
            JTcp0_116_13 = ROcp0_512 * (RLcp0_314 + RLcp0_316) - ROcp0_612 * (RLcp0_214 + RLcp0_216);
            JTcp0_216_13 = -(ROcp0_412 * (RLcp0_314 + RLcp0_316) - ROcp0_612 * (RLcp0_114 + RLcp0_116));
            JTcp0_316_13 = ROcp0_412 * (RLcp0_214 + RLcp0_216) - ROcp0_512 * (RLcp0_114 + RLcp0_116);
            JTcp0_116_14 = -(RLcp0_216 * ROcp0_313 - RLcp0_316 * ROcp0_213);
            JTcp0_216_14 = RLcp0_116 * ROcp0_313 - RLcp0_316 * ROcp0_113;
            JTcp0_316_14 = -(RLcp0_116 * ROcp0_213 - RLcp0_216 * ROcp0_113);
            JTcp0_116_15 = -(RLcp0_216 * ROcp0_914 - RLcp0_316 * ROcp0_814);
            JTcp0_216_15 = RLcp0_116 * ROcp0_914 - RLcp0_316 * ROcp0_714;
            JTcp0_316_15 = -(RLcp0_116 * ROcp0_814 - RLcp0_216 * ROcp0_714);
            ORcp0_116 = OMcp0_215 * RLcp0_316 - OMcp0_315 * RLcp0_216;
            ORcp0_216 = -(OMcp0_115 * RLcp0_316 - OMcp0_315 * RLcp0_116);
            ORcp0_316 = OMcp0_115 * RLcp0_216 - OMcp0_215 * RLcp0_116;
            VIcp0_116 = ORcp0_111 + ORcp0_112 + ORcp0_113 + ORcp0_114 + ORcp0_116 + ORcp0_18 + ORcp0_19 + qd[1] + ROcp0_415 * qd[16];
            VIcp0_216 = ORcp0_211 + ORcp0_212 + ORcp0_213 + ORcp0_214 + ORcp0_216 + ORcp0_28 + ORcp0_29 + qd[2] + ROcp0_515 * qd[16];
            VIcp0_316 = ORcp0_311 + ORcp0_312 + ORcp0_313 + ORcp0_314 + ORcp0_316 + ORcp0_38 + ORcp0_39 + qd[3] + ROcp0_615 * qd[16];
            ACcp0_116 = qdd[1] + OMcp0_210 * ORcp0_311 + OMcp0_211 * ORcp0_312 + OMcp0_212 * ORcp0_313 + OMcp0_213 * ORcp0_314 + OMcp0_215 * ORcp0_316
                        + OMcp0_27 * ORcp0_38 + OMcp0_28 * ORcp0_39 - OMcp0_310 * ORcp0_211 - OMcp0_311 * ORcp0_212 - OMcp0_312 * ORcp0_213 - OMcp0_313 * ORcp0_214 -
                        OMcp0_315 * ORcp0_216 - OMcp0_37 * ORcp0_28 - OMcp0_38 * ORcp0_29 + OPcp0_210 * RLcp0_311 + OPcp0_211 * RLcp0_312 + OPcp0_212 * RLcp0_313 +
                        OPcp0_213 * RLcp0_314 + OPcp0_215 * RLcp0_316 + OPcp0_27 * RLcp0_38 + OPcp0_28 * RLcp0_39 - OPcp0_310 * RLcp0_211 - OPcp0_311 * RLcp0_212 -
                        OPcp0_312 * RLcp0_213 - OPcp0_313 * RLcp0_214 - OPcp0_315 * RLcp0_216 - OPcp0_37 * RLcp0_28 - OPcp0_38 * RLcp0_29 + ROcp0_415 * qdd[16] + (2.0) * qd[16] * (
                            OMcp0_215 * ROcp0_615 - OMcp0_315 * ROcp0_515);
            ACcp0_216 = qdd[2] - OMcp0_110 * ORcp0_311 - OMcp0_111 * ORcp0_312 - OMcp0_112 * ORcp0_313 - OMcp0_113 * ORcp0_314 - OMcp0_115 * ORcp0_316
                        - OMcp0_17 * ORcp0_38 - OMcp0_18 * ORcp0_39 + OMcp0_310 * ORcp0_111 + OMcp0_311 * ORcp0_112 + OMcp0_312 * ORcp0_113 + OMcp0_313 * ORcp0_114 +
                        OMcp0_315 * ORcp0_116 + OMcp0_37 * ORcp0_18 + OMcp0_38 * ORcp0_19 - OPcp0_110 * RLcp0_311 - OPcp0_111 * RLcp0_312 - OPcp0_112 * RLcp0_313 -
                        OPcp0_113 * RLcp0_314 - OPcp0_115 * RLcp0_316 - OPcp0_17 * RLcp0_38 - OPcp0_18 * RLcp0_39 + OPcp0_310 * RLcp0_111 + OPcp0_311 * RLcp0_112 +
                        OPcp0_312 * RLcp0_113 + OPcp0_313 * RLcp0_114 + OPcp0_315 * RLcp0_116 + OPcp0_37 * RLcp0_18 + OPcp0_38 * RLcp0_19 + ROcp0_515 * qdd[16] - (2.0) * qd[16] * (
                            OMcp0_115 * ROcp0_615 - OMcp0_315 * ROcp0_415);
            ACcp0_316 = qdd[3] + OMcp0_110 * ORcp0_211 + OMcp0_111 * ORcp0_212 + OMcp0_112 * ORcp0_213 + OMcp0_113 * ORcp0_214 + OMcp0_115 * ORcp0_216
                        + OMcp0_17 * ORcp0_28 + OMcp0_18 * ORcp0_29 - OMcp0_210 * ORcp0_111 - OMcp0_211 * ORcp0_112 - OMcp0_212 * ORcp0_113 - OMcp0_213 * ORcp0_114 -
                        OMcp0_215 * ORcp0_116 - OMcp0_27 * ORcp0_18 - OMcp0_28 * ORcp0_19 + OPcp0_110 * RLcp0_211 + OPcp0_111 * RLcp0_212 + OPcp0_112 * RLcp0_213 +
                        OPcp0_113 * RLcp0_214 + OPcp0_115 * RLcp0_216 + OPcp0_17 * RLcp0_28 + OPcp0_18 * RLcp0_29 - OPcp0_210 * RLcp0_111 - OPcp0_211 * RLcp0_112 -
                        OPcp0_212 * RLcp0_113 - OPcp0_213 * RLcp0_114 - OPcp0_215 * RLcp0_116 - OPcp0_27 * RLcp0_18 - OPcp0_28 * RLcp0_19 + ROcp0_615 * qdd[16] + (2.0) * qd[16] * (
                            OMcp0_115 * ROcp0_515 - OMcp0_215 * ROcp0_415);
            OMcp0_117 = OMcp0_115 + ROcp0_115 * qd[17];
            OMcp0_217 = OMcp0_215 + ROcp0_215 * qd[17];
            OMcp0_317 = OMcp0_315 + ROcp0_315 * qd[17];
            OMcp0_118 = OMcp0_117 + ROcp0_717 * qd[18];
            OMcp0_218 = OMcp0_217 + ROcp0_817 * qd[18];
            OMcp0_318 = OMcp0_317 + ROcp0_917 * qd[18];
            OPcp0_118 = OPcp0_115 + ROcp0_115 * qdd[17] + ROcp0_717 * qdd[18] + qd[17] * (OMcp0_215 * ROcp0_315 - OMcp0_315 * ROcp0_215) + qd[18] * (
                            OMcp0_217 * ROcp0_917 - OMcp0_317 * ROcp0_817);
            OPcp0_218 = OPcp0_215 + ROcp0_215 * qdd[17] + ROcp0_817 * qdd[18] - qd[17] * (OMcp0_115 * ROcp0_315 - OMcp0_315 * ROcp0_115) - qd[18] * (
                            OMcp0_117 * ROcp0_917 - OMcp0_317 * ROcp0_717);
            OPcp0_318 = OPcp0_315 + ROcp0_315 * qdd[17] + ROcp0_917 * qdd[18] + qd[17] * (OMcp0_115 * ROcp0_215 - OMcp0_215 * ROcp0_115) + qd[18] * (
                            OMcp0_117 * ROcp0_817 - OMcp0_217 * ROcp0_717);

            // = = Block_1_0_0_1_1_0 = =

            // Symbolic Outputs

            sens->P[1] = POcp0_116;
            sens->P[2] = POcp0_216;
            sens->P[3] = POcp0_316;
            sens->R[1][1] = ROcp0_118;
            sens->R[1][2] = ROcp0_218;
            sens->R[1][3] = ROcp0_318;
            sens->R[2][1] = ROcp0_418;
            sens->R[2][2] = ROcp0_518;
            sens->R[2][3] = ROcp0_618;
            sens->R[3][1] = ROcp0_717;
            sens->R[3][2] = ROcp0_817;
            sens->R[3][3] = ROcp0_917;
            sens->V[1] = VIcp0_116;
            sens->V[2] = VIcp0_216;
            sens->V[3] = VIcp0_316;
            sens->OM[1] = OMcp0_118;
            sens->OM[2] = OMcp0_218;
            sens->OM[3] = OMcp0_318;
            sens->J[1][1] = (1.0);
            sens->J[1][5] = JTcp0_116_5;
            sens->J[1][6] = JTcp0_116_6;
            sens->J[1][7] = JTcp0_116_7;
            sens->J[1][8] = JTcp0_116_8;
            sens->J[1][9] = JTcp0_116_9;
            sens->J[1][10] = JTcp0_116_10;
            sens->J[1][11] = JTcp0_116_11;
            sens->J[1][12] = JTcp0_116_12;
            sens->J[1][13] = JTcp0_116_13;
            sens->J[1][14] = JTcp0_116_14;
            sens->J[1][15] = JTcp0_116_15;
            sens->J[1][16] = ROcp0_415;
            sens->J[2][2] = (1.0);
            sens->J[2][4] = JTcp0_216_4;
            sens->J[2][5] = JTcp0_216_5;
            sens->J[2][6] = JTcp0_216_6;
            sens->J[2][7] = JTcp0_216_7;
            sens->J[2][8] = JTcp0_216_8;
            sens->J[2][9] = JTcp0_216_9;
            sens->J[2][10] = JTcp0_216_10;
            sens->J[2][11] = JTcp0_216_11;
            sens->J[2][12] = JTcp0_216_12;
            sens->J[2][13] = JTcp0_216_13;
            sens->J[2][14] = JTcp0_216_14;
            sens->J[2][15] = JTcp0_216_15;
            sens->J[2][16] = ROcp0_515;
            sens->J[3][3] = (1.0);
            sens->J[3][4] = JTcp0_316_4;
            sens->J[3][5] = JTcp0_316_5;
            sens->J[3][6] = JTcp0_316_6;
            sens->J[3][7] = JTcp0_316_7;
            sens->J[3][8] = JTcp0_316_8;
            sens->J[3][9] = JTcp0_316_9;
            sens->J[3][10] = JTcp0_316_10;
            sens->J[3][11] = JTcp0_316_11;
            sens->J[3][12] = JTcp0_316_12;
            sens->J[3][13] = JTcp0_316_13;
            sens->J[3][14] = JTcp0_316_14;
            sens->J[3][15] = JTcp0_316_15;
            sens->J[3][16] = ROcp0_615;
            sens->J[4][4] = (1.0);
            sens->J[4][6] = S5;
            sens->J[4][7] = S5;
            sens->J[4][8] = ROcp0_17;
            sens->J[4][9] = ROcp0_17;
            sens->J[4][10] = ROcp0_49;
            sens->J[4][11] = ROcp0_710;
            sens->J[4][12] = ROcp0_111;
            sens->J[4][13] = ROcp0_412;
            sens->J[4][14] = ROcp0_113;
            sens->J[4][15] = ROcp0_714;
            sens->J[4][17] = ROcp0_115;
            sens->J[4][18] = ROcp0_717;
            sens->J[5][5] = C4;
            sens->J[5][6] = ROcp0_85;
            sens->J[5][7] = ROcp0_85;
            sens->J[5][8] = ROcp0_27;
            sens->J[5][9] = ROcp0_27;
            sens->J[5][10] = ROcp0_59;
            sens->J[5][11] = ROcp0_810;
            sens->J[5][12] = ROcp0_211;
            sens->J[5][13] = ROcp0_512;
            sens->J[5][14] = ROcp0_213;
            sens->J[5][15] = ROcp0_814;
            sens->J[5][17] = ROcp0_215;
            sens->J[5][18] = ROcp0_817;
            sens->J[6][5] = S4;
            sens->J[6][6] = ROcp0_95;
            sens->J[6][7] = ROcp0_95;
            sens->J[6][8] = ROcp0_37;
            sens->J[6][9] = ROcp0_37;
            sens->J[6][10] = ROcp0_69;
            sens->J[6][11] = ROcp0_910;
            sens->J[6][12] = ROcp0_311;
            sens->J[6][13] = ROcp0_612;
            sens->J[6][14] = ROcp0_313;
            sens->J[6][15] = ROcp0_914;
            sens->J[6][17] = ROcp0_315;
            sens->J[6][18] = ROcp0_917;
            sens->A[1] = ACcp0_116;
            sens->A[2] = ACcp0_216;
            sens->A[3] = ACcp0_316;
            sens->OMP[1] = OPcp0_118;
            sens->OMP[2] = OPcp0_218;
            sens->OMP[3] = OPcp0_318;

            break;
        default:
            break;
    }


    // ====== END Task 1 ======


}


