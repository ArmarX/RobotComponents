
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore DMPComponent LongtermMemory PriorKnowledge CommonStorage)

armarx_add_test(DMPComponentTest DMPComponentTest.cpp "${LIBS}")
