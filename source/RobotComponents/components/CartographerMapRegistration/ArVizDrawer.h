/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <Eigen/Geometry>

#include <RobotAPI/components/ArViz/Client/ScopedClient.h>
// TODO(fabian.reister): use forward decl
#include <RobotComponents/libraries/cartographer/map_registration/MapRegistration.h>

namespace armarx
{

}

namespace armarx::viz
{
    struct Layer;
}

namespace armarx::cartographer
{
    class ArVizDrawer : virtual public armarx::viz::ScopedClient
    {
    public:
        ArVizDrawer(armarx::viz::Client& arviz);
        virtual ~ArVizDrawer();

        void drawFrames(const Eigen::Affine3f& world_T_map);

        void drawPointCloud(const MapRegistration::PointCloud& cloud, const Eigen::Affine3f& world_T_map);

        void drawClusters(const MapRegistration::Clusters& clusters, const Eigen::Affine3f& world_T_map);

        void drawRobotPoses(const std::vector<Eigen::Affine3f>& robotPoses, const Eigen::Affine3f& world_T_map);

        void drawModels(const std::vector<wykobi::Model>& models);

        void drawModelAssociations(const std::vector<MapRegistration::Association>& associations, const Eigen::Affine3f& world_T_map);

        void drawMapBoundaries(const RotatedRect& boundingRect, const Eigen::Affine3f& world_T_map);

        void drawSelectedBoundingBoxCorner(const Eigen::Affine3f& boundingBoxCornerPose, const Eigen::Affine3f& world_T_map);

        void clear();

    protected:
        void drawCluster(const MapRegistration::Cluster& cluster, const Eigen::Affine3f& world_T_map, viz::Layer& layer) const;

        void drawRobotPose(const Eigen::Affine3f& robotPose, const Eigen::Affine3f& world_T_map, viz::Layer& layer) const;

        void drawModel(const wykobi::Model& model, viz::Layer& layer) const;

        void drawModelAssociation(const MapRegistration::Association& association, const Eigen::Affine3f& world_T_map, viz::Layer& layer) const;

    private:
        struct
        {
            std::string mapBoundaries{"map_boundaries"};
            std::string modelSceneAssociations{"model_scene_associations"};
            std::string sceneModels{"scene_models"};
            std::string robotPoses{"robot_poses"};
            std::string selectedBoundingBoxCorner{"selected_bounding_box_corner"};
            std::string clusters{"clusters"};
            std::string pointCloud{"point_cloud"};
            std::string frames{"frames"};
        } layers;

    };

} // namespace armarx::cartographer
