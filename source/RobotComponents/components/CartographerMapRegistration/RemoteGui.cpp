#include "RemoteGui.h"

#include "ArmarXCore/core/time/CycleUtil.h"
#include "ArmarXCore/statechart/ParameterMapping.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/IntegerWidgets.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/LayoutWidgets.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/StaticWidgets.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <utility>

namespace armarx::cartographer_map_registration
{

    namespace gui = RemoteGui;
    namespace xc = ::armarx::cartographer_map_registration;

    RemoteGui::RemoteGui(const RemoteGuiInterfacePrx& remoteGui, RemoteGuiCallee& callee) :
        RemoteGui::RemoteGuiBase(remoteGui),
        callee(callee)
    {
        createRemoteGuiTab();
    }

    void xc::RemoteGui::createRemoteGuiTab()
    {
        auto boxLayout = gui::makeVBoxLayout();

        const auto createButtonWithDescription = [&boxLayout](const std::string & buttonNameAndText, const std::string& description = "")
        {
            const gui::WidgetPtr label = gui::makeTextLabel(buttonNameAndText);
            const gui::WidgetPtr button = gui::makeButton(buttonNameAndText).toolTip(description);
            const gui::WidgetPtr line = gui::makeHBoxLayout().children({label, button});
            boxLayout.addChild(line);
        };

        createButtonWithDescription(loadGraphButtonName, "Use this button to create a map after you collected enough data.");
        createButtonWithDescription(loadDataFromMemoryButtonName);
        createButtonWithDescription(selectBoundingBoxCornerButtonName);
        createButtonWithDescription(alignBoundingBoxButtonName);
        createButtonWithDescription(findAssociationsButtonName);
        createButtonWithDescription(computeCoarseCorrectionButtonName);
        createButtonWithDescription(computeFineCorrectionButtonName);
        createButtonWithDescription(storeRegistrationResultButtonName);

        createTab(boxLayout);
    }


    const std::string& xc::RemoteGui::tabName() const
    {
        return remoteGuiTabMame;
    }

    void xc::RemoteGui::handleEvents(armarx::RemoteGui::TabProxy& tab)
    {
        auto callIfClicked = [&](const std::string & buttonName, auto&& cbFn, auto... args)
        {
            const ::armarx::RemoteGui::ButtonProxy button = getTab().getButton(buttonName);
            if (button.clicked())
            {
                auto fn = std::bind(cbFn, &callee, std::forward<decltype(args)>(args)...);
                fn();
            }
        };

        auto callCallIfClicked = [&](const std::string & buttonName, auto&& cbFn, auto&& callArgs)
        {
            const ::armarx::RemoteGui::ButtonProxy button = getTab().getButton(buttonName);
            if (button.clicked())
            {
                auto fn = std::bind(cbFn, &callee, callArgs());
                fn();
            }
        };

        static int selectedCorner = 0;


        callIfClicked(loadGraphButtonName, &RemoteGuiCallee::loadGraph);
        callIfClicked(loadDataFromMemoryButtonName, &RemoteGuiCallee::loadDataFromMemory);
        callIfClicked(alignBoundingBoxButtonName, &RemoteGuiCallee::alignBoundingBox);
        callIfClicked(findAssociationsButtonName, &RemoteGuiCallee::findAssociations);

        callIfClicked(computeCoarseCorrectionButtonName, &RemoteGuiCallee::computeCoarseCorrection);
        callIfClicked(computeFineCorrectionButtonName, &RemoteGuiCallee::computeFineCorrection);
        callIfClicked(storeRegistrationResultButtonName, &RemoteGuiCallee::storeRegistrationResult);

        callCallIfClicked(selectBoundingBoxCornerButtonName, &RemoteGuiCallee::selectBoundingBoxCorner, [&]() -> int
        {
            return selectedCorner++;
        });


    }

    void xc::RemoteGui::enable()
    {
        getTab().internalSetDisabled(loadGraphButtonName, false);
    }

    void xc::RemoteGui::disable()
    {
        getTab().internalSetDisabled(loadGraphButtonName, true);
    }


} // namespace armarx::cartographer_map_registration
