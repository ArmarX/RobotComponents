/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <ArmarXGui/libraries/RemoteGui/RemoteGuiBase.h>

namespace armarx::cartographer_map_registration
{

    class RemoteGuiCallee
    {
    public:
        virtual void loadGraph() = 0;
        virtual void loadDataFromMemory() = 0;
        virtual void alignBoundingBox() = 0;
        virtual void selectBoundingBoxCorner(int i) = 0;
        virtual void findAssociations() = 0;
        virtual void computeCoarseCorrection() = 0;
        virtual void computeFineCorrection() = 0;
        virtual void storeRegistrationResult() = 0;
    };

    class RemoteGui : virtual public armarx::RemoteGui::RemoteGuiBase
    {
    public:
        RemoteGui(const RemoteGuiInterfacePrx& remoteGui, RemoteGuiCallee& callee);

        void enable();
        void disable();

    private:

        void handleEvents(armarx::RemoteGui::TabProxy& tab) override;
        const std::string& tabName() const override;

        RemoteGuiCallee& callee;

        void createRemoteGuiTab();

        const std::string remoteGuiTabMame = "Map registration";

        const std::string loadGraphButtonName = "Load SLAM graph";
        const std::string loadDataFromMemoryButtonName = "Load data from memory";
        const std::string selectBoundingBoxCornerButtonName = "select bb corner";
        const std::string alignBoundingBoxButtonName = "Align (bounding box)";
        const std::string findAssociationsButtonName = "Find associations";
        const std::string computeCoarseCorrectionButtonName = "Coarse alignment (locally)";
        const std::string computeFineCorrectionButtonName = "Fine alignment (locally)";
        const std::string storeRegistrationResultButtonName = "Store registration result";

    };

} // namespace armarx::cartographer_map_registration
