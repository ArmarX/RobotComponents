/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <ArmarXCore/core/Component.h>

#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/ObjectClassSegmentWrapper.h>

#include <RobotAPI/libraries/armem_vision/client/laser_scans/Reader.h>


// #include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

// #include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include "ArVizDrawer.h"
#include "RobotComponents/components/CartographerMapRegistration/RemoteGui.h"

namespace armarx
{
    class LaserScanAggregator;
}

namespace armarx::cartographer
{

    /**
     * @defgroup Component-CartographerMapRegistration CartographerMapRegistration
     * @ingroup RobotComponents-Components
     * A description of the component CartographerMapRegistration.
     *
     * @class CartographerMapRegistration
     * @ingroup Component-CartographerMapRegistration
     * @brief Brief description of class CartographerMapRegistration.
     *
     * Detailed description of class CartographerMapRegistration.
     */
    class CartographerMapRegistration :
        virtual public armarx::Component
    // , virtual public armarx::DebugObserverComponentPluginUser
    // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
        , virtual public armarx::cartographer_map_registration::RemoteGuiCallee
    {
    public:

        using Point = pcl::PointXYZ;
        using PointCloud = pcl::PointCloud<Point>;
        using PointClouds = std::vector<PointCloud>;

        CartographerMapRegistration();
        virtual ~CartographerMapRegistration();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        // RemoteGuiCallee interface
        void loadGraph() override;
        void loadDataFromMemory() override;
        void selectBoundingBoxCorner(int i) override;
        void alignBoundingBox() override;
        void findAssociations() override;
        void computeCoarseCorrection() override;
        void computeFineCorrection() override;
        void storeRegistrationResult() override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */


    private:

        void updateArViz();

        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */

        Eigen::Affine3f selectedBoundingBoxCorner() const;




        // Private member variables go here.
        std::unique_ptr<armem::vision::laser_scans::client::Reader> mappingDataReader;

        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
        memoryx::WorkingMemoryInterfacePrx workingMemory;
        armarx::RobotStateComponentInterfacePrx robotStateComponent;

        // publisher
        RemoteGuiInterfacePrx remoteGuiInterface;


        memoryx::ObjectClassSegmentWrapper objectClassSegment;

        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string mapPath = "./RobotComponents/maps/";
            std::string mapToLoad;
            std::string cartographerConfigPath = "./RobotComponents/cartographer/config/";

            float minLaserRange = 400.F;
        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */
        ArVizDrawer arvizDrawer;

        std::vector<wykobi::Model> models;

        std::unique_ptr<LaserScanAggregator> aggregator;

        std::unique_ptr<MapRegistration> mapRegistration;
        std::vector<MapRegistration::Association> associations;

        Eigen::Affine3f world_T_map = Eigen::Affine3f::Identity();

        std::unique_ptr<armarx::cartographer_map_registration::RemoteGui> remoteGui;

        int boundingBoxCorner{0};

    };

}  // namespace armarx::cartographer

