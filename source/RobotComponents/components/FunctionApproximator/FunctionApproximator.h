/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::FunctionApproximator
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <RobotComponents/interface/components/FunctionApproximatorBase.h>
#include <dmp/functionapproximation/radialbasisfunctioninterpolator.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/functionapproximationregistration.h>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

namespace armarx
{
    /**
     * @class FunctionApproximatorPropertyDefinitions
     * @brief
     */
    class FunctionApproximatorPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        FunctionApproximatorPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-FunctionApproximator FunctionApproximator
     * @ingroup RobotComponents-Components
     * A description of the component FunctionApproximator.
     *
     * @class FunctionApproximator
     * @ingroup Component-FunctionApproximator
     * @brief Brief description of class FunctionApproximator.
     *
     * Detailed description of class FunctionApproximator.
     */
    using FuncAppMap = std::map<std::string, DMP::FunctionApproximationInterfacePtr>;
    using FuncAppPair = std::pair<std::string, DMP::FunctionApproximationInterfacePtr> ;
    class FunctionApproximator :
        virtual public Component,
        virtual public armarx::FunctionApproximatorBase
    {
    public:

        FunctionApproximator():
            dimension(2)
        {
            DMP::DVec widthFactor;
            widthFactor.push_back(10);
            widthFactor.push_back(10);
        }

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "FunctionApproximator";
        }


    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        //        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

        // FunctionApproximatorBase interface
    public:

        void initialize(const std::string& fappName, const Ice::DoubleSeq& factors, const Ice::Current&) override;
        void learn(const std::string& name, const DVector2d&, const DVector2d&, const Ice::Current&) override;
        Ice::Double predict(const std::string& name, const Ice::DoubleSeq&, const Ice::Current&) override;
        void ilearn(const std::string& name, const Ice::DoubleSeq&, Ice::Double, const Ice::Current&) override;
        void blearn(const std::string& name, const Ice::DoubleSeq&, const Ice::DoubleSeq&, const Ice::Current&) override;
        Ice::DoubleSeq bpredict(const std::string& name, const Ice::DoubleSeq& x, const Ice::Current&) override;

        void getFunctionApproximatorFromFile(const std::string& funcName, const std::string& name, const Ice::Current&) override; //TODO: deserialization;
        void getFunctionApproximatorsFromFile(const std::vector<std::string>& funcNameList, const std::string& filename, const Ice::Current&) override;
        void saveFunctionApproximatorInFile(const std::string& funcName, const std::string& name, const Ice::Current&) override; //TODO:  serialization;

        //        void getWeightsFromFile(const std::string & funcName, const std::string& filename, const Ice::Current&);
        //        void writeWeightsToFile(const std::string & funcName, const std::string& filename, const Ice::Current&);

        void reset(const Ice::Current&) override;

        void saveFunctionApproximatorInFileTest(const std::string& funcName, const std::string& name);
        void initializeTest(const std::string& fappName, const Ice::DoubleSeq& widthFactors);
    private:
        FuncAppMap funcAppPool;
        int dimension;
        int num;

    };
    using FunctionApproximatorPtr = ::IceInternal::Handle< ::armarx::FunctionApproximator>;

}

