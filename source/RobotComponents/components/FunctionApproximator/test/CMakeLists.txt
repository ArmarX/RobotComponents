
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore FunctionApproximator)
 
armarx_add_test(FunctionApproximatorTest FunctionApproximatorTest.cpp "${LIBS}")