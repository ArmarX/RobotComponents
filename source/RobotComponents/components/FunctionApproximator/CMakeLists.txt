armarx_component_set_name("FunctionApproximator")

find_package(DMP QUIET)
find_package(MMMCore QUIET)

armarx_build_if(DMP_FOUND "DMP not available")
armarx_build_if(MMMCore_FOUND "MMMCore not available")

set(COMPONENT_LIBS ArmarXCore ${DMP_LIBRARIES} MMMCore RobotComponentsInterfaces)
set(SOURCES FunctionApproximator.cpp)
set(HEADERS FunctionApproximator.h)

armarx_add_component("${SOURCES}" "${HEADERS}")
target_compile_options(FunctionApproximator PUBLIC "-fopenmp")
if(DMP_FOUND)
    target_include_directories(FunctionApproximator PUBLIC ${DMP_INCLUDE_DIRS})
endif()

# add unit tests
add_subdirectory(test)
