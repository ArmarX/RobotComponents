/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "reflex.h"
#include "ReflexCombination.h"
#include <Eigen/Core>
#include <Eigen/Geometry>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/IK/GazeIK.h>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <RobotComponents/components/Feedforward/GazeStabilization.hh>
#include <RobotComponents/components/Feedforward/forwardPredictor.h>

namespace armarx
{
    class ReflexCombination;
    class FeedforwardReflex : virtual public Reflex
    {
    public:
        FeedforwardReflex(int interval) : Reflex(interval)
        {
            stabilizer = NULL;
            forward_predictor = NULL;

            optFlow_pred.resize(2);
            mean_optFl_pred = 0.;
            gyroscopeRotation_pred.resize(3);
            onStop();
        }
        ~FeedforwardReflex() override
        {
            if (stabilizer)
            {
                delete stabilizer;
            }

            if (forward_predictor)
            {
                delete forward_predictor;
            }
        }

        void setRobot(std::string nodeSetName, std::string headIKName, RobotStateComponentInterfacePrx robotStateComponent);
        void setBools(bool armar4, bool velocityBased);
        bool update_input_fromArmarX(GazeStabInput*  gs_input);
        void update_output_toArmarX(GazeStabOutput*  gs_output);

        void reportJointAngles(const NameValueMap& values, bool valueChanged, const Ice::Current& c);
        void reportJointVelocities(const NameValueMap& values, bool valueChanged, const Ice::Current& c);
        void reportPlatformVelocity(float x, float y, float a);

        void reportHeadTargetChanged(const NameValueMap& targetJointAngles, const FramedPositionBasePtr& targetPosition);


        std::string getName() const override
        {
            return "FeedforwardReflex";
        }

        // optical flow prediction
        std::vector<float> optFlow_pred; // [x, y] in [deg/s]
        double mean_optFl_pred; // mean distance of the optical flow [deg/s]
        std::vector<float> gyroscopeRotation_pred; // head IMU velocity in head attached frame (x,y,z) [rad/s]

    protected:

        void onStop() override;

        void calc() override;

    private:
        VirtualRobot::RobotPtr robot;
        std::vector< VirtualRobot::RobotNodePtr > allRobotNodeSet;
        RobotStateComponentInterfacePrx robotStateComponent;
        std::string headIKName;

        std::mutex dataMutex;

        GazeStabilization* stabilizer;
        ForwardPredictor* forward_predictor;

        std::vector<std::string> headJointNames;

        FramedPositionPtr globalPos;
        IceUtil::Time startTime;

        bool reportedJointAnglesBool, reportedJointVelocitiesBool;
        bool armar4, velocityBased;
        NameValueMap reportedJointAngles, reportedJointVelocities;

    };
}
