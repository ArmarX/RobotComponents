armarx_component_set_name("SimpleRobotPlacement")
set(COMPONENT_LIBS
    RobotComponentsInterfaces
    MemoryXCore MemoryXVirtualRobotHelpers MemoryXObjectRecognitionHelpers
    RobotAPICore
    ArmarXCoreStatechart ArmarXCoreObservers MotionPlanning
)
set(SOURCES SimpleRobotPlacement.cpp)
set(HEADERS SimpleRobotPlacement.h)
armarx_add_component("${SOURCES}" "${HEADERS}")
